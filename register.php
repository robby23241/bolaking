<?php include('base.php')?>

<div ui-view="content">
    <div class="register_form">
        <div class="form_container">
            <div style="display: flex; margin-top: 30px">
                <div style="width: 32px">
                </div>
                <div class="member_benefits">
                    <div>
                        <div ng-bind-html="memberBenefit">
                            <style>
                                .member-benefits.header {
                                    color: #3B3B3B;
                                    font-size: 15px;
                                    text-align: left;
                                    padding-left: 30px;
                                    margin-bottom: 15px;
                                }
                                
                                .member-benefits.body {
                                    text-align: left;
                                    font-size: 12px;
                                    color: #B0B0B0;
                                    padding: 0px 10px;
                                }
                                
                                .member-benefits.body li {
                                    line-height: 13px;
                                    padding: 5px 0px;
                                }
                            </style>
                            <div class="member-benefits header">
                                <b>MEMBER BENEFITS</b>
                            </div>
                            <div class="member-benefits body">
                                <ul>
                                    <li>EPL KICK OFF 100% Welcome Bonus up to MYR 800</li>
                                    <li>1% Unlimited Daily Cash Rebate for sport and live casino</li>
                                    <li>1% Unlimited Daily Cash Rebate for Slot</li>
                                    <li>Fast deposit and withdrawal</li>
                                    <li>Free Live Score and Live TV for sports</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 35px; height: 1px"></div>
                <div class="form_section">
                    <div class="register_header">
                        <span style="width: 110px; height: 13px" class="text-light header-title" data-translate="register.createAccount">
                            Create Account
                        </span>
                    </div>
                    <div>
                        <div class="progress_bar_container">
                            <div class="progress_bar">
                                <hr>
                                <span class="active-circle left-circle">1</span> 
                                <span class="inactive-circle middle-circle">2</span> 
                                <span class="inactive-circle right-circle">✔</span>
                            </div>
                        </div>
                        <div class="data_input">
                            <form>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data">
                                        <span>Username</span>
                                    </div>
                                    <div class="register-input-div">
                                        <span style="color: red; padding: 0 5px 0 11px">*&nbsp;</span>
                                        <input type="text" name="username" class="form-control">
                                    </div>
                                </div>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data">
                                        <span>New Password</span>
                                    </div>
                                    <div class="register-input-div">
                                        <span style="color: red; padding: 0 5px 0 11px">*&nbsp;</span>
                                        <input type="password" name="password" class="form-control">
                                    </div>
                                </div>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data">
                                        <span>Confirm Password</span>
                                    </div>
                                    <div class="register-input-div">
                                        <span style="color: red; padding: 0 5px 0 11px">*&nbsp;</span>
                                        <input type="password" name="confirm_password" class="form-control">
                                    </div>
                                </div>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data"></div>
                                    <div class="register-breakline-div"><hr></div>
                                </div>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data">
                                        <span>Affiliate ID</span>
                                    </div>
                                    <div class="register-input-div">
                                        <span style="color: red; padding: 0 5px 0 11px">*&nbsp;</span>
                                        <input type="text" name="affiliate" class="form-control">
                                    </div>
                                </div>
                                <div style="display: flex; height: 30px; margin-top: 31px; margin-bottom: 25px">
                                    <div class="register-form-data"></div>
                                    <div class="register-input-div">
                                        <button type="submit" class="btn btn-primary" style="margin-left: 27px" disabled="disabled">
                                            JOIN
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 100%; margin-top: 2%; margin-bottom: 35px" ng-show="vm.link != ''" aria-hidden="false" class=""><img ng-src="public/html/images/bk8-DBR-Register-en.jpg" style="display: block; margin: 0 auto" src="public/html/images/bk8-DBR-Register-en.jpg"></div>
        </div>
    </div>
</div>

<?php include('footer_sign.php') ?>