<div ui-view="banner">
        <div id="special-support-container" class="positionFixed" ng-show="vm.bannerShow &amp;&amp; vm.showContactUs" aria-hidden="false">
            <div class="side_menu_right" style="position: absolute; top: 128px;">
                <span>
                    <div class="side_menu_contant">
                        <i class="icons icons_side_menu_contant"></i>
                    </div>
                    <div class="side_menu_contant_on off">
                        <span>
                            <span>
                                <i class="icons icons_side_menu_arrow"></i>
                            </span>
                        </span>
                        <!---->
                        <span ng-repeat="contact in vm.contactList">
                            <span>
                                <i ng-class="vm.customIconsForContact(contact);" class="icons icons_side_menu_wechat"></i>
                                <div class="on item_hover" ng-show="contact.href == ''" aria-hidden="false">
                                    <p ng-bind-html="contact.description">bolakingcsmyr</p>
                                </div>
                                <div class="on item_hover_img ng-hide" ng-show="contact.href != ''" aria-hidden="true">
                                    <img ng-src="" class="icons icons_side_menu_line_code">
                                </div>
                            </span>
                        </span>
                        <!---->
                        <span ng-repeat="contact in vm.contactList">
                            <span>
                                <i ng-class="vm.customIconsForContact(contact);" class="icons icons_side_menu_watsapp"></i>
                                <div class="on item_hover" ng-show="contact.href == ''" aria-hidden="false">
                                    <p ng-bind-html="contact.description">+601157735870</p>
                                </div>
                                <div class="on item_hover_img ng-hide" ng-show="contact.href != ''" aria-hidden="true">
                                    <img ng-src="" class="icons icons_side_menu_line_code">
                                </div>
                            </span>
                        </span>
                        <!---->
                        <span ng-repeat="contact in vm.contactList">
                            <span>
                                <i ng-class="vm.customIconsForContact(contact);" class="icons icons_side_menu_mobile"></i>
                                <div class="on item_hover" ng-show="contact.href == ''" aria-hidden="false">
                                    <p ng-bind-html="contact.description">+60392121600</p>
                                </div>
                                <div class="on item_hover_img ng-hide" ng-show="contact.href != ''" aria-hidden="true">
                                    <img ng-src="" class="icons icons_side_menu_line_code">
                                </div>
                            </span>
                        </span>
                        <!---->
                    </div>
                </span>
            </div>
        </div>
        <div id="gameBanner" class="positionFixed" ng-show="vm.bannerShow == true ? vm.affPoral : vm.bannerShow" aria-hidden="false">
            <div class="side_menu_left" style="position: absolute; top: 128px;">
                <span>
                    <div class="side_menu_download">
                        <i class="icons icons_side_menu_download"></i>
                    </div>
                    <div class="side_menu_download_on off">
                        <span>
                            <span>
                                <i class="icons icons_side_menu_arrow"></i>
                            </span>
                        </span>
                        <span>
                            <span>
                                <a href="/mobile#mobileDownloadContainer" class="icons icons_side_menu_android">
                                    <i data-class="icons_side_menu_android"></i>
                                    <div class="on item_hover">
                                        <p style="margin: 0">Android Download</p>
                                    </div>
                                </a>
                            </span>
                        </span>
                        <span>
                            <span>
                                <a href="/mobile#mobileDownloadContainer2" class="icons icons_side_menu_ios">
                                    <i data-class="icons_side_menu_ios"></i>
                                    <div class="on item_hover">
                                        <p style="margin: 0">IOS Download</p>
                                    </div>
                                </a>
                            </span>
                        </span>
                        <span>
                            <!--                    <span><a href="/mobile#mobileDownloadContainer" class="icons icons_side_menu_pc"><i data-class="icons_side_menu_pc" ></i><div class="on item_hover"><p style="margin: 0">SG</p><p style="margin: 0">PC Download</p></div></a></span></span> -->
                        </span>
                    </div>
                </span>
            </div>
        </div>
        <div id="spContactDiv" style="witdh: 100%"></div>
        <div id="hotGameDiv" ng-show="(vm.bannerShow == true &amp;&amp; vm.hotGameBannerShow == true) ? vm.affPoral : (vm.hotGameBannerShow &amp;&amp; vm.bannerShow)" class="hot-game-banner ng-hide" aria-hidden="true">
            <div id="hotGameCloseBtn">
                <!-- <img id="hotGameCloseBtnImg" /> -->
                <img id="hotGameCloseBtnImg" src="assets/images/closebutton.png">
            </div>
        </div>
        <div id="angpowDiv" ng-show="vm.angpowBannerShow == true" ng-click="vm.successClaimAngpow()" class="angpow-banner ng-hide" style="position: fixed; z-index: 200; bottom:5%; height: 190px; right: 15px" role="button" tabindex="0" aria-hidden="true">
            <img http-src="angpow_banner" src="assets/images/redPacket.png">
            <div id="angpowCloseBtn" style="margin-top: -52%; margin-left: 81%">
                <img id="angpowCloseBtnImg" http-src="angpow_close_btn" src="assets/images/closebutton.png">
            </div>
        </div>
        <div id="spContactDiv" style="width: 100%"></div>
        <script>
            $(document).ready(function () {
                initPopup();
                initSide();
            });

            function initPopup() {
                $(".icons_popup_close").click(function() {
                    $(this).parent().addClass("off");
                });
                handleScroll();
                angular.element($(window)).bind("scroll", function() {
                    handleScroll();
                });
            }


            function handleScroll() {
                var popObj = {
                    'pop_up_left': {
                        fixedTop: 560,
                        absoluteTop: 1000
                    },
                    'pop_up_right': {
                        fixedTop: 470,
                        absoluteTop: 1000
                    },
                    'side_menu_left': {
                        fixedTop: 0,
                        absoluteTop: 128
                    },
                    'side_menu_right': {
                        fixedTop: 0,
                        absoluteTop: 128
                    }
                }
                var scrollTop =
                window.pageYOffset ||
                document.documentElement.scrollTop ||
                document.body.scrollTop;

                for (var key in popObj) {
                    if (popObj.hasOwnProperty(key)) {

                        var item = popObj[key];
                        var diff = item.absoluteTop - scrollTop;
                        if (diff < item.fixedTop) {
                            $('.' + key).css({
                                position: 'fixed',
                                top: item.fixedTop + 'px'
                            })
                        } else {
                            $('.' + key).css({
                                position: 'absolute',
                                top: item.absoluteTop + 'px'
                            })
                        }

                    }
                }
            }

            function initSide() {
                var sideList = ['side_menu_download', 'side_menu_contant']
                for (var index = 0; index < sideList.length; index++) {
                    (function (num) {
                        var item = sideList[index];
                        $(".icons_" + item + "").mouseenter(function () {
                            $("." + item + "").addClass("off");
                            $("." + item + "_on").removeClass("off")
                        })

                        $("." + item + "_on").mouseleave(function () {
                            $("." + item + "").removeClass("off")
                            $("." + item + "_on").addClass("off");
                        })

                        $('.side_menu_left').mouseleave(function(){
                            $("." + item + "").removeClass("off");
                            $("." + item + "_on").addClass("off");
                        });

                        $('.side_menu_right').mouseleave(function(){
                            $("." + item + "").removeClass("off");
                            $("." + item + "_on").addClass("off");
                        })

                    })(index)
                }
            }
        </script>
    </div>