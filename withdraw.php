<?php include("navbar-intern.php") ?>

    <div class="tab-content" id="tabTransfer">
        <div class="tabContentHeader">
            <div class="font-14" style="height: 54px">
                <div class="userActivityTitle" style="padding-left: 45px">WITHDRAW</div>
            </div>
        </div>
        <div class="line-divider section">
            <hr>
        </div>
        <form name="form" role="form" class="ng-pristine ng-valid-min ng-valid-max ng-invalid ng-invalid-required">

            <div class="radio-tile-group row" style="margin-top: 22px;display: flex">
                <div class="left-title font-14 text-blue">
                    <span>Country</span>*
                </div>
                <div class="w-290 h-30 font-14 mt-10" style="width: fit-content; width: -moz-fit-content">
                    <div class="user-bank-detail" style="margin-left: 15px">
                        <span>Indonesia</span>
                    </div>
                </div>
            </div>

            <div class="radio-tile-group row" style="margin-top: 26px">
                <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                    <span>Name Of Bank</span>&nbsp;*
                </div>
                <div class="custom-select w-300 font-14" style="margin-right: 25px">
                    <select required="" class="ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" aria-invalid="false">
                        <option value="S" data-translate="settings.dropdown.pleaseselect">--- Please Select ---</option>
                        <option value="O" data-translate="settings.dropdown.onlineBanking">BCA</option>
                        <option value="D" data-translate="settings.dropdown.cashDepositMachine">BNI</option>
                        <option value="A" data-translate="settings.dropdown.atmTransfer">MANDIRI</option>
                    </select>
                    <div class="select-selected" role="button" tabindex="0">
                        <div aria-hidden="false" class="">--- Please Select ---</div>
                        <div aria-hidden="true" class="ng-hide">--- Please Select ---</div>
                    </div>
                    <div class="select-items select-hide">
                        <div role="button" tabindex="0">--- Please Select ---</div>
                        <div role="button" tabindex="0">Online Banking</div>
                        <div role="button" tabindex="0">Cash Deposit Machine</div>
                        <div role="button" tabindex="0">ATM</div>
                    </div>
                </div>
            </div>

            <div class="radio-tile-group row" style="margin-top: 22px;display: flex">
                <div class="left-title font-14 text-blue">
                    <span>Bank Account Number</span>*
                </div>
                <div class="w-290 h-30 font-14 mt-10" style="width: fit-content; width: -moz-fit-content">
                    <div class="inputDeposit w-300 h-40 align-items-center">
                        <input class="ml-15 font-14 ng-pristine ng-untouched ng-valid ng-empty" id="referenceID" name="referenceID" placeholder="remarks" type="Reference" aria-invalid="false">
                    </div>
                </div>
            </div>

            <div class="radio-tile-group row" style="margin-top: 22px;display: flex">
                <div class="left-title font-14 text-blue">
                    <span>Bank Account Name</span>*
                </div>
                <div class="w-290 h-30 font-14 mt-10" style="width: fit-content; width: -moz-fit-content">
                    <div class="inputDeposit w-300 h-40 align-items-center">
                        <input class="ml-15 font-14 ng-pristine ng-untouched ng-valid ng-empty" id="referenceID" name="referenceID" placeholder="remarks" type="Reference" aria-invalid="false">
                    </div>
                </div>
            </div>
              <div class="radio-tile-group row" style="margin-top: 31px">
                    <div class="left-title font-14 text-blue">

                    </div>
                    <button class="gradientBtn" type="submit" style="font-size: inherit" disabled="disabled">SUBMIT</button>
                    <!---->

                    <!---->
                    <!---->
                </div>
        </form>
    </div>

    </div>
    </section>
    </div>
    </div>
    </div>
    <?php include('footer_sign.php') ?>