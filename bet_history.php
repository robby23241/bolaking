<?php include("navbar-intern.php") ?>

    <div class="tab-content" id="tabTransfer">
        <div class="tabContentHeader">
            <div class="font-14" style="height: 54px">
                <div class="userActivityTitle" style="padding-left: 45px">HISTORY</div>
            </div>
        </div>
        <div class="line-divider section">
            <hr>
        </div>
        <div class="container">

            <div class="form-main">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="text-uppercase"><strong>BET History</strong></h3>
                        <hr class="hr">
                    </div>
                </div>

                <div class="row">
                    <form name="form" role="form" class="ng-pristine ng-valid-min ng-valid-max ng-invalid ng-invalid-required">

                         <div class="radio-tile-group row" style="margin-top: 14px;display: flex; align-items: center"><div class="left-titleHistory font-14 text-blue" data-translate="history.startDate">Start Date</div>
                         <div class="datePickerBox w-130 h-40"><img class="datePicker" src="public/new_bk8/content/images/newSetting/tContent_historyContent_datePickerIcon.png" ng-click="vm.triggerDatePickerDropDown('#historyStartDate')" style="cursor: pointer" role="button" tabindex="0"> <input type="text" class="datepicker" id="historyStartDate" style="min-width: 0; width: 100%" readonly="true"></div>

                         <div class="text-blue font-14" style="margin-left: 13px; margin-right: 12px" data-translate="transaction.history.to">To</div>
                         <div class="datePickerBox w-130 h-40" style="margin-right: 24px">
                         	<img class="datePicker" src="public/new_bk8/content/images/newSetting/tContent_historyContent_datePickerIcon.png" style="cursor: pointer" role="button" tabindex="0">
                         	<input type="text" class="datepicker" id="historyEndDate" style="min-width: 0; width: 100%" readonly="true">
                         	<div class="contextmenu" style="left: 0px; top: 40px;"></div>
                         </div>

                         <div class="text-blue font-14" style="margin-left: 13px; margin-right: 12px" data-translate="transaction.history.to">
                     	<select name="provider" id="provider" class="form-control">
                            <option value="">provider.</option>
                            <option value="M8">M8</option>
                            <option value="HABANERO">Habanero</option>
                            <option value="IMONESB">IM Sportsbook</option>
                            <option value="TCGIBC">IBC</option>
                            <option value="PRAGMATIC">Pragmatic</option>
                            <option value="OG">Oriental Game</option>
                            <option value="RTG">Real Time Gaming</option>
                            <option value="UNITEDGAMING">United Gaming</option>
                            <option value="SA36">SA 36</option>
                            <option value="ONEGAMING">provider.ONEGAMING</option>
                            <option value="TPG">RP Fishing</option>                                       
                        </select>
                     </div>

                     
                         <button class="gradientBtn" ng-click="vm.search();" ng-disabled="vm.isRequesting">Search</button></div>
                     </div>
                     
                    </form>
                </div>
            </div>

            <div class="text-blue font-14" style="margin-left: 13px; margin-right: 12px" data-translate="transaction.history.to">

                No results found

            </div>
        </div>
    </div>

    </div>
    </section>
    </div>
    </div>
    </div>

    <script>
  $( function() {
    $( "#historyStartDate" ).datepicker();
  } );
  </script>

    <?php include('footer_sign.php') ?>
