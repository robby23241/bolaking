<?php include('base.php')?>
	<div ui-view="content">
    <div class="register_form">
        <div class="form_container">
            <div style="display: flex; margin-top: 30px">
                <div style="width: 32px">
                </div>
                <div class="member_benefits member_benefits2">
                   <div ng-hide="vm.success === 'OK'" aria-hidden="false" class=""><p data-translate="reset.request.messages.info">Enter the email address you used to register</p></div>
                </div>
                <div style="width: 35px; height: 1px"></div>
                <div class="form_section">
                    <div class="register_header">
                        <span style="width: 110px; height: 13px" class="text-light header-title" data-translate="register.createAccount">
                            Reset Your Password
                        </span>
                    </div>
                    <div>
                        <div class="data_input">
                            <form>
                                <div style="display: flex; height: 30px; margin-bottom: 10px">
                                    <div class="register-form-data">
                                        <span>Email</span>
                                    </div>
                                    <div class="register-input-div">
                                        <span style="color: red; padding: 0 5px 0 11px">*&nbsp;</span>
                                        <input type="text" name="email" class="form-control" placeholder="Email Address">
                                    </div>
                                </div>
                                <div style="display: flex; height: 30px; margin-top: 31px; margin-bottom: 25px">
                                    <div class="register-form-data"></div>
                                    <div class="register-input-div">
                                        <button type="submit" class="btn btn-primary" style="margin-left: 27px" disabled="disabled">
                                            Reset Password
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 100%; margin-top: 2%; margin-bottom: 35px" ng-show="vm.link != ''" aria-hidden="false" class=""><img ng-src="public/html/images/bk8-DBR-Register-en.jpg" style="display: block; margin: 0 auto" src="public/html/images/bk8-DBR-Register-en.jpg"></div>
        </div>
    </div>
</div>
<?php include('footer_sign.php') ?>