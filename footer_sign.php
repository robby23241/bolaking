<div ui-view="footer">
    <div class="copyRight">
        <div class="container">
            <div class="copyRightContent">
                <div class="left">
                    <div class="li" role="button" tabindex="0"><span data-translate="footer.about-us">About Us</span></div>
                    <div class="li" role="button" tabindex="0"><span data-translate="footer.livechat">Live Chat</span></div>
                    <div class="li" role="button" tabindex="0"><span data-translate="footer.contact-us">Contact Us</span></div>
                    <div class="li" role="button" tabindex="0"><span data-translate="footer.terms-and-condition">Terms &amp; Conditions</span></div>
                    <div class="li" aria-hidden="false"><a href="http://aff.bolaking.net/aff" target="_blank" style="font-size: 9px"><span style="font-size: 14px !important; color: #757575" data-translate="footer.affiliate">Affiliate</span></a></div>
                </div>
                <div class="right"><span data-translate="footer.all-right-reserved" data-translate-values="{ merchant: vm.globVar}">© 2019 BK8. All rights reserved.</span><i class="icons icons_footer_18"></i></div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="footertop">
                <div class="footertopContent">
                    <section class="footertopItem">
                        <div class="iconText">
                            <div class="icon"><i class="icons_aff"></i></div>
                            <p class="pheader" data-translate="footer.affiliate-program.header">Affiliate Program</p>
                            <p data-translate="footer.affiliate-program.content" data-translate-values="{merchant: vm.globVar}">Free to join affiliates program and earn commissions based.</p><a href="http://aff.bolaking.net/aff"><span data-translate="footer.joinNow">Join Now &gt;</span></a></div>
                        <!---->
                    </section>
                    <section class="footertopItem">
                        <div class="iconText">
                            <div class="icon"><i class="icons_vip"></i></div>
                            <p class="pheader" data-translate="footer.vip-program.header">VIP Program</p>
                            <p data-translate="footer.vip-program.content">Upgrade to VIP to enjoy exclusive benefit and earn rewards.</p><a href="/vip-home"><span data-translate="footer.applyNow">Apply Now &gt;</span></a></div>
                        <!---->
                    </section>
                    <section class="footertopItem">
                        <div class="iconText">
                            <div class="icon"><i class="icons icons_gift"></i></div>
                            <p class="pheader" data-translate="footer.special-promotion.header">Special Promotion</p>
                            <p data-translate="footer.special-promotion.content">Apply for special financing and earn rewards.</p><a href="promotion"><span data-translate="footer.playNow">Play Now &gt;</span></a></div>
                        <!---->
                    </section>
                    <section class="footertopItem">
                        <div class="iconText">
                            <div class="icon"><i class="icons icons_services"></i></div>
                            <p class="pheader" data-translate="footer.customer-service.header">Customer Service 24/7</p>
                            <p data-translate="footer.customer-service.content">Have a question? Call a Speciaist or chat online.</p><a onclick="load_livechat();"><span data-translate="footer.chatNow">Chat Now &gt;</span></a></div>
                        <!---->
                    </section>
                    <section class="footertopItem">
                        <div class="iconText">
                            <div class="icon"><i class="icons icons_live"></i></div>
                            <p class="pheader" data-translate="footer.live-streaming.header">Live Streaming</p>
                            <p data-translate="footer.live-streaming.content">Watch the live worldwide broadcast at any moment.</p><a href="livetv"><span data-translate="footer.watchNow">Watch Now &gt;</span></a></div>
                    </section>
                </div>
                <!-- for seo contents -->
                <div class="footertopContent"></div>
            </div>
        </div>
    </footer>
    <div id="contactUsContent" class="overlay"><a href="javascript:void(0)" class="overlay-close-lg" onclick="closeOverlay()">×</a>
        <div class="overlay-content">
            <div class="overlay-container">
                <div class="row" style="height: 15px"></div>
                <div style="text-align: right">
                    <div class="overlay-close-sm pull-right" onclick="closeOverlay()" data-translate="footer.x-close">X Close</div>
                </div>
                <div class="overlay-title" data-translate="footer.contact-us">Contact Us</div>
                <div class="row">
                    <div class="col-xs-12 text-justify">
                        <div class="overlay-body">
                            <table style="width: 100%">
                                <tbody>
                                    <!---->
                                    <tr>
                                        <td style="width: 36%; text-align: right">
                                            <span class="overlay-content-desc">WeChat</span>
                                        </td>
                                        <td style="width: 4%"></td>
                                        <td style="width: 60%; text-align: left">
                                            <span class="overlay-content-desc">bolakingcsmyr</span>
                                        </td>
                                    </tr>
                                    <!---->
                                    <tr>
                                        <td style="width: 36%; text-align: right">
                                            <span class="overlay-content-desc">WhatsApp</span>
                                        </td>
                                        <td style="width: 4%"></td>
                                        <td style="width: 60%; text-align: left">
                                            <span class="overlay-content-desc">+601157735870</span>
                                        </td>
                                    </tr>
                                    <!---->
                                    <tr>
                                        <td style="width: 36%; text-align: right">
                                            <span class="overlay-content-desc">Phone</span>
                                        </td>
                                        <td style="width: 4%"></td>
                                        <td style="width: 60%; text-align: left">
                                            <span class="overlay-content-desc">+60392121600</span>
                                        </td>
                                    </tr>
                                    <!---->
                                    <tr>
                                        <td style="width: 36%; text-align: right"></td>
                                        <td style="width: 4%"></td>
                                        <td style="width: 60%; text-align: left">
                                            <span class="overlay-content-desc overlay-content-livechat" role="button" tabindex="0">LIVE CHAT</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="text-align: right">
                        <div class="overlay-footer"><img class="logo-img" alt="logo" width="150" src="public/logo/logo_001_20190221070242.png"></div>
                    </div>
                </div>
                <div class="row" style="height: 15px"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function closeOverlay() {
            document.getElementById("contactUsContent").style.display = "none";
        }

        function contactUs() {
            closeOverlay();
            document.getElementById("contactUsContent").style.display = "block";
        }
    </script>
</div>

<!-- footer -->
<div ui-view="footer2">
    <footer>
        <div class="container">
            <div class="footerbottomempty" style="padding-top:38px">
            </div>

            <div class="brand-hr">
                <div class="footerbottomempty2 top-first-hr">
                </div>

                <div class="top-mid-hr">
                </div>

                <div class="footerbottomempty2 top-last-hr">
                </div>
            </div>

            <div class="brandContainer">
                <div class="leftInnerContainer">
                    <p class="gameTitle" data-translate="footer.game-title">
                        Gaming License
                    </p>
                    <a href="https://www.bk8.com/curacao.php" target="_blank">
                        <div class="gameImage">
                        </div>
                    </a>
                    <p class="brandGame" data-translate="footer.game-brand" data-translate-values="{ merchant: vm.globVar}">
                        BK8 is a Registered Trade Mark, brand and registered business 30, Ghar Id-Dud Street Sliema SLM1572, Malta. Regulated &amp; Licensed by the Government of Curacao and operates under the Master License of Gaming Services Provider, N.V. #365/JAZ
                    </p>
                </div>
                <div class="centerInnerContainer">
                    <a target="_blank" href="https://www.bk8dbr.com/">
                        <p class="brandSponsor" data-translate="footer.official-sponsor">
                            Official DBR Partner
                            <br>Spanish Football League
                            <br>2018/2019
                        </p>
                        <div class="brandImage">
                        </div>
                    </a>
                </div>
                <div class="rightInnerContainer">
                    <a href="https://www.johnterrybk8.com/" target="_blank">
                        <p class="ambassadorTitle" data-translate="footer.ambassador-title">Brand Ambassador</p>
                        <p class="ambassadorName" data-translate="footer.ambassador-name1">Former England</p>
                        <p class="ambassadorName" data-translate="footer.ambassador-name2">&amp; Chelsea Captain</p>
                        <p class="ambassadorName" data-translate="footer.ambassador-name3">John Terry</p>
                        <div class="ambassadorImage">
                        </div>
                    </a>
                </div>
            </div>
            <div class="footerbottomempty2">
            </div>

            <div class="footerbottom">
                <div class="footerbottomContent">
                    <section class="footerbottomItem">
                        <div class="title">
                            <span data-translate="footer.follow-us">
                                                                        Follow Us
                                                                    </span>
                        </div>
                        <div class="footerIconBox">
                            <a target="_blank" aria-hidden="true" class="ng-hide">
                                <i class="icons icons_fb"></i>
                            </a>
                            <a target="_blank" href="https://www.instagram.com/bk8asia/" aria-hidden="false" class="">
                                <i class="icons icons_insta"></i>
                            </a>
                            <a target="_blank" href="http://bit.ly/bk8youtuberedirect" aria-hidden="false" class="">
                                <i class="icons icons_youtube"></i>
                            </a>
                            <a target="_blank" aria-hidden="true" class="ng-hide">
                                <i class="icons icons_twitter"></i>
                            </a>
                        </div>
                    </section>

                    <section class="footerbottomItem">
                        <div class="title">
                            <span data-translate="footer.suggest-browser">Suggested Browser</span>
                        </div>
                        <div class="iconBox">
                            <i class="icons icons_chrome" style="cursor: default"></i>
                            <i class="icons icons_fox" style="cursor: default"></i>
                        </div>
                    </section>
                    <section class="footerbottomItem">
                        <div class="title">
                            <span data-translate="footer.payment-method">Payment Method</span>
                        </div>
                        <div class="iconBox">
                            <i class="icons icons_eeziepay" style="cursor: default"></i>
                            <i class="icons icons_help2pay" style="cursor: default"></i>
                        </div>
                    </section>
                    <section class="footerLicense">
                        <div class="licenseDiv">
                            <div class="title">
                                <span data-translate="footer.license">Licenses</span>
                            </div>
                            <div class="iconBox">
                                <i class="icons icons_pagcor" style="cursor: default"></i>
                                <i class="icons icons_bmm" style="cursor: default"></i>
                                <i class="icons icons_itechlabs" style="cursor: default"></i>
                                <i class="icons icons_globe" style="cursor: default"></i>
                                <i class="icons icons_verified" style="cursor: default"></i>
                                <i class="icons icons_tt" style="cursor: default"></i>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </footer>
</div>

<script type="text/javascript" charset="utf-8" src="public/javascript/init.js"></script>
<script src="public/template/new_bk8/scripts/src.js" type="text/javascript" charset="utf-8"></script>
<script src="public/template/new_bk8/scripts/settings.js" type="text/javascript" charset="utf-8"></script>
<script src="public/template/new_bk8/scripts/third-party-scripts.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 9688135;
    window.__lc.group = 1;
    window.__lc.chat_between_groups = false;
    (function() {
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
    })();
</script>
<script src="app/vendor.js"></script>
<!-- 
        <script src="app/app.js"></script> -->
<script type="text/javascript" charset="utf-8" src="public/javascript/core.js"></script>
<script src="public/javascript/clipboard.min.js"></script>
<script>
    new ClipboardJS('.cpybtn');
</script>

</body>

</html>