<?php include('base.php')?>

	<div ui-view="content">
		<div class="content new-bk8-setting">
			<div class="container container-user">
				<section class="profileBox">
					<div class="profileItemDetail">
						<img class="upperElementEightIcon" src="public/new_bk8/content/images/newSetting/user-normal.png">
							<div class="tooltip nonDiamond">
								<img alt="" src="public/new_bk8/content/images/newSetting/level-over-normal.png"> 
									<a href="/vip-home" data-translate="settings.abtVip">
										More About Vip &gt;
									</a>
							</div>
							<div class="rightContentBox1">
								<div class="light-blue font-14 text-bold">
									NORMAL
								</div>
								<div class="font-14 text-title">
									<span data-translate="global.menu.account.welcome">
										Welcome 
									</span>
								</div>
								<div class="light-gray font-12">
									
								</div>
							</div>
						</div>
						<div style="list-style: none">
							<div class="vl"></div>
						</div>
						<div class="profileItemDetail">
							<div class="leftContentBox2">
								<div class="title" data-translate="settings.mainwallet">
									Main Wallet
								</div>
								<div class="leftBottomContent dark-blue font-16 text-bold">IDR 0.00 <span class="reload active" style="margin-left: 10px" ng-click="vm.requestReloadWallets()" role="button" tabindex="0"><!-- Generator: Adobe Illustrator 23.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 18.5 18.5" xml:space="preserve"><g><path class="st0" d="M9.75,1.9l0.16-0.28l1.18-1.18c-2.87-0.6-5.98,0.21-8.21,2.45c-3.12,3.12-3.47,7.96-1.05,11.47l-1.26,1.26
									l4.05,0.58l-0.57-4.05l-1.03,1.03c-1.79-2.84-1.45-6.65,1.03-9.12C5.61,2.48,7.7,1.76,9.75,1.9L9.75,1.9z M9.75,1.9"></path><path class="st0" d="M17.35,2.31L13.3,1.73l0.58,4.05L15,4.66c2.31,2.89,2.13,7.12-0.55,9.8c-1.8,1.8-4.3,2.47-6.62,2.02L6.5,17.82
									c3.11,0.99,6.65,0.26,9.12-2.21c3.32-3.32,3.5-8.59,0.55-12.12L17.35,2.31z M17.35,2.31"></path></g></svg></span></div>
							</div>
							<div class="rightContentBox2">
								<button class="btn--restoreBtn">
									<img alt="restoreBtn" src="public/new_bk8/content/images/newSetting/upperElement_restoreIcon.png">
										<span class="pl-10 font-14" data-translate="settings.transferwalletv2">
											Restore
										</span>
								</button>
							</div>
						</div>
						<div class="profileItemDetail">
							<div class="leftContentBox3 font-14 text-title">
								
							</div>
						</div>
						<div class="profileItemDetail contentBox4">
							<ul class="breadcrumb font-14 float-right">
								<li role="button" tabindex="0">
									<a ui-sref="settingv2.deposit" href="/member-settings/deposit" style="color: blue;">
										<span data-translate="settings.tabs.heading.deposit">Deposit</span>
									</a>
								</li>
								<li role="button" tabindex="0">
									<a ui-sref="settingv2.transfer" href="/member-settings/transfer" style="">
										<span data-translate="settings.tabs.heading.transfer">Transfer</span>
									</a>
								</li>
								<li role="button" tabindex="0">
									<a ui-sref="settingv2.withdraw" href="/member-settings/withdraw">
										<span data-translate="settings.tabs.heading.withdraw">Withdraw</span>
									</a>
								</li>
								<li role="button" tabindex="0">
									<a ui-sref="settingv2.history" href="/member-settings/history">
										<span data-translate="settings.tabs.heading.history">History</span>
									</a>
								</li>
							</ul>
						</div>

				</section>
				<section class="userActivity">
					<div id="userActivity-nav">
						<ul class="userActivityMenuPart">
							<div class="font-14 mx-auto" style="height: 58px; margin: 0px">
								<div class="userActivityTitle" data-translate="settings.banking">BANKING</div>
							</div>
							<li class="userActivityMenuItem first current" style="margin-bottom: 12px" id="tab1" tab="deposit" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_deposit_White.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.deposit"><a href="/template_bolaking/deposit.php">Deposit</a></span>
							</li>
							<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab3" tab="withdraw" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_withdraw.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.withdraw"><a href="/template_bolaking/withdraw.php">Withdraw</a></span>
							</li>
							<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab4" tab="history" href="/member-settings/history" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_history.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.history"><a href="/template_bolaking/trans_history.php">Transaction History</a></span>
							</li>
							<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab4" tab="history" href="/member-settings/history" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_history.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.history"><a href="/template_bolaking/bet_history.php">Bet History</a></span>
							</li>
							<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab2" tab="transfer" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_transfer.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.transfer"><a href="/template_bolaking/transfer.php">Transaction Pending</a></span>
							</li>
							<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab2" tab="transfer" role="button" tabindex="0">
								<img src="public/new_bk8/content/images/newSetting/sidePanel_transfer.png"> 
									<span class="font-14" data-translate="settings.tabs.heading.transfer"><a href="/template_bolaking/transfer.php">Transaction Declined</a></span>
							</li>
							<div class="font-14 mx-auto" style="height: 59px;margin: 0px">
								<div class="userActivityTitleSecond" data-translate="settings.userProfile">User Profile</div>
							</div>
								<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab5" tab="inbox" href="/member-settings/inbox" role="button" tabindex="0">
									<img src="public/new_bk8/content/images/newSetting/sidePanel_messaging.png"> 
										<span class="font-14">Messaging</span>
								</li>
								<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab6" tab="profile" ui-sref="settingv2.profile" ng-click="vm.decideShowLoader('settingv2.profile');" role="button" tabindex="0">
									<img src="public/new_bk8/content/images/newSetting/sidePanel_editProfile.png"> 
										<span class="font-14" data-translate="settings.profiletab.myprofile"><a href="/template_bolaking/profile.php">My Profile</a></span>
								</li>
								<li class="userActivityMenuItem" style="margin-bottom: 12px" id="tab7" tab="change-password" href="/member-settings/change-password" role="button" tabindex="0">
									<img src="public/new_bk8/content/images/newSetting/sidePanel_changePassword.png"> 
										<span class="font-14" data-translate="settings.profiletab.changepass"><a href="/template_bolaking/change_password.php">Change Password</a></span>
								</li>
								<li class="userActivityMenuItem" id="tab8" tab="bank" role="button" tabindex="0">
									<img src="public/new_bk8/content/images/newSetting/sidePanel_bankDetails.png"> 
										<span class="font-14" data-translate="settings.profiletab.bank"><a href="/bolaking/bank_details.php">Banking Details</a></span>
								</li>
							</ul>
							<div class="tab-content ng-hide" style="position: relative; background-color: unset" aria-hidden="true">
								<div id="loader"></div>
							</div>
							
			