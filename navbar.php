<div ui-view="navbar">
        <div id="top">
            <div class="header">
                <div class="container" style="box-sizing: border-box;">
                    <div class="headerContent">
                        <a class="" href="#">
                            <img ng-src="public/logo/logo_001_20190221070242.png" alt="logo" src="public/logo/logo_001_20190221070242.png">
                        </a>
                        <div class="right">
                            <form class="loginBar">
                                <div class="mobile_nav"></div>
                                <div class="username">
                                    <input type="text" placeholder="Your Username" class="mainFont ng-pristine ng-untouched ng-valid ng-empty">
                                </div>
                                <div class="password">
                                    <input type="password" placeholder="Your Password" class="mainFont ng-pristine ng-untouched ng-valid ng-empty">
                                </div>
                                <button type="submit" class="login">
                                    <span>LOGIN</span>
                                </button> 
                                <button type="button" class="joinnow">
                                    <span>JOIN NOW</span>
                                </button>
                                <section class="tools mainFont">
                                    <div class="forgot mainFont">
                                        <span>Forgot 
                                        </span> 
                                        <span class="mainFont" style="cursor: pointer">
                                            <u>username</u> 
                                        </span> 
                                        <span data-translate="global.menu.account.or">or</span> 
                                        <span class="mainFont" style="cursor: pointer">
                                            <u>password</u>?
                                        </span>
                                    </div>
                                    <div class="mainFont">29/05/2019 02:01:10 (GMT+8)</div>
                                </section>
                            </form>
                            <!----><!---->
                            <div class="languageTog" style="cursor: pointer">
                                <i id="countryLanguageIcon" class="icons region icons_header_malay">    
                                </i> 
                                <i class="icons icons_header_more"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu">
                <div class="menuBox">
                    <div class="container">
                        <div class="menuContent">
                            <!-- hardcode first item must be home logo -->
                            <a class="icon_menu_logo" ng-href="/" href="/"></a>
                            <ul class="menunav">
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="2" class="menunav" ui-sref="mobile" eat-click-if="vm.checkState(menu)" href="/mobile">
                                        <div title="Mobile" style="margin: 0 21px">
                                            <img ng-show="menu.isImageMenu &amp;&amp; !vm.imageHover" src="assets/images/mobile.png" http-src="menu-mobile" style="margin-right: -8px" aria-hidden="false" class="">
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="3" class="menunav" ui-sref="-" eat-click-if="vm.checkState(menu)">
                                        <div title="SPORTS" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="SPORTS" aria-hidden="false" class="">SPORTS</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="4" class="menunav" ui-sref="livecasino({provider: 'Allbet'})" eat-click-if="vm.checkState(menu)" href="/casino/live/Allbet">
                                        <div title="LIVE CASINO" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="LIVE CASINO" aria-hidden="false" class="">LIVE CASINO</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="5" class="menunav" ui-sref="slot({provider: 'spadeGaming'})" eat-click-if="vm.checkState(menu)" href="/slots/spadeGaming">
                                        <div title="SLOTS" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="SLOTS" aria-hidden="false" class="">SLOTS</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="6" class="menunav" ui-sref="poker" eat-click-if="vm.checkState(menu)" href="/poker">
                                        <div title="POKER" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="POKER" aria-hidden="false" class="">POKER</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="7" class="menunav" ui-sref="main-fishing" eat-click-if="vm.checkState(menu)" href="/main-fishing">
                                        <div title="FISHING" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="FISHING" aria-hidden="false" class="">FISHING</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="9" class="menunav" ui-sref="promotion" eat-click-if="vm.checkState(menu)" href="/promotion">
                                        <div title="PROMOS" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="PROMOS" aria-hidden="false" class="">PROMOS</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()" class="showLive">
                                    <a id="10" class="menunav" ui-sref="livetv" eat-click-if="vm.checkState(menu)" href="/livetv">
                                        <div title="TV" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="TV" aria-hidden="false" class="">TV</span>
                                        </div>
                                        <i class="icons icons_menu_live" ng-show="menu.isLive" aria-hidden="false"></i>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()" class="vip">
                                    <a id="13" class="menunav" ui-sref="viphome" eat-click-if="vm.checkState(menu)" href="/vip-home">
                                        <div title="VIP" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="VIP" aria-hidden="false" class="">VIP</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                                <li ng-repeat="menu in vm.menus" ng-class="{showLive:menu.isLive, vip:menu.name=='VIP'}" ng-mouseenter=" menu.name ==='Mobile' ?  vm.hoverImage() :  vm.displaySubmenu(menu) " ng-mouseleave="vm.isNotHover()">
                                    <a id="16" class="menunav" ui-sref="-" eat-click-if="vm.checkState(menu)">
                                        <div title="BRAND AMBASSADOR" style="margin: 0 21px">
                                            <span ng-show="!menu.isImageMenu" title="BRAND AMBASSADOR" aria-hidden="false" class="">BRAND AMBASSADOR</span>
                                        </div>
                                    </a>
                                </li>
                                <!---->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="submenu">
        <div id="submenu-container" class="container navbar-center">
            <!---->
            <div id="submenuRow" class="submenuRow" ng-repeat="submenuArray in vm.submenus2d">
                <div id="submenuRowContainer" class="submenuRowContainer navbarCenterRow navbarSubRow6" ng-class="{navbarCenterRow:vm.submenus2d[1] == null, navbarSubRow4:vm.subMenuRow4Class, navbarSubRow6:vm.subMenuRow6Class}">
                    <!---->
                    <section ng-repeat="submenu in submenuArray" ng-class="{off:vm.addOff &amp;&amp; vm.isNotMouseOver($parent.$index, $index)}" class="on" ng-mouseover="vm.subMenuMouseOver($parent.$index, $index)" ng-mouseleave="vm.subMenuMouseLeave()">
                        <a ui-sref="sport" eat-click-if="vm.checkState(submenu)" href="/cmd368">
                            <div style="display: block">
                                <div id="submenu-CMD368" class="firms firms_subbutton_c_sport"></div>
                                <div style="text-align: center; color: #0185be; font-size: 13px; margin-top: -5px">c-Sports</div>
                            </div>
                        </a>
                    </section>
                    <!---->
                    <section ng-repeat="submenu in submenuArray" ng-class="{off:vm.addOff &amp;&amp; vm.isNotMouseOver($parent.$index, $index)}" class="on" ng-mouseover="vm.subMenuMouseOver($parent.$index, $index)" ng-mouseleave="vm.subMenuMouseLeave()">
                        <a ui-sref="ibc-sport" eat-click-if="vm.checkState(submenu)">
                            <div style="display: block">
                                <div id="submenu-IBC" class="firms firms_subbutton_i_sport"></div>
                                <div style="text-align: center; color: #0185be; font-size: 13px; margin-top: -5px">i-Sports</div>
                            </div>
                        </a>
                    </section>
                    <!---->
                    <section ng-repeat="submenu in submenuArray" ng-class="{off:vm.addOff &amp;&amp; vm.isNotMouseOver($parent.$index, $index)}" class="on" ng-mouseover="vm.subMenuMouseOver($parent.$index, $index)" ng-mouseleave="vm.subMenuMouseLeave()">
                        <a ui-sref="sbo-sport" eat-click-if="vm.checkState(submenu)" href="/sbo">
                            <div style="display: block">
                                <div id="submenu-SBO" class="firms firms_subbutton_s_sport"></div>
                                <div style="text-align: center; color: #0185be; font-size: 13px; margin-top: -5px">s-Sports</div>
                            </div>
                        </a>
                    </section>
                    <!---->
                    <section ng-repeat="submenu in submenuArray" ng-class="{off:vm.addOff &amp;&amp; vm.isNotMouseOver($parent.$index, $index)}" class="on" ng-mouseover="vm.subMenuMouseOver($parent.$index, $index)" ng-mouseleave="vm.subMenuMouseLeave()">
                        <a ui-sref="vsports" eat-click-if="vm.checkState(submenu)" href="/virtualsports-landing">
                            <div style="display: block">
                                <div id="submenu-BetRadar" class="firms firms_subbutton_v_sport"></div>
                                <div style="text-align: center; color: #0185be; font-size: 13px; margin-top: -5px">Virtual Sports</div>
                            </div>
                        </a>
                    </section>
                    <!---->
                </div>
            </div>
            <!---->
        </div>
    </div>
    