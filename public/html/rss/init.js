/*
function onloadEvent() {
    var contentDivs = document.getElementsByClassName('identifier');
    for (var i = 0; i < contentDivs.length; i++) {
        var titleDiv = contentDivs[i].getElementsByTagName('h2')[0];
        var title = titleDiv.getElementsByTagName('a')[0].text;
        var descDiv = contentDivs[i].getElementsByTagName('p')[0];
        var aLink = contentDivs[i].getElementsByTagName('input')[0].value;
        var imgElem = null;
        
        // check if descDiv contains img
        var imgs = descDiv.getElementsByTagName('img');
        if (imgs && imgs.length > 0) {
            imgElem = imgs[0];
            imgElem.style.marginRight = '20px';
            imgElem.style.float = 'left';
            
            // remove extra img
            if (imgs.length > 1) {
                for (var j = imgs.length - 1; j >= 1; j--) {
                    imgs[j].remove();
                }
            }
            
            contentDivs[i].classList.add('withImg');
            //contentDivs[i].style.height = '135px';
        } else {
            contentDivs[i].classList.add('withoutImg');
        }
        
        var pElem = document.createElement('p');
        var titleElem = document.createElement('h2');
        titleElem.classList.add('title');
        titleElem.innerHTML = title;
        pElem.appendChild(titleElem);
        
        var textContent = descDiv.textContent;
        if (textContent.indexOf('(function(') > 0) {
            textContent = removeTextBetweenTwoKeywords(textContent, '(function(', ');');
        }
        var textNode = document.createTextNode(textContent);
        
        pElem.appendChild(textNode);
        
        // remove original h2 title and p description
        titleDiv.parentNode.removeChild(titleDiv);
        descDiv.parentNode.removeChild(descDiv);
        
        if (imgElem != null) {
            contentDivs[i].appendChild(imgElem);
        }
        contentDivs[i].appendChild(pElem);
        
        //var linkElem = document.createElement('a');
        //linkElem.innerHTML = 'Read more';
        //linkElem.addEventListener('click', function() {
        //  window.parent.homepageSportNewsPopupCallbackFromIframe(aLink);
        //});
        //contentDivs[i].appendChild(linkElem);
    }
    
    function removeTextBetweenTwoKeywords(oriText, firstKey, secondKey) {
        var subtext = textContent.split(firstKey);
        var firstText = subtext[0];
        var subtext2 = subtext[1].split(secondKey);
        var secondText = subtext2[1];
        
        return firstText + secondText;
    }
}
*/
function onloadEvent() {
    var contentDivs = document.getElementsByClassName('identifier');
    for (var i = 0; i < contentDivs.length; i++) {
        var titleDiv = contentDivs[i].getElementsByTagName('h2')[0];
        var title = titleDiv.getElementsByTagName('a')[0].text;
        var descDiv = contentDivs[i].getElementsByTagName('p')[0];
        var aLink = contentDivs[i].getElementsByTagName('input')[0].value;
        var imgElem = null;

        // check if descDiv contains img
        var imgs = descDiv.getElementsByTagName('img');
        if (imgs && imgs.length > 0) {
            imgElem = imgs[0];
            imgElem.style.marginRight = '20px';
            imgElem.style.float = 'left';

            // remove extra img
            if (imgs.length > 1) {
                for (var j = imgs.length - 1; j >= 1; j--) {
                    imgs[j].remove();
                }
            }

            contentDivs[i].classList.add('withImg');
            //contentDivs[i].style.height = '135px';
        } else {
            contentDivs[i].classList.add('withoutImg');
        }

        var pElem = document.createElement('p');
        pElem.className = "pIdentifier";
        var titleElem = document.createElement('h2');
        titleElem.classList.add('title');
        titleElem.innerHTML = title;
        pElem.appendChild(titleElem);

        var textContent = descDiv.textContent;
        if (textContent.indexOf('(function(') > 0) {
            textContent = removeTextBetweenTwoKeywords(textContent, '(function(', ');');
        }
        var textNode = document.createTextNode(textContent);

        pElem.appendChild(textNode);

        // remove original h2 title and p description
        titleDiv.parentNode.removeChild(titleDiv);
        descDiv.parentNode.removeChild(descDiv);

        if (imgElem != null) {
            contentDivs[i].appendChild(imgElem);
        }
        contentDivs[i].appendChild(pElem);

        // Get URL language
        function parse_query_string() {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            var query_string = {};
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                var key = decodeURIComponent(pair[0]);
                var value = decodeURIComponent(pair[1]);
                // If first entry with this name
                if (typeof query_string[key] === "undefined") {
                    query_string[key] = decodeURIComponent(value);
                    // If second entry with this name
                } else if (typeof query_string[key] === "string") {
                    var arr = [query_string[key], decodeURIComponent(value)];
                    query_string[key] = arr;
                    // If third or later entry with this name
                } else {
                    query_string[key].push(decodeURIComponent(value));
                }
            }
            return query_string;
        }

        // Change read more to page's language
        var url = parse_query_string();
        var urlLang = url.lang;
        urlLanguage = url.lang;
        var readmoreLang = "";

        if (urlLang == "EN_MY") {
            readmoreLang = "Read more";
        } else if (urlLang == "MS_MY") {
            readmoreLang = "Maklumat lanjut";
        } else if (urlLang == "ZH_MY") {
            readmoreLang = "更多";
        } else if (urlLang == "ID_ID") {
            readmoreLang = "Info lebih lanjut";
        } else if (urlLang == "TH_TH") {
            readmoreLang = "ข้อมูลเพิ่มเติม";
        } else {
            readmoreLang = "Read more";
        }

        var linkElem = document.createElement('a');
        linkElem.innerHTML = readmoreLang;
        linkElem.style.color = "#33a3dc";
        linkElem.style.fontFamily = "mainFont";
        linkElem.style.cursor = "pointer";
        linkElem.addEventListener('click', function(event) {
            var parentElem = event.target.parentElement;
            if (parentElem) {
                var childList = parentElem.children;
                var rssLink = childList[childList.length - 1].value;

                var param = {
                    "rssLink": rssLink
                };

                window.parent.homepageSportNewsPopupCallbackFromIframe(param);
            }
        });
        contentDivs[i].appendChild(linkElem);

        var rssFullContentLink = document.createElement('input');
        rssFullContentLink.type = 'hidden';
        rssFullContentLink.value = aLink;
        contentDivs[i].appendChild(rssFullContentLink);

        function removeTextBetweenTwoKeywords(oriText, firstKey, secondKey) {
            var subtext = textContent.split(firstKey);
            var firstText = subtext[0];
            var subtext2 = subtext[1].split(secondKey);
            var secondText = subtext2[1];

            return firstText + secondText;
        }
    }

    var pIdentifiers = document.getElementsByClassName('pIdentifier');
    for (var i = 0; i < pIdentifiers.length; i++) {
        truncateWithEllipsis(pIdentifiers[i]);
    }
    var h2Identifiers = document.getElementsByClassName('title');
    for (var i = 0; i < h2Identifiers.length; i++) {
        truncateTitleEllipsis(h2Identifiers[i]);
    }
}

function truncateWithEllipsis(elem) {
    if (elem.clientHeight > 100) {
        elem.style.overflow = "hidden";
        elem.style.height = "100px";

        if (urlLanguage == 'ZH_MY') {
            while (elem.scrollHeight > elem.offsetHeight) {

                if (elem.scrollHeight > 500) {
                    elem.innerHTML = elem.innerHTML.substring(0, elem.innerHTML.length - 500) + '...';

                } else if (elem.scrollHeight > 115) {
                    elem.innerHTML = elem.innerHTML.substring(0, elem.innerHTML.length - 40) + '...';

                } else {
                    elem.innerHTML = elem.innerHTML.substring(0, elem.innerHTML.length - 5) + '...';

                }
            }

        } else {
            var wordArray = elem.innerHTML.split(' ');
            while (elem.scrollHeight > elem.offsetHeight) {
                wordArray.pop();
                elem.innerHTML = wordArray.join(' ') + '...';
            }
        }

    }
}

function truncateTitleEllipsis(elem) {
    if (elem.clientHeight > 35) {
        elem.style.height = "35px";
        elem.style.overflow = "hidden";
        if (urlLanguage == 'ZH_MY') {

            while (elem.scrollHeight > elem.offsetHeight) {

                if (elem.scrollHeight > 35) {
                    elem.innerHTML = elem.innerHTML.substring(0, elem.innerHTML.length - 2) + '...';

                } else {
                    elem.innerHTML = elem.innerHTML.substring(0, elem.innerHTML.length - 1) + '...';

                }
            }
        } else {
            var wordArray = elem.innerHTML.split(' ');

            while (elem.scrollHeight > elem.offsetHeight) {
                wordArray.pop();
                elem.innerHTML = wordArray.join(' ') + '...';
            }
        }

    }
}