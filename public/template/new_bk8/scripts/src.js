var useNewBk8Setting = true; // simple flag to switch between new bk8 setting and settingv2

// override index.html getTemplateSrc
getTemplateSrc = function (src, type, part, state) {
    var templateSrc = null;
    if (part === 'empty') {
        templateSrc = 'template/' + template + 'web/empty.html';
        return templateSrc;
    }

    if (type === 'navbar') {
        templateSrc = getNavbarTemplateSrc(part, state);
    } else if (type === 'content') {
        templateSrc = getContentTemplateSrc(part, state);
    } else if (type === 'banner') {
        templateSrc = getBannerTemplateSrc(part, state);
    } else if (type === 'footer') {
        templateSrc = getFooterTemplateSrc(part, state);
    } else if (type === 'footer2') {
        templateSrc = getFooter2TemplateSrc(part, state);
    } else if (type === 'popup') {
        templateSrc = getPopupTemplateSrc(part, state);
    }

    if (templateSrc === null) {
        return src;
    } else if (templateSrc.indexOf('.html') < 0) {
        templateSrc = 'template/' + template + '/web/empty.html';
        return templateSrc;
    } else {
        return templateSrc;
    }
}

function getNavbarTemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === null) {
        src += '/navbar.html';
    }
    return src;
}

function getContentTemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === 'home') {
        src += '/home.html';
    } else if (part === 'register') {
        src += '/register/register.html';
    } else if (part === 'register.success2') {
        src += '/register/register-success.html';
    } else if (part === 'promotion') {
        src += '/promotion/promotion.html';
    } else if (part === 'reset-request'){
        src += '/reset/reset-request.html';
    } else if (part === 'reset-finish'){
        src += '/reset/reset-finish.html';
    } else if (part === 'download') {
        src += '/misc/download.html';
    } else if (part === 'livetv') {
        src += '/misc/livetv.html';
    } else if (part === 'livescore') {
        src += '/misc/livescore.html';
    }
    //poker
    else if (part === 'idn-poker-casino') {
        src += '/poker/poker.html';
    }
    // settings
    else if (state !== undefined && state !== null) {
        if (state === 'settingv2.history') {
            if (part === 'settingv2.profile.tab_layer') {
                return 'app/entities/common/empty.html';
            } else if (part === 'settingv2.profile.history') {
            	if(useNewBk8Setting)
            		src += '/new-bk8-setting/history/new-bk8-history.html';
            	else
            		src += '/setting/history/history-tab.html';
            }
        } else if (state === 'settingv2.inbox') {
            if (part === 'settingv2.profile.tab_layer') {
                src += '/setting/profile/profile_tab.html';
            }
        }
    } else if (part === 'settings') {
        if(useNewBk8Setting)
        	src += '/new-bk8-setting/new-bk8-setting.html';
        else
        	src += '/setting/setting.html';
    } else if (part === 'settingv2.profile.tab_layer') {
        src += '/setting/profile/profile_tab.html';
    } else if (part === 'settingv2.profile') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/profile/new-bk8-profile.html';
    	else
    		src += '/setting/profile/member-profile.html';
    } else if (part === 'settingv2.profile.chgpwd') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/change-password/new-bk8-change-password.html';
    	else
    		src += '/setting/profile/change-member-password.html';
    } else if (part === 'settingv2.profile.chgproviderpwd') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/change-provider-password/new-bk8-change-provider-password.html';
    	else
    		src += '/setting/profile/change-provider-password.html';
    } else if (part === 'settingv2.profile.bank') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/bank-details/new-bk8-bank-details.html';
    	else
    		src += '/setting/profile/bank.html';
    } else if (part === 'settingv2.inbox') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/messaging/new-bk8-inbox.html';
    	else
    		src += '/setting/inbox/inbox.html';
    } else if (part === 'settingv2.withdraw') {
        if(useNewBk8Setting)
        	src += '/new-bk8-setting/withdraw/new-bk8-withdraw.html';
        else
        	src += '/setting/withdraw/withdraw.html';
    } else if (part === 'settingv2.withdraw.tab_layer') {
        src += '/setting/withdraw/withdraw-tab.html';
    } else if (part === 'settingv2.transfer') {
    	if(useNewBk8Setting)
    		src += '/new-bk8-setting/transfer/new-bk8-transfer.html';
    	else
    		src += '/setting/transfer/transfer.html';
    } else if (part === 'settingv2.profile.history') {
        src += '/setting/history/history-tab.html';
    } else if (part === 'settingv2.profile.history.withdraw_deposit') {
        src += '/setting/history/withdraw-deposit-history.html';
    } else if (part === 'settingv2.profile.history.transfer') {
        src += '/setting/history/transfer-history.html';
    } else if (part === 'settingv2.profile.history.promotion') {
        src += '/setting/history/promotion-applied-history.html';
    } else if (part === 'settingv2.profile.history.rebate') {
        src += '/setting/history/rebate-history.html';
    } else if (part === 'settingv2.profile.history.reward') {
        src += '/setting/history/reward-history.html';
    } else if (part === 'settingv2.deposit.tab_layer') {
        src += '/setting/deposit/deposit-tab.html';
    } else if (part === 'settingv2.deposit') {
        if(useNewBk8Setting)
        	src += '/new-bk8-setting/deposit/new-bk8-deposit.html';
        else
        	src += '/setting/deposit/bank-transfer.html';
    } else if (part === 'settingv2.deposit.online_transfer') {
        if(useNewBk8Setting)
        	src += '/new-bk8-setting/deposit/new-bk8-deposit.html';
        else
        	src += '/setting/deposit/online-transfer.html';
    }
    // sports
    else if (part === 'sbo-sport') {
        src += '/sport/sbo-landing.html';
    } else if (part === 'cmd-sport') {
        src += '/sport/cmd-landing.html';
    } else if (part === 'br-sport-landing') {
        src += '/sport/br-landing.html';
    } else if (part === 'br-sport') {
        src += '/sport/br-gameplay.html';
    }
    // live casino
    else if (part === 'all-live-casino') {
        src += '/live-casino/casino.html';
    }
    // slot
    else if (part === 'common-slot') {
        src += '/slots/common-slot.html';
    } else if (part === 'ultimate-slot') {
        src += '/slots/ultimate-landing.html';
    } else if (part === '918kiss-slot') {
        src += '/slots/918kiss-landing.html';
    } else if (part === 'gg-fishing-slot') {
        src += '/slots/fishing/gg-fishing-landing.html';
    } else if (part === 'pt-fishing-slot') {
        src += '/slots/fishing/pt-fishing-landing.html';
    } else if (part === 'sg-fishing-slot') {
        src += '/slots/fishing/sg-fishing-landing.html';
    } else if (part === 'main-fishing-slot') {
        src += '/slots/fishing/main-fishing-landing.html';
    }
    //promotion
    else if (part === 'promotion') {
        src += '/promotion/promotion.html';
    }
    // lottery
    else if (part === 'lottery-qqthai') {
    	src += '/lottery/lottery-qqthai.html';
    }
    // vip
    else if (part === 'viplogin') {
    	src += '/vip/vip-login.html';
    } else if (part === 'viploginsuccess') {
    	src += '/vip/vip-login-success.html';
    } else if (part === 'vip') {
    	src += '/vip/vip.html';
    }
    return src;
}

function getBannerTemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === null) {
        src += '/banner.html';
    }
    return src;
}

function getFooterTemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === null) {
        src += '/footer.html';
    } else if (part === 'mhome') {
        src += '/footer.html';
    }
    return src;
}

function getFooter2TemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === null) {
        src += '/footer2.html';
    }
    return src;
}

function getPopupTemplateSrc(part, state) {
    var src = 'template/' + template + '/web';

    if (part === 'changeLanguageDialog') {
        src += '/misc/change-language-dialog.html';
    } else if (part === 'walletTransferDialog') {
		src += '/misc/transfer-dialog.html';
	} else if (part === 'promotionMoreInfo') {
		src += '/promotion/more-info-dialog.html';
	} else if (part === 'promotionApplyNow') {
		src += '/promotion/apply-now-dialog.html';
	} else if (part === "customizedAnnouncement") {
		src += '/misc/announcement.html';
	} else if (part === 'popup-angpow') {
		src += '/misc/popup-angpow.html';
	} else if (part === 'addMemberBankDialog') {
		src += '/misc/add-member-bank.html';
	} else if (part === 'deleteMemberMessage') {
    	src += '/new-bk8-setting/messaging/new-bk8-delete-message.html';
	} else if (part === 'sportnewsReadMore') {
		src += '/misc/sportnews-popup.html';
    }
    return src;
}

//override index.html getTemplateSrc
getMobileTemplateSrc = function (src, type, part, state) {
    var templateSrc = null;
    if (part === 'empty') {
        templateSrc = 'template/' + template + '/web/empty.html';
        return templateSrc;
    }

    if (state === 'mhome') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/mobile-index.html';
    	} else if (type === 'navbar' && part === 'navbar') {
    		templateSrc = 'template/' + template + '/mobile/navbar.html';
    	} else if (type === 'sidebar' && part === 'sidebar') {
    		templateSrc = 'template/' + template + '/mobile/sidebar.html';
    	} else if (type === 'content' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/home.html';
    	} else if (type === 'footer' && part === 'footer') {
    		templateSrc = 'template/' + template + '/mobile/bottom-navbar.html';
    	} else if (type === 'overlay' && part === 'overlay') {
    		templateSrc = 'template/' + template + '/mobile/overlay.html';
    	} else if (type === 'popup' && part === 'popup-angpow') {
    		templateSrc = 'template/' + template + '/mobile/popup/popup-angpow.html';
    	}
    } else if (state === 'mlogin') {
    	if (type === 'content' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/misc/login.html';
    	}
    } else if (state === 'mfunds') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/funds/funds-landing.html';
    	} else if (type === 'header' && part === 'header') {
            templateSrc = 'template/' + template + '/mobile/funds/wallet-control.html';
        } else if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/funds.html';
        }
    } else if (state === 'mmyaccount') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/account/myaccount.html';
    	}
    } else if (state === 'mregister') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/account/register.html';
    	}
    } else if (state === 'mregister-success') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/account/register-success.html';
    	}
    } else if (state === 'password') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/password/password.html';
    	}
        
    } else if (state === 'memberprofile') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/member/member-profile.html';
    	}
    
    } else if (state === 'mmembermessage') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/inbox/member-message.html';
    	}else if (type === 'inbox-popup' && part === 'inbox-popup') {
    		templateSrc = 'template/' + template + '/mobile/inbox/member-message-delete-dialog.html';
    	}
        
    } else if (state === 'providerpassword') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/provider-password/provider-password.html';
    	}
    } else if (state === 'mbankingdetails') {
    	if (type === 'index' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/bank/bank-details.html';
        }
        
    } else if (state === 'mbankingdetails-addMemberBank') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/deposit/add-member-bank-dialog.html';
        }
    } else if (state === 'inboxcontent') {
    	if (type === 'index' && part === 'navbar') {
    		templateSrc = 'template/' + template + '/mobile/navbar/navbar-fullscreen.html';
        } else if (type === 'index' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/inbox/member-message-dialog.html';
        }
        
    } else if (state === 'mtransfer') {
        if (type === 'index' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/funds-landing.html';
        } else if (type === 'header' && part === 'header') {
            templateSrc = 'template/' + template + '/mobile/funds/wallet-control.html';
        } else if (type === 'content' && part === 'content') {
            // return empty content since header already can replace content
            templateSrc = 'template/' + template + '/web/empty.html';
        }
    } else if (state === 'mdeposit') {
        if (type === 'index' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/funds-landing.html';
        } else if (type === 'header' && part === 'header') {
            templateSrc = 'template/' + template + '/mobile/funds/wallet-control.html';
        } else if (type === 'content' && part === 'content-index') {
            templateSrc = 'template/' + template + '/mobile/funds/deposit/deposit-landing.html';
        } else if (type === 'content' && part === 'online-transfer') {
            templateSrc = 'template/' + template + '/mobile/funds/deposit/online-transfer.html';
        } else if (type === 'content' && part === 'bank-transfer') {
            templateSrc = 'template/' + template + '/mobile/funds/deposit/bank-transfer.html';
        }
    } else if (state === 'mdeposit-addMemberBank') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/deposit/add-member-bank-dialog.html';
        }
    } else if (state === 'mwithdraw') {
        if (type === 'index' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/funds-landing.html';
        } else if (type === 'header' && part === 'header') {
            templateSrc = 'template/' + template + '/mobile/funds/wallet-control.html';
        } else if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/withdraw/withdraw.html';
        }
    } else if (state === 'mhistory') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/history.html';
        }
    } else if (state === 'mtransferhistory') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/transfer-history.html';
        }
    } else if (state === 'mwithdraw-deposit-history') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/withdraw-deposit-history.html';
        } else if (type === 'navbar' && part === 'navbar') {
            templateSrc = 'template/' + template + '/mobile/navbar.html';
        }
    } else if (state === 'mpromotion-claim-history') {
        if (type == 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/promotion-claim-history.html';
        }
    } else if (state === 'mrebate-history') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/rebate-history.html';
        }
    } else if (state === 'mrewards-history') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/funds/history/rewards-history.html';
        }
    } else if (state === 'mlanguage') {
    	if (type === 'content' && part === 'content') {
    		templateSrc = 'template/' + template + '/mobile/misc/language.html';
    	}
    } else if (state === 'mpromotion') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/promotion/promotion.html';
        } else if (type === 'content' && part === 'dialog'){
            templateSrc = 'template/' + template + '/mobile/promotion/promotion-apply.html';            
        }
    } else if (state === 'mcontact') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/contactus/contact-us.html';
        }
    } else if (state === 'mcasino') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/common-casino-landing.html';
        }
    } else if (state === 'mcasino.ab') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/allbet-landing.html';
        }
    } else if (state === 'mcasino.gp') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/gameplay-landing.html';
        }
    } else if (state === 'mcasino.ag') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/asia-gaming-landing.html';
        }
    } else if (state === 'mcasino.pt') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/playtech-landing.html';
        }
    } else if (state === 'mcasino.ug') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/sexy-baccarat-landing.html';
        }
    } else if (state === 'mcasino.idn') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/idn-poker.html';
        }
    } else if (state === 'mcasino.mgp') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/casino/microgamingplus-landing.html';
        }
    } else if (state === 'mslot') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/slot/slot.html';
        }
    } else if (state === 'mlivetv') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/livetv/livetv.html';
        }
    } else if (state === 'mdownload') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/misc/download.html';
        }
    } else if (state === 'm-game-redirect-launcher') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/misc/game-redirect-page.html';
        } else if (type === 'transfer-popup' && part === 'transfer-popup') {
            templateSrc = 'template/' + template + '/mobile/misc/transfer-popup.html';
        }
    } else if (state === 'm918kiss') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/slot/918kiss.html';
        }
    } else if (state === 'mreset-request') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/reset/reset-accountpassword.html';
        }
    } else if (state === 'mviplogin') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/vip/viplogin.html';
        }
    } else if (state === 'mviploginsuccess') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/mobile/vip/viplogin-success.html';
        }
    } else if (state === 'proceedlogin') {
        if (type === 'login' && part === 'login') {
    		templateSrc = 'template/' + template + '/mobile/misc/proceed-login.html';
        } else if (type === 'login' && part === 'custom-modal') {
        	templateSrc = 'template/' + template + '/mobile/misc/proceed-login-customized-modal.html';
        }
    }
    
    if (templateSrc === null) {
        return src;
    } else if (templateSrc.indexOf('.html') < 0) {
        templateSrc = 'template/' + template + '/web/empty.html';
        return templateSrc;
    } else {
        return templateSrc;
    }
}

getAffiliateTemplateSrc = function (src, type, part, state) {
	var templateSrc = null;

    if (state === 'ahome') {
        if (type === 'navbar' && part === 'navbar') {
            templateSrc = 'template/' + template + '/affiliate/navbar.html';
        } else if (type === 'banner' && part === 'banner') {
            templateSrc = 'template/' + template + '/affiliate/banner.html';
        } else if (type === 'footer' && part === 'footer') {
            templateSrc = 'template/' + template + '/affiliate/footer.html';
        } else if (type === 'footer2' && part === 'footer2') {
            templateSrc = 'template/' + template + '/web/empty.html';
        } else if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/home.html';
        }
    } else if (state === 'affregister') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/register/register.html';
        }
    } else if (state === 'affregister.success') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/register/register-success.html';
        }
    } else if (state === 'a-contact-us') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/contact-us.html';
        }
    } else if (state === 'a-commission-plans') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/commission-plans.html';
        }
    } else if (state === 'a-faq') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/faq.html';
        }
    } else if (state === 'affSettings') {
        // settings main page
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/settings/settings.html';
        }
        // reports tab
        else if (type === 'reports_tab' && part === 'reports_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/reports.html';
        } else if (type === 'reports_tab' && part === 'monthly_win_loss_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/monthly-win-loss.html';
        } else if (type === 'reports_tab' && part === 'monthly_commission_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/monthly-commission.html';
        } else if (type === 'reports_tab' && part === 'withdrawal_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/withdrawal.html';
        } else if (type === 'reports_tab' && part === 'deposit_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/deposit.html';
        } else if (type === 'reports_tab' && part === 'transfer_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/transfer.html';
        } else if (type === 'reports_tab' && part === 'member_subtab') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/member.html';
        } else if (type === 'reports_tab' && part === 'commission_details_popup') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/monthly-commission-details-dialog.html';
        } else if (type === 'reports_tab' && part === 'shared_cost_details_popup') {
            templateSrc = 'template/' + template + '/affiliate/settings/reports/monthly-commission-shared-cost-details-dialog.html';
        } else if (type === 'reports_tab' && part === 'commission_provider_details_popup') {
        	templateSrc = 'template/' + template + '/affiliate/settings/reports/commission-provider-details-dialog.html';
        } else if (type === 'reports_tab' && part === 'commission_other_cost_popup') {
        	templateSrc = 'template/' + template + '/affiliate/settings/reports/commission-shared-cost-dialog.html';
        }
        // profile tab
        else if (type === 'profile_tab' && part === 'profile_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/profile/profile.html';
        } else if (type === 'profile_tab' && part === 'password_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/profile/password.html';
        } else if (type === 'profile_tab' && part === 'bank_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/profile/bank.html';
        }
        // withdraw tab
        else if (type === 'withdraw_tab' && part === 'withdraw_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/withdraw/withdraw.html';
        }
        // deposit tab
        else if (type === 'deposit_tab' && part === 'deposit_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/deposit/deposit.html';
        }
        // transfer tab
        else if (type === 'transfer_tab' && part === 'transfer_tab') {
            templateSrc = 'template/' + template + '/affiliate/settings/transfer/transfer.html';
        } else if (type === 'transfer_tab' && part === 'transfer_popup') {
            templateSrc = 'template/' + template + '/affiliate/settings/transfer/transfer-popup.html';
        }
    } else if (state === 'a-gallery') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/gallery.html';
        }
    } else if (state === 'a-overview') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/overview.html';
        }
    } else if (state === 'aff-requestReset') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/request-reset-password.html';
        }
    } else if (state === 'aff-finishReset') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/finish-reset-password.html';
        }
    } else if (state === 'changeLanguageDialog') {
        if (type === 'content' && part === 'content') {
            templateSrc = 'template/' + template + '/affiliate/misc/language-popup.html'
        } else if (type === 'content' && part === 'popup-window') {
            templateSrc = 'template/' + template + '/affiliate/misc/language-popup-window.html';
        }
    }

    if (templateSrc === null) {
        return src;
    } else if (templateSrc.indexOf('.html') < 0) {
        templateSrc = 'template/' + template + '/web/empty.html';
        return templateSrc;
    } else {
        return templateSrc;
    }
}

//override state controller definition
getCustomController = function(oriCtrl, state, part) {
    if (state === 'mdeposit' && part === 'index') {
        return '';
    } else if (state === 'mwithdraw-deposit-history' && part === 'navbar') {
        return 'MNavbarController';
    } else if (state === 'mpromotion-claim-history' && part === 'navbar') {
    	return 'MNavbarController';
    } else if (state === 'mrebate-history' && part === 'navbar') {
    	return 'MNavbarController';
    } else if (state === 'scr888' && part === 'content') {
        return 'SCR2888Controller';
    } else if (state === 'settingv2.deposit' && part === 'whole-page') {
    	if(useNewBk8Setting)
    		return 'NewBk8DepositController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.profile.history' && part === 'whole-page') {
    	if(useNewBk8Setting)
    		return 'NewBk8HistoryController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.profile') {
    	if(useNewBk8Setting)
    		return 'NewBk8ProfileController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.transfer') {
    	if(useNewBk8Setting)
    		return 'NewBk8TransferController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.withdraw') {
    	if(useNewBk8Setting)
    		return 'NewBk8WithdrawController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.inbox') {
    	if(useNewBk8Setting)
    		return 'NewBk8InboxController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.profile.chgpwd') {
    	if(useNewBk8Setting)
    		return 'NewBk8ChangePasswordController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.profile.bank') {
    	if(useNewBk8Setting)
    		return 'NewBk8BankDetailsController';
    	else
    		return oriCtrl;
    } else if (state === 'settingv2.profile.chgproviderpwd') {
    	if(useNewBk8Setting)
    		return 'NewBk8ChangeProviderPasswordController';
    	else
    		return oriCtrl;
    } else if (state === 'mfunds') {
        if (part === 'index') {
            return '';
        } else if (part === 'header') {
            return 'MWalletControlController';
        } else if (part === 'mfunds') {
            return 'MFundsController';
        }
    } else if (state === 'mwithdraw') {
        if (part === 'index') {
            return '';
        } else if (part === 'header') {
            return 'MWalletControlController';
        } else if (part === 'content') {
            return 'MTransactionWithdrawalController';
        }
    }
    return '';
}
