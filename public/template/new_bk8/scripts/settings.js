templateSettings = {
	"navbarBalanceChecking": 1,
	"Home-loadSlideImage": 1,
	"Home-customText": 1,
	"Footer-customText": 1,
	"Promotion-customText": 1,
	"Settings-customText": 1,
	"Settings-customProviderList": 1,
	"MobileHome-customText": 1,
	"MobileSidebarBalanceChecking": 1,
	"Banner-contactBanner": 1,
	"Settings-defaultInit": 1,
	"Home-getAccount": 1,
	"Home-loadBody": 1,
	"Slot-separateTopBottomGameList": 1,
	"Slot-displayJackpot": 0,
	"MNavbarTopRightCornerFunc": "Messages",
	'Home-hotGames': 1
}

var isTranslated = false;

templateText = {
	"ZH_CN": {
		
	}
}

function getCurrentTime () {
	var date = new Date();
    var sec_num = parseInt(date.getTime() / 1000, 10); // don't forget the second param
    var displayHour = date.getHours();
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (displayHour < 10) {displayHour = "0"+displayHour;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    
    var offset = date.getTimezoneOffset();
    var sign = offset < 0? '+' : '-';
    offset = Math.abs(offset);
    var timezone = sign + (offset / 60 | 0);
    return displayHour + ':' + minutes + ':' + seconds + "(GMT" + timezone + ")";
};


navbarControllerInit = function () {
	if (!affPortal) {
		setInterval(function () {
			$(".timeNow").text(getCurrentTime());
		}, 1000);
		
		TweenMax.lagSmoothing(0, null);
	}
}

navbarControllerCustomGetLanguage = function() {
	var countryLanguageIcon = document.getElementById('countryLanguageIcon');
	if (countryLanguageIcon) {
		countryLanguageIcon.className = 'icons region';
	
		if (!affPortal) {
			if (langCurrency === 'MYR') {
				countryLanguageIcon.className += ' icons_header_malay';
			} else if (langCurrency === 'IDR') {
				countryLanguageIcon.className += ' icons_header_indo';
			} else if (langCurrency === 'THB') {
				countryLanguageIcon.className += ' icons_header_thai';
			} else if (langCurrency === 'SGD') {
				countryLanguageIcon.className += ' icons_header_singa';
			}
		} else {
			if (langCurrency === 'MYR') {
				countryLanguageIcon.src = 'public/new_bk8/content/images/affiliate/lang_my.png';
			} else if (langCurrency === 'IDR') {
				countryLanguageIcon.src = 'public/new_bk8/content/images/affiliate/lang_id.png';
			} else if (langCurrency === 'THB') {
				countryLanguageIcon.src = 'public/new_bk8/content/images/affiliate/lang_thai.png';
			} else if (langCurrency === 'SGD') {
				countryLanguageIcon.src = 'public/new_bk8/content/images/affiliate/lang_sg.png';
			}
		}
	}
}

getTemplateCustomCountryIconClass = function(languageObj) {
	if (!affPortal) {
		if (languageObj.countryName === 'Malaysia') {
			return 'icons icons_language_malay';
		} else if (languageObj.countryName === 'Indonesia') {
			return 'icons icons_language_indo';
		} else if (languageObj.countryName === 'Thailand') {
			return 'icons icons_language_thai';
		} else if (languageObj.countryName === 'Singapore') {
			return 'icons icons_language_singa';
		}
	} else {
		var id = 'langPopup-' + languageObj.countryName;
		var imgElem = document.getElementById(id);
		if (imgElem) {
			if (languageObj.countryName === 'Malaysia') {
				imgElem.src = 'public/new_bk8/content/images/affiliate/lang_my.png';
			} else if (languageObj.countryName === 'Indonesia') {
				imgElem.src = 'public/new_bk8/content/images/affiliate/lang_id.png';
			} else if (languageObj.countryName === 'Thailand') {
				imgElem.src = 'public/new_bk8/content/images/affiliate/lang_thai.png';
			} else if (languageObj.countryName === 'Singapore') {
				imgElem.src = 'public/new_bk8/content/images/affiliate/lang_sg.png';
			}
		}
	}
};

getMTemplateCustomCountryIconClass = function(languageObj) {
	if (languageObj.countryName === 'Malaysia') {
		return 'icons icon_language_malay';
	} else if (languageObj.countryName === 'Indonesia') {
		return 'icons icon_language_indo';
	} else if (languageObj.countryName === 'Thailand') {
		return 'icons icon_language_thai';
	} else if (languageObj.countryName === 'Singapore') {
		return 'icons icon_language_singa';
	}
};

getTemplateCustomLanguageIsSelectedClass = function (languageObj) {
	if (languageObj.countryLanguageKey === countryLanguageKey) {
		return 'mainFont on';
	} else {
		return 'mainFont';
	}
};

function getSubmenuClassName(submenu, category) {
	if (category === 'SL') {
		if (submenu === 'SpadeGaming') {
			return 'firms firms_spadegaming';
		} else if (submenu === 'PlayTech') {
			return 'firms firms_playtech';
		} else if (submenu === 'MicroGaming') {
			return 'firms firms_microgaming';
		} else if (submenu === 'TTG') {
			return 'firms firms_ttg';
		} else if (submenu === 'Ultimate') {
			return 'firms firms_ultimate';
		} else if (submenu === 'GamePlay') {
			return 'firms firms_gameplay';
		} else if (submenu === 'AsiaGaming') {
			return 'firms firms_asiagaming';
		} else if (submenu === '918Kiss' || submenu === '918Kiss2') {
			return 'firms firms_kiss918';
		} else if (submenu === 'PlayNGo') {
			return 'firms firms_playngo';
		} else if (submenu === 'MicroGamingPlus') {
			return 'firms firms_mgp';
		}
	}
	else if (category === 'C') {
		if (submenu === 'Allbet') {
			return 'firms firms_casino_subbutton_allbet';
		} else if (submenu === 'GoldDeluxe') {
			return 'firms firms_casino_subbutton_gd';
		} else if (submenu === 'Bamako' || submenu === 'Bamako2') {
			return 'firms firms_casino_subbutton_evo';
		} else if (submenu === 'GamePlay') {
			return 'firms firms_casino_subbutton_gp';
		} else if (submenu === 'DreamGaming') {
			return 'firms firms_casino_subbutton_dream';
		} else if (submenu === 'AsiaGaming') {
			return 'firms firms_casino_subbutton_ag';
		} else if (submenu === 'PlayTech') {
			return 'firms firms_casino_subbutton_pt';
		} else if (submenu === 'SexyBaccarat') {
			return 'firms firms_casino_subbutton_sexy';
		} else if (submenu === 'MicroGamingPlus') {
			return 'firms firms_casino_subbutton_mgp';
		}
	} 
	else if (category === 'S') {
		if (submenu === 'CMD368') {
			return 'firms firms_subbutton_c_sport';
		} else if (submenu === 'BetRadar') {
			return 'firms firms_subbutton_v_sport';
		} else if (submenu === 'IBC') {
			return 'firms firms_subbutton_i_sport';
		} else if (submenu === 'SBO' || submenu === 'SBOBET') {
			return 'firms firms_subbutton_s_sport';
		}
	}
	else if (category === 'F') {
		if (submenu === 'GG Fishing') {
			return 'firms firms_fishing_subbutton_gg_gaming';
		} else if (submenu === 'PT Fishing') {
			return 'firms firms_fishing_subbutton_cashfish';
		} else if (submenu === 'SG Fishing') {
			return 'firms firms_fishing_subbutton_sg_fishing';
		}
	}
	else if (category === 'L') {
		if (submenu === 'QQThai Lottery') {
			return 'firms_lottery_subbutton_qqthai';
		}
	}
	return '';
}

function populateCustomSubmenu(menus, submenus, subParam) {
	setTimeout(function(){
		for (var i = 0; i < submenus.length; i++) {
			var submenuElem = document.getElementById('submenu-' + submenus[i].name);
			if (submenuElem) {
				var className = getSubmenuClassName(submenus[i].name, subParam);
				submenuElem.className = className;
			}
		}
	}, 0);
}

templateNavbarGetMenu = function() {
	var menuList = [
		// { "id": 1, "name": "Home", "content": "global.menu.home", "submenu": null, "sref": "home", "isImageMenu": true, "imageMenuSrc": "menu-home", "isLive": false, "isNew": false, "provider": null }
		{ "id": 2, "name": "Mobile", "content": "global.menu.mobile", "submenu": null, "sref": "mobile", "isImageMenu": true, "imageMenuSrc": "menu-mobile", "imageMenuHoverSrc": "menu-mobile-hover","isLive": false, "isNew": false, "provider": null }
		,{ "id": 3, "name": "Sportsbook", "content": "global.menu.sportsbook", "submenu": "S", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 4, "name": "Casino", "content": "global.menu.casino", "submenu": "C", "sref": "livecasino({provider: 'Allbet'})", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 5, "name": "Slot", "content": "global.menu.slot", "submenu": "SL", "sref": "slot({provider: 'spadeGaming'})", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 6, "name": "Poker", "content": "global.menu.poker", "submenu": null, "sref": "poker", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": "IDN" }
		,{ "id": 7, "name": "Fishing", "content": "global.menu.fishing", "submenu": "F", "sref": "main-fishing", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 8, "name": "918Kiss", "content": "global.menu.scr888", "submenu": null, "sref": "918kiss", "isImageMenu": true, "imageMenuSrc": "menu-scr", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 9, "name": "Promotions", "content": "global.menu.promotions", "submenu": null, "sref": "promotion", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 10, "name": "LiveTV", "content": "global.menu.tv", "submenu": null, "sref": "livetv", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": true, "isNew": false, "provider": null }
		,{ "id": 11, "name": "LiveScore", "content": "global.menu.score", "submenu": null, "sref": "livescore", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": true, "isNew": false, "provider": null }
		// SUP-122, hide affiliate from menu and only display in affiliate footer
		//,{ "id": 12, "name": "Affiliate", "content": "global.menu.affiliate", "submenu": null, "sref": "ahome", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 13, "name": "VIP", "content": "global.menu.vip", "submenu": null, "sref": "viphome", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		// special menu for testing please use negative id
		//,{ "id": -99, "name": "Affiliate", "content": "global.menu.affiliate", "submenu": null, "sref": "ahome", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "provider": null }
		,{ "id": 15, "name": "Lottery", "content": "global.menu.lottery", "submenu": "L", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
		,{ "id": 16, "name": "Brand Ambassador", "content": "global.menu.brandambassador", "submenu": "B", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
	];
	
	return menuList;
};

templateNavbarGetSubmenu = function(category) {
	if (category === 'SL') {
		var slotSubmenu = [
			{ "id": 1, "name": "SpadeGaming", "content": "global.submenu.slots.spadeGaming", "category": "SL", "provider": "SG", "sref": "slot({provider: 'spadeGaming'})", "httpSrc": "submenu-sg" }
			,{ "id": 2, "name": "PlayTech", "content": "global.submenu.slots.playtech", "category": "SL", "provider": "PT", "sref": "slot({provider: 'playTech'})", "httpSrc": "submenu-slot-pt" }
			,{ "id": 3, "name": "MicroGaming", "content": "global.submenu.slots.microgaming", "category": "SL", "provider": "MG", "sref": "slot({provider: 'microGaming'})", "httpSrc": "submenu-mg" }
			,{ "id": 4, "name": "TTG", "content": "global.submenu.slots.ttg", "category": "SL", "provider": "TTG", "sref": "slot({provider: 'toptrendgaming'})", "httpSrc": "submenu-ttg" }
			,{ "id": 5, "name": "Ultimate", "content": "global.submenu.slots.ultimate", "category": "SL", "provider": "UL", "sref": "ultimate", "httpSrc": "submenu-ultimate" }
			,{ "id": 6, "name": "GamePlay", "content": "global.submenu.slots.gp", "category": "SL", "provider": "GP", "sref": "slot({provider: 'gameplay'})", "httpSrc": "submenu-gp" }
			,{ "id": 7, "name": "AsiaGaming", "content": "global.submenu.slots.ag", "category": "SL", "provider": "AG", "sref": "slot({provider: 'asia-gaming'})", "httpSrc": "submenu-ag" }
			,{ "id": 8, "name": "918Kiss", "content": "global.submenu.slots.918kiss", "category": "SL", "provider": "SCR", "sref": "918kiss", "httpSrc": "submenu-scr" }
			,{ "id": 9, "name": "PlayNGo", "content": "global.submenu.slots.playNGo", "category": "SL", "provider": "PG", "sref": "slot({provider: 'playNGo'})", "httpSrc": "submenu-pg" }
			,{ "id": 10, "name": "MicroGamingPlus", "content": "global.submenu.slots.microgaming", "category": "SL", "provider": "MGP", "sref": "slot({provider: 'microGamingPlus'})", "httpSrc": "submenu-mg" }
			// special menu for testing please use negative id
			//,{ "id": -99, "name": "918Kiss", "content": "global.submenu.slots.scr888", "category": "SL", "provider": "SCR", "sref": "918kiss", "httpSrc": "submenu-scr" }
    	];
		
		return slotSubmenu;
	}
	else if (category === 'C') {
		var casinoSubmenu = [
			{ "id": 1, "name": "Allbet", "content": "global.submenu.casino.allbet", "category": "C", "provider": "AB", "sref": "livecasino({provider: 'allbet'})", "httpSrc": "submenu-ab" }
			,{ "id": 2, "name": "GoldDeluxe", "content": "global.submenu.casino.goldDeluxe", "category": "C", "provider": "GD", "sref": "livecasino({provider: 'gold-deluxe'})", "httpSrc": "submenu-gd" }
			,{ "id": 3, "name": "Bamako", "content": "global.submenu.casino.evo", "category": "C", "provider": "BMK", "sref": "livecasino({provider: 'evolution-gaming'})", "httpSrc": "submenu-eg" }
			,{ "id": 4, "name": "GamePlay", "content": "global.submenu.casino.gamePlay", "category": "C", "provider": "GP", "sref": "livecasino({provider: 'gameplay'})", "httpSrc": "submenu-casino-gp" }
			,{ "id": 5, "name": "DreamGaming", "content": "global.submenu.casino.dreamGame", "category": "C", "provider": "DG", "sref": "livecasino({provider: 'dream-gaming'})", "httpSrc": "submenu-dg" }
			,{ "id": 6, "name": "AsiaGaming", "content": "global.submenu.casino.asiaGaming", "category": "C", "provider": "AG", "sref": "livecasino({provider: 'asia-gaming'})", "httpSrc": "submenu-casino-ag" }
			,{ "id": 7, "name": "PlayTech", "content": "global.submenu.slots.playtech", "category": "C", "provider": "PT", "sref": "livecasino({provider: 'playtech'})", "httpSrc": "submenu-casino-pt" }
			,{ "id": 8, "name": "SexyBaccarat", "content": "global.submenu.casino.sexyBaccarat", "category": "C", "provider": "UG", "sref": "livecasino({provider: 'sexy-baccarat'})", "httpSrc": "submenu-ug" }
			,{ "id": 9, "name": "Bamako2", "content": "global.submenu.casino.evo", "category": "C", "provider": "BMK2", "sref": "livecasino({provider: 'evolution-gaming'})", "httpSrc": "submenu-eg" }
			,{ "id": 10, "name": "MicroGamingPlus", "content": "global.submenu.slots.microgaming", "category": "C", "provider": "MGP", "sref": "livecasino({provider: 'microGamingPlus'})", "httpSrc": "submenu-mg" }
    	];
		
		return casinoSubmenu;
	}
	else if (category === 'S') {
		var sportsSubmenu = [
			{ "id": 1, "name": "CMD368", "content": "global.submenu.vsports.cmd368", "category": "S", "provider": "CMD", "sref": "sport", "httpSrc": "submenu-cmd" }
			,{ "id": 2, "name": "BetRadar", "content": "global.submenu.vsports.vsports", "category": "S", "provider": "BR", "sref": "vsports", "httpSrc": "submenu-br" }
			,{ "id": 3, "name": "IBC", "content": "global.submenu.sport.ibc", "category": "S", "provider": "IBC", "sref": "ibc-sport", "httpSrc": "submenu-ibc" }
			,{ "id": 4, "name": "SBO", "content": "global.submenu.sport.sbo", "category": "S", "provider": "SBO", "sref": "sbo-sport", "httpSrc": "submenu-sbo" }
			,{ "id": 5, "name": "SBOBET", "content": "global.submenu.sport.sbo", "category": "S", "provider": "SBO", "sref": "sbo-sport-bet", "httpSrc": "submenu-sbo" }
    	];
		
		return sportsSubmenu;
	}
	else if (category === 'F') {
		var fishingSubmenu = [
			{ "id": 1, "name": "GG Fishing", "content": "global.submenu.fishing.gg", "category": "F", "provider": "GG", "sref": "main-fishing", "httpSrc": "submenu-gg" }
			,{ "id": 2, "name": "PT Fishing", "content": "global.submenu.fishing.pt", "category": "F", "provider": "PT", "sref": "main-fishing", "httpSrc": "submenu-pt" }
			,{ "id": 3, "name": "SG Fishing", "content": "global.submenu.fishing.sg", "category": "F", "provider": "SG", "sref": "main-fishing", "httpSrc": "submenu-sg" }
    	];
		
		return fishingSubmenu;
	}
	else if (category === 'L') {
		var lotterySubmenu = [
			{ "id": 1, "name": "QQThai Lottery", "content": "global.submenu.lottery.qqthai", "category": "L", "provider": "QQT", "sref": "lottery-qqt", "httpSrc": "submenu-qqt" }
		];
		
		return lotterySubmenu;
	}
};

templateNavbarGetAffiliateMenu = function() {
	var affiliateMenu = [
		{ "id": 1, "name": "Home", "content": "global.menu.home", "submenu": null, "sref": "ahome", "isImageMenu": true, "imageMenuSrc": "menu-home", "isLive": false, "provider": null },
		{ "id": 2, "name": "PortalHome", "content": "global.menu.portal-home", "submenu": "PortalHome", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 3, "name": "Report", "content": "global.menu.report", "submenu": null, "sref": "affSettings({view: 'reports'})", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 4, "name": "Products", "content": "global.menu.products", "submenu": null, "sref": "a-products", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 5, "name": "Commission Plans", "content": "global.menu.commission-plans", "submenu": null, "sref": "a-commission-plans", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 6, "name": "FAQ", "content": "global.menu.faq", "submenu": null, "sref": "a-faq", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 7, "name": "Contact", "content": "global.menu.contact-us", "submenu": null, "sref": "a-contact-us", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 8, "name": "Overview", "content": "global.menu.overview", "submenu": null, "sref": "a-overview", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 9, "name": "Gallery", "content": "global.menu.gallery", "submenu": null, "sref": "a-gallery", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 10, "name": "Brochure", "content": "global.menu.brochure", "submenu": "AffBrochure", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null }
	];
	return affiliateMenu;
}

templateCustomAfterLoginEvent = function() {
	var url = document.location.href;
    if ((url.length > 2 && url.slice(-2) !== '/m') || url.indexOf("/m/") === -1) {
    	if(affPortal) {
    		window.location.href = "aff/settings/deposit/cash";
    	} else {
    		window.location.href = "/member-settings/deposit";
    	}
    }
};

templateSettingTabIndicator = function(tabname, statename) {
	if (tabname === 'profileTab') {
		if (statename === 'settingv2.profile' || statename === 'settingv2.profile.chgpwd'
			|| statename === 'settingv2.profile.chgproviderpwd' || statename === 'settingv2.profile.bank'
			|| statename === 'settingv2.inbox') {
			return true;
		}
	} else if (tabname === 'depositTab') {
		if (statename === 'settingv2.deposit' || statename === 'settingv2.deposit.online-transfer') {
			return true;
		}
	} else if (tabname === 'withdrawTab') {
		if (statename === 'settingv2.withdraw') {
			return true;
		}
	} else if (tabname === 'transferTab') {
		if (statename === 'settingv2.transfer') {
			return true;
		}
	} else if (tabname === 'inboxTab') {
		if (statename === 'settingv2.inbox') {
			return true;
		}
	} else if (tabname === 'historyTab') {
		if (statename === 'settingv2.history') {
			return true;
		}
	}
	// subtab indicator
	else if (tabname === 'profileMainSubtab') {
		if (statename === 'settingv2.profile') {
			return true;
		}
	} else if (tabname === 'profileChgpwdSubtab') {
		if (statename === 'settingv2.profile.chgpwd') {
			return true;
		}
	} else if (tabname === 'profileBankSubtab') {
		if (statename === 'settingv2.profile.bank') {
			return true;
		}
	} else if (tabname === 'profileHistorySubtab') {
		if (statename === 'settingv2.history') {
			return true;
		}
	} else if (tabname === 'bankOnlineTransferSubtab') {
		if (statename === 'settingv2.deposit.online-transfer') {
			return true;
		}
	} else if (tabname === 'bankDepositSubtab') {
		if (statename === 'settingv2.deposit') {
			return true;
		}
	} else if (tabname === 'withdrawSubtab') {
		if (statename === 'settingv2.withdraw') {
			return true;
		}
	}
	
	return false;
};

// translate variable to correct value
function getTemplateTextJson() {
	if (!isTranslated) {
		var regex = new RegExp('{MERCHANT}', 'g');
		var temp = JSON.stringify(templateText).replace(regex, globMerchantName);
		templateText = JSON.parse(temp);
		isTranslated = true;
	}
	return templateText;
}

templateSlotInit = function(provider) {
	$(document).ready(function() {
		var providerId = '';
		if (provider === 'SG') {
			providerId = 'sg_game_icon';
		} else if (provider === 'PT') {
			providerId = 'pt_game_icon';
		} else if (provider === 'MG') {
			providerId = 'mg_game_icon';
		} else if (provider === 'TTG') {
			providerId = 'ttg_game_icon';
		} else if (provider === 'GP') {
			providerId = 'gp_game_icon';
		} else if (provider === 'AG') {
			providerId = 'ag_game_icon';
		} else if (provider === 'PG') {
			providerId = 'pg_game_icon';
		} else if (provider === 'MGP') {
			providerId = 'mgp_game_icon';
		}
		
		var providerElem = $('#' + providerId);
		providerElem.addClass('on');
		
		$('#providerListbox').animate({scrollLeft: providerElem.position().left}, 500);
	});
}

templateSlotSlideProvider = function(direction) {
	// temporary hardcode most left and right
	if (direction === 'left') {
		$('#providerListbox').animate({scrollLeft: $('#sg_game_icon').position().left}, 500);
	} else if (direction === 'right') {
		$('#providerListbox').animate({scrollLeft: $('#pg_game_icon').position().left}, 500);
	}
}

templateCasinoInit = function(provider) {
	// $(document).ready(function() {
	// 	if (provider === 'AB') {
	// 		$("#ab_casino").addClass("on");
	// 	} else if (provider === 'GD') {
	// 		$("#gd_casino").addClass("on");
	// 	} else if (provider === 'EG') {
	// 		$("#eg_casino").addClass("on");
	// 	} else if (provider === 'GP') {
	// 		$("#gp_casino").addClass("on");
	// 	} else if (provider === 'DG') {
	// 		$("#dg_casino").addClass("on");
	// 	} else if (provider === 'AG') {
	// 		$("#ag_casino").addClass("on");
	// 	} else if (provider === 'PT') {
	// 		$("#pt_casino").addClass("on");
	// 	} else if (provider === 'UG') {
	// 		$("#ug_casino").addClass("on");
	// 	}
	// });
}

templateSlotWinnerInit = function(winList) {
	var pageSwitch = $(".pageSwitch");
    pageSwitch.find(".page_pre, .page_next").click(function(){
        $("#sliderWinner").trigger("doswitch");
    });

    $("#sliderWinner").on("switch", function(evt, i){
        $(".nowPage").text(i + 1);
    });
	
	if (winList.length > 0) {
		$(".icons_Latest_Winner_bg").addClass("on");
	}
}

getTemplateText = function(type) {
	// temporary hardcode ZH_CN first
	return getTemplateTextJson()['ZH_CN'][type];
}

function updateFormFieldTips(className, validation, errorMsg) {
	var elem = document.getElementsByClassName(className)[0];
	if (elem.nextElementSibling !== null
		&& $.grep(elem.nextElementSibling.classList, function(e) { return e === 'tips'; }).length > 0) {
		elem.nextElementSibling.style.display = 'none';
	}
	
	var validationElem = elem.nextElementSibling !== null ? $.grep(elem.nextElementSibling.classList, function(e) { return e === 'error-tips'; }) : null;
	var isExist = false;
	if (validationElem !== null && validationElem.length > 0) {
		isExist = true;
		validationElem = elem.nextElementSibling;
		
		if (validation) {
			validationElem.classList.add('valid-tips');
    	} else {
    		validationElem.classList.remove('valid-tips');
    	}
		
		validationElem.innerHTML = errorMsg;
	} else {
		var newValidationElem = document.createElement('span');
		newValidationElem.classList.add('error-tips');
		
		if (validation) {
			newValidationElem.classList.add('valid-tips');
    	} 
		
		newValidationElem.innerHTML = errorMsg;
		
		elem.parentNode.insertBefore(newValidationElem, elem.nextSibling);
	}
}

registerFormChanged = function(type, scope) {
	var className = '';
	var validation = false;
	var errorMsg = '';
	
	var registerAccount = scope.vm.registerAccount;
	var confirmPassword = scope.vm.confirmPassword;
	var customText = scope.vm.customText;
	var phone = scope.vm.phone;
	
	if (type === 'login') {
		className = 'registerFormLogin';
		if (registerAccount.login.length === 0) {
			errorMsg = customText.error.usernameEmpty;
		} else if (registerAccount.login.length < 4) {
			errorMsg = customText.error.usernameMinLength;
		} else {
			validation = true;
		}
	} else if (type === 'password') {
		className = 'registerFormPassword';
		if (registerAccount.password.length === 0) {
			errorMsg = customText.error.passwordEmpty;
		} else if (registerAccount.password.length < 6 || registerAccount.password.length > 12) {
			errorMsg = customText.error.passwordMinLength;
		} else {
			validation = true;
		}
	} else if (type === 'confirmPassword') {
		className = 'registerFormConfirmPassword';
		if (registerAccount.password !== confirmPassword) {
			errorMsg = customText.error.passwordNotSame;
		} else {
			validation = true;
		}
	} else if (type === 'name') {
		className = 'registerFormName';
		if (registerAccount.fullName.length === 0 || registerAccount.fullName.match(/\d+/g)) {
			errorMsg = customText.error.nameInvalid;
		} else {
			validation = true;
		}
	} else if (type === 'email') {
		className = 'registerFormEmail';
		var mail = registerAccount.email.split('@');
		if (registerAccount.email.length === 0 || mail.length !== 2 || mail[1].split('.').length !== 2 || mail[1].split('.')[1].length === 0) {
			errorMsg = customText.error.emailInvalid;
		} else {
			validation = true;
		}
	} else if (type === 'phone') {
		className = 'registerFormPhone';
		if (phone.length === 0 || !phone.match(/^[+]?[0-9]+$/)) {
			errorMsg = customText.error.phoneInvalid;
		} else {
			validation = true;
		}
	}
	
	updateFormFieldTips(className, validation, errorMsg);
}

populateMenuNgClass = function(menu) {
	if (menu.name === 'Home') {
		return 'nav-home cur';
	}
	return '';
}

getNotificationTemplate = function(msg, extraClass) {
	var classes = 'notification-inner';
	if(extraClass != null)
		classes += ' ' + extraClass;
	var param = {
		isCustomHtml: true,
		customHtmlCode: '<div class=\"notification-container\">'
			+ '<button type=\"button\" class=\"notification-close\" onclick=\"this.parentNode.hidden = true;\">x</button>'
			+ '<div class=\"'
			+ classes
			+ '\">' + msg + '</div>'
			+ '</div>',
		width: 350
	};
	return param;
}

var templatePromoDetailOpened = false;

window.onresize = function() {
	if (templatePromoDetailOpened) {
		var event = document.createEvent('Event');
		event.initEvent('windowResize:promoDetail', true, true);
		window.dispatchEvent(event);
	}
};

window.addEventListener('windowResize:promoDetail', function(e) {
	templatePromotionDetailResize();
}, false);

function genericTabChangeEvent(id, lastSelectedTabId, tablist) {
	var selectedTab = document.getElementById(id);
	if ($.inArray('current', selectedTab.classList) > 0) {
		return;
	}
	
	var lastTab = document.getElementById(lastSelectedTabId);
	if (lastTab !== undefined && lastTab !== null) {
		lastTab.classList.remove('current');
	}
	selectedTab.classList.add('current');
	
	if (tablist !== undefined) {
		for (var i = 0; i < tablist.length; i++) {
			elem = document.getElementById(tablist[i]);
			elem.style.display = 'none';
		}
	}
}

var selectedSettingsTab = '';

templateSettingsTabChangeEvent = function(id, scope) {
	genericTabChangeEvent(id, selectedSettingsTab, undefined);
	selectedSettingsTab = id;
}

changePasswordFormChanged = function(type, scope) {
	var className = '';
	var validation = false;
	var errorMsg = '';
	
	var currentPassword = scope.vm.currentpassword;
	var newPassword = scope.vm.password;
	var confirmPassword = scope.vm.confirmPassword;
	var customText = scope.vm.profileCustomText;
	var phone = scope.vm.phone;
	
	if (type === 'oldPassword') {
		className = 'chgPwdOldPassword';
		if (currentPassword.length === 0) {
			errorMsg = customText.chgPwd.error.empty;
		} else if (currentPassword.length < 6) {
			errorMsg = customText.chgPwd.error.invalid;
		} else {
			validation = true;
		}
	} else if (type === 'newPassword') {
		className = 'chgPwdNewPassword';
		if (newPassword.length === 0) {
			errorMsg = customText.chgPwd.error.empty;
		} else if (newPassword.length < 6) {
			errorMsg = customText.chgPwd.error.invalid;
		} else {
			validation = true;
		}
	} else if (type === 'confirmPassword') {
		className = 'chgPwdConfirmPassword';
		if (confirmPassword.length === 0 || newPassword !== confirmPassword) {
			errorMsg = customText.chgPwd.error.invalid;
		} else {
			validation = true;
		}
	}
	
	updateFormFieldTips(className, validation, errorMsg);
}

addBankFormFieldChanged = function(type, scope) {
	var className = '';
	var validation = false;
	var errorMsg = '';
	
	var newBankAccount = scope.vm.newBankAccount;
	var customText = scope.vm.profileCustomText;
	
	if (type === 'cardNo') {
		className = 'addBankFormCardNo';
		if (newBankAccount.accountNumber.length === 0 || !newBankAccount.accountNumber.match(/^[+]?[0-9]+$/)) {
			errorMsg = customText.bank.error.cardNoInvalid;
		} else if (newBankAccount.accountNumber.length < 6) {
			errorMsg = customText.bank.error.cardNoMinLength;
		} else {
			validation = true;
		}
	} else if (type === 'bankName') {
		className = 'addBankFormBankName';
		if (newBankAccount.bankName.length === 0 || newBankAccount.accountNumber.match(/^[+]?[0-9]+$/)) {
			errorMsg = customText.bank.error.invalid;
		} else {
			validation = true;
		}
	}
	
	updateFormFieldTips(className, validation, errorMsg);
}

var historySubtab = 'historyTransferTab';

templateHistorySubtabChangeEvent = function(id, scope) {
	genericTabChangeEvent(id, historySubtab, scope.vm.subtab);
	historySubtab = id;
	
	var elem = null;
	if (id === 'historyTransferTab') {
		elem = document.getElementById('historyTransferContent');
	} else if (id === 'historyDepositWithdrawTab') {
		elem = document.getElementById('historyDepositWithdrawContent');
	} else if (id === 'historyPromotionTab') {
		elem = document.getElementById('historyPromotionContent');
	} else if (id === 'historyRebateTab') {
		elem = document.getElementById('historyRebateContent');
	}
	elem.style.display = 'block';
}

getMemberAllWalletMapping = function(listData, copyProviderWalletFunc) {
	var mainWalletBalance = 0;
	var providerWallets = {};
	var totalturnoveramount = 0;
	
	// map data
	for (var i = 0; i < listData.length; i++) {
		if (listData[i].provider == null) {
			mainWalletBalance = listData[i].balance;
		} else {
			var provider = listData[i].provider;
			providerWallets[provider] = copyProviderWalletFunc(listData[i]);
			totalturnoveramount = providerWallets[provider].totalturnoveramount;
		}
	}
	
	var mappedData = {
		'mainWalletBalance': mainWalletBalance,
		'providerWallets': providerWallets,
		'totalturnoveramount': totalturnoveramount
	};
	
	return mappedData;
};

providerTransferListMapping = function(providerWallets) {
	var returnList = [];
	var rowlist = [];
	var i = 0;
	var rowIdentifier = '';
	
	for (var key in providerWallets) {
		if (i % 2 === 0) {
			if (returnList.length > 0) {
				rowlist = [];
			}
		}
		providerWallets[key].rowIdentifier = '' + returnList.length + (i % 2);
		rowlist.push(providerWallets[key]);
		
		if (i % 2 === 1) {
			returnList.push(rowlist);
		}
		
		i++;
	}
	
	// append extra dummy column if rowlist is not even
	if (rowlist.length % 2 === 1) {
		var dummyColumn = {
			isNull: true
		};
		rowlist.push(dummyColumn);
		returnList.push(rowlist);
	}
	
	return returnList;;
}

providerTransferListMappingForNewBk8Setting = function(providerWallets) {
	var returnList = [];
	var sportsbook = ['CMD', 'IBC', 'SBO', 'BR'];
	var liveCasino = ['AB', 'GD', 'BMK', 'BMK2', 'GP', 'DG', 'AG', 'PT', 'UG'];
	var fishing = ['GG', 'SG'];
	var slot = ['SCR', 'SCR2', 'MG', 'MGP', 'TTG', 'PG', 'UL'];

	var row0 = [];
	for(var x=0; x<sportsbook.length; x++) {
		if(providerWallets[sportsbook[x]] != null) {
			providerWallets[sportsbook[x]].rowIdentifier = '0';
			row0.push(providerWallets[sportsbook[x]]);
		}
	}
	
	var row1 = [];
	for(var x=0; x<liveCasino.length; x++) {
		if(providerWallets[liveCasino[x]] != null) {
			providerWallets[liveCasino[x]].rowIdentifier = '1';
			row1.push(providerWallets[liveCasino[x]]);
		}
	}
	for(var x=0; x<fishing.length; x++) {
		if(providerWallets[fishing[x]] != null) {
			providerWallets[fishing[x]].rowIdentifier = '1';
			row1.push(providerWallets[fishing[x]]);
		}
	}
	for(var x=0; x<slot.length; x++) {
		if(providerWallets[slot[x]] != null) {
			providerWallets[slot[x]].rowIdentifier = '1';
			row1.push(providerWallets[slot[x]]);
		}
	}
	
	var row2 = [];
	var keys = Object.keys(providerWallets);
	for(var x=0; x<keys.length; x++) {
		if(!sportsbook.includes(keys[x]) && !liveCasino.includes(keys[x]) && !fishing.includes(keys[x]) && !slot.includes(keys[x])) {
			providerWallets[keys[x]].rowIdentifier = '2';
			row2.push(providerWallets[keys[x]]);
		}
	}
	
	returnList.push(row0);
	returnList.push(row1);
	returnList.push(row2);
	return returnList;
}

maskProviderTransferList = function(displayList) {
	for (var row = 0; row < displayList.length; row++) {
		for (var column = 0; column < displayList[row].length; column++) {
			if (displayList[row][column].isNull === false) {
				displayList[row][column].isdisplay = false;
			}
		}
	}

	return displayList;
};

mapProviderWallet = function(displayList, serviceData) {

	for (var row = 0; row < displayList.length; row++) {
		for (var column = 0; column < displayList[row].length; column++) {
			if (displayList[row][column].isNull === false) {
				var data = $.grep(serviceData, function(e) { return e.provider == displayList[row][column].provider; })[0];
				if (data !== undefined) {
					displayList[row][column].balance = data.balance;
					displayList[row][column].serverIsUnderMaintenance = data.serverIsUnderMaintenance;
					displayList[row][column].isdisplay = true;
				} else {
					// if not returned means no wallet, set default
					displayList[row][column].balance = 0;
					displayList[row][column].serverIsUnderMaintenance = false;
					displayList[row][column].isdisplay = true;
				}
			}
		}
	}
	
	return displayList;
}

getProviderWalletDictionaryFromDisplayList = function(displayList, provider) {
	var returnProviderWallet = null;
	var isFound = false;
	for (var row = 0; row < displayList.length && !isFound; row++) {
		for (var column = 0; column < displayList[row].length; column++) {
			if (displayList[row][column].provider === provider) {
				returnProviderWallet = displayList[row][column];
				isFound = true;
			}
		}
	}
	return returnProviderWallet;
}

populateCommonSlotBackground = function(data, provider, providerClassName, providerDefaultBannerName) {
	
};

templateGetSlotSlidingBanner = function(portalSetting) {
	var returnBannerImgList = [];
	if (portalSetting && portalSetting.bannerList) {
		var langBanner = portalSetting.bannerList[countryLanguageKey];
		if (langBanner) {
			for (var i = 0; i < langBanner.length; i++) {
				var dict = {
					src: langBanner[i]
				};
				returnBannerImgList.push(dict);
			}
		}
	}
	return returnBannerImgList;
};

templateSlotTypeSelectEvent = function(filterType, lastSelectedFilter, productList) {
	var returnDict = {
		selectedFilter: filterType
	};
	
	if (lastSelectedFilter === filterType) {
		return returnDict;
	}
	
	returnDict.selectedFilter = filterType;
	
	for (var i = 0; i < productList.length; i++) {
		var id = productList[i];
		var divIDName = "#" + id;
		var div = $(divIDName)[0];
		
		if (id === filterType) {
			div.className = "tab_item_showall on";
		}
		else {
			div.className = "tab_item_showall";
		}
	}
	
	return returnDict;
}

//new_bk8 fishing banner
templateGetFishingSlidingBanner = function(portalSetting) {
	var returnJson = {
		slideBanner: [],
		providerBannerJsons: []
	};

	if (portalSetting) {
		// get banner list
		if (portalSetting.bannerList) {
			var langBanner = portalSetting.bannerList[countryLanguageKey];
			if (langBanner) {
				for (var i = 0; i < langBanner.length; i++) {
					var dict = {
						src: langBanner[i]
					};
					returnJson.slideBanner.push(dict);
				}
			}
		}

		var keys = Object.keys(portalSetting);
		// get provider banners
		for (var i = 0; i < keys.length; i++) {
			// skip bannerList key
			if (keys[i] === 'bannerList') {
				continue;
			} else {
				portalSetting[keys[i]].bannerSrc = portalSetting[keys[i]].bannerSrc.replace('{0}', countryLanguageKey);
				portalSetting[keys[i]].id = keys[i];
				returnJson.providerBannerJsons.push(portalSetting[keys[i]]);
			}
		}
	}

	return returnJson;
};

templateFishingInitProviderBanner = function (providerDiv, providerObj, scope, compile) {
	if (providerObj.btnMapping && providerObj.btnMapping[countryLanguageKey]) {
		var btnProp = providerObj.btnMapping[countryLanguageKey];
		// append play button
		var playPosition = imgBgBtnRepositionEvent(btnProp.play);
		var playBtn = generateTransparentBtn(btnProp.play, playPosition);

		if (providerObj.id === 'PTBanner') {
			playBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.PTplayfn);
		} else if (providerObj.id === 'GGBanner') {
			playBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.GGplayfn);
		} else if (providerObj.id === 'SGBanner') {
			playBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.SGplayfn);
		}

		// function to bind element to use angularjs $scope, check core.js on implementation
		jsToAngularJsBinding(playBtn, scope, compile)

		providerDiv.appendChild(playBtn);

		// append demo button
		var demoPosition = imgBgBtnRepositionEvent(btnProp.demo);
		var demoBtn = generateTransparentBtn(btnProp.demo, demoPosition);

		if (providerObj.id === 'PTBanner') {
			demoBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.PTdemofn);
		} else if (providerObj.id === 'GGBanner') {
			demoBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.GGdemofn);
		} else if (providerObj.id === 'SGBanner') {
			demoBtn.setAttribute('ng-click', providerObj.providerPlayDemoFn.SGdemofn);
		}


		// function to bind element to use angularjs $scope, check core.js on implementation
		jsToAngularJsBinding(demoBtn, scope, compile)

		providerDiv.appendChild(demoBtn);
		
		providerDiv.style.maxWidth = btnProp.play.oriBannerWidth + 'px';
	

		// reposition function on window resize event
		function repositionFishingImgBgBtnOnResize() {
			var playPosition = imgBgBtnRepositionEvent(btnProp.play);
			playBtn.style.left = playPosition.left + 'px';
			playBtn.style.top = playPosition.top + 'px';

			var demoPosition = imgBgBtnRepositionEvent(btnProp.demo);
			demoBtn.style.left = demoPosition.left + 'px';
			demoBtn.style.top = demoPosition.top + 'px';
			
		}

		// add resize checking
		new ResizeSensor(providerDiv, repositionFishingImgBgBtnOnResize);

		
	}
}

//new_bk8 poker banner
templateGetPokerSlidingBanner = function(portalSetting) {
	var returnBannerImgList = [];
	if (portalSetting && portalSetting.pokerBannerList) {
		var langBanner = portalSetting.pokerBannerList[countryLanguageKey];
		if (langBanner) {
			for (var i = 0; i < langBanner.length; i++) {
				var dict = {
					src: langBanner[i]
				};
				returnBannerImgList.push(dict);
			}
		}
	}
	return returnBannerImgList;
};

templateSlotDefaultSelectTypeAll = function() {
	var all = $("#ALL")[0];
	all.className = "tab_item_showall on";

	var all = $("#HOT")[0];
	all.className = "tab_item_showall";
}

templateGetCasinoSlidingBanner = function(portalSetting) {
	var returnBannerImgList = [];
	if (portalSetting && portalSetting.bannerList) {
		var langBanner = portalSetting.bannerList[countryLanguageKey];
		if (langBanner) {
			for (var i = 0; i < langBanner.length; i++) {
				var dict = {
					src: langBanner[i]
				};
				returnBannerImgList.push(dict);
			}
		}
	}
	return returnBannerImgList;
}

templateGetCasinoSmallSlidingBanner = function(portalSetting) {
	var returnBannerImgList = [];
	if (portalSetting && portalSetting.smallBannerList) {
		var langBanner = portalSetting.smallBannerList[countryLanguageKey];
		if (langBanner) {
			for (var i = 0; i < langBanner.length; i++) {
				var dict = {
					src: langBanner[i]
				};
				returnBannerImgList.push(dict);
			}
		}
	}
	return returnBannerImgList;
}

templateMobileDownloadTypeSelectEvent = function(filterType, lastSelectedFilter, productList) {
	var returnDict = {
		selectedFilter: filterType
	};
	
	if (lastSelectedFilter === filterType.name) {
		return returnDict;
	}
	
	returnDict.selectedFilter = filterType;
	
	for (var i = 0; i < productList.length; i++) {
		var id = productList[i].name;
		var divIDName = "#" + id;
		var div = $(divIDName)[0];
		
		if (id === filterType.name) {
			div.className = "current";
		}
		else {
			div.className = "";
		}
	}
	
	return returnDict;
}

templateSlotGameSmallBannerInit = function(portalSetting) {
	$("#divNewGame").on("switch", function(){
        TweenMax.fromTo(".newGameContent", 1, {
            opacity: 0,
            x: -20
        }, {
            opacity: 1,
            x: 0
        });
    });
	
	var returnBannerImgList = [];
	if (portalSetting && portalSetting.smallBannerList) {
		var langBanner = portalSetting.smallBannerList[countryLanguageKey];
		if (langBanner) {
			for (var i = 0; i < langBanner.length; i++) {
				var dict = {
					src: langBanner[i].src,
					header: langBanner[i].header,
					desc: langBanner[i].desc,
					rtp: langBanner[i].rtp,
					gameId: langBanner[i].gameId
				};
				returnBannerImgList.push(dict);
			}
		}
	}
	return returnBannerImgList;
};

templateGetCasinoButton = function (portalSetting) {
	if (portalSetting && portalSetting.casinoButton) {
		var langButton = portalSetting.casinoButton[countryLanguageKey];
		return langButton[0];
	}
}

customIconsForContact = function(contact) {
	if(!contact)
		return '';
	var name = contact.src;
	if (name === 'contact-phone') {
		return 'icons icons_side_menu_mobile';
	}
	else if (name === 'contact-ws') {
		return 'icons icons_side_menu_watsapp';
	}
	else if (name === 'contact-wc') {
		return 'icons icons_side_menu_wechat';
	}
	else if (name === 'contact-line' || contact.href != '') {
		return 'icons icons_side_menu_line';
	}
	else if (name === 'contact-skype') {
		return 'icons_side_menu_skype';
	}
	else if (name === 'contact-yahoo') {
		return 'icons_side_menu_yahoo';
	}
	else if (name === 'contact-bbm') {
		return 'icons_side_menu_bbm';
	}
	else if (name === 'contact-telegram') {
		return 'icons_side_menu_telegram';
	}
};


customIconsForMContact = function(contact) {
	if(!contact)
		return '';
	var name = contact.src;
	if (name === 'm-contact-phone') {
		return 'icons icon_menu_contact_phone';
	}
	else if (name === 'm-contact-ws') {
		return 'icons icon_menu_contact_whatsapp';
	}
	else if (name === 'm-contact-wc') {
		return 'icons icon_menu_contact_wechat';
	}
	else if (name === 'm-contact-bbm') {
		return 'icons icon_menu_contact_bbm';
	}
	else if (name === 'm-contact-line') {
		return 'icons icon_menu_contact_line';
	}
	else if (name === 'm-contact-skype') {
		return 'icons icon_menu_contact_skype';
	}
	else if (name === 'm-contact-telegram') {
		return 'icons icon_menu_contact_telegram';
	}
	else if (name === 'm-contact-yahoo') {
		return 'icons icon_menu_contact_yahoo';
	}
};

// designer said homepage banner confirm is 2000 x 768px size
// other banner confirm is 2000 x 582px size
initCommonSliderDiv = function(banners, idWildcard, height, noID) {
	var id = null;
	for (var i = 0; i < banners.length; i++) {
		if (noID) {
			id = '#' + idWildcard + i;
		} else {
			id = '#' + idWildcard + banners[i].id;
		}
		
		var divElem = $(id)[0];
    	divElem.style.height = height + 'px';
    	divElem.style.backgroundImage = 'url(' + banners[i].src + ')';
    	divElem.style.backgroundPosition = 'top center';
    	divElem.style.backgroundRepeat = 'no-repeat';
	}
}

initCommonPromoSliderDiv = function(banners, idWildcard, height, noID) {
	
	var id = null;
	for (var i = 0; i < banners.length; i++) {
		if (noID) {
			id = '#' + idWildcard + i;
		} else {
			id = '#' + idWildcard + banners[i].id;
		}
		
		var divElem = $(id)[0];
    	divElem.style.height = height + 'px';
		divElem.style.backgroundImage = 'url(' + banners[i].img + ')';
		divElem.style.backgroundPosition = 'center';
    	divElem.style.backgroundRepeat = 'no-repeat';
	}
}

initCommonDivBg = function(imgSrc, bannerId, height) {
	var id = '#' + bannerId;
	var divElem = $(id)[0];
	divElem.style.backgroundImage = 'url(' + imgSrc + ')';
	divElem.style.height = ((height) ? height : 582) + 'px';
	divElem.style.backgroundPosition = 'top center';
	divElem.style.backgroundRepeat = 'no-repeat';
}

initCommonDivSmallBg = function(imgSrc, bannerId, height) {
	var id = '#' + bannerId;
	var divElem = $(id)[0];
	divElem.style.backgroundImage = 'url(' + imgSrc + ')';
	divElem.style.backgroundPosition = 'top center';
	divElem.style.backgroundRepeat = 'no-repeat';
}

initSlider = function(slider, bannerCheatId, initIndex) {
	 slider.each(function (i, item) {
		 _initSlider($(item), bannerCheatId, initIndex);
	 });
};

function _initSlider(slider, bannerCheatId, initIndex) {
    //var slider = id ? $("#" + id) : $(".slider");
    var pages = slider.find(".page_item"); // page_item
    var pageNo = slider.find(".page_no li");
    var len = pages.length;
    var index = 0;
    if(initIndex != undefined)
    	index = initIndex;

    pages.filter(":gt(" + index + ")").hide();
	pages.filter(":lt(" + index + ")").hide();
    var onSwtich = function (newIndex) {
        if(newIndex === index) return;

        var leavePage = pages.eq(index),
            enterPage = pages.eq(newIndex);
        
        pages.removeClass("leave_active leave_to enter_active enter_to");
        pages.not(leavePage).not(enterPage).hide();
        requestAnimationFrame(function () {
            leavePage.show().addClass("leave");
            enterPage.show().addClass("enter");

            requestAnimationFrame(function () {
                leavePage
                    .addClass("leave_active leave_to")
                    .removeClass("leave");
                enterPage
                    .addClass("enter_active enter_to")
                    .removeClass("enter")

                //page no
                pageNo.removeClass("on").eq(newIndex).addClass("on");
                index = newIndex;
                if (bannerCheatId) {
                	$("#" + bannerCheatId).attr('src', enterPage.find("img").attr('src'));
                }
            });
        });

        slider.trigger("switch", newIndex);
    };

    var nextPage = function () {
        var newIndex = index > len - 2 ? 0 : index + 1;
        onSwtich(newIndex);
    };

    pageNo.click(function (evt) {
        var li = $(evt.target).parent("li");
        if (li.length < 1) return;

        pageNo.removeClass("on");
        li.addClass("on");
        var i = pageNo.index(li);

        if (index === i) return;

        onSwtich(i);
        runTimer();
    });

    var timer = 0;
    var runTimer = function () {
        if (slider.attr("auto") === "false") return;

        clearInterval(timer);
        timer = setInterval(function () {
            nextPage();
        }, 5000);
    };

    runTimer();
    slider.on("doswitch", function (e, i) {
        if(i == undefined)
            nextPage();
        else
            onSwtich(i);
    });
}


//For New Game in Slot Page
initSlider2 = function(slider, secondSlider, bannerCheatId, initIndex) {
	slider.each(function (i, item) {
		_initSlider2($(item), secondSlider,  bannerCheatId, initIndex);
	});
};

//For New Game in Slot Page
function _initSlider2(slider, secondSlider,  bannerCheatId, initIndex) {
   //var slider = id ? $("#" + id) : $(".slider");
   var pages = slider.find(".page_item"); // page_item
   var pages2 = slider.find(".page_item."+ secondSlider); // page_item2
   pages.hide();
   pages.eq(0).show();
   pages2.eq(0).show();
   var pageNo = slider.find(".page_no li");
   var len = pages.length - pages2.length;

   var index = 0;
   if (initIndex != undefined)
	   index = initIndex;

   pages2.filter(":gt(" + index + ")").hide();
   pages2.filter(":lt(" + index + ")").hide();

   var onSwtich = function (newIndex) {
	   if (newIndex === index) return;

	   var leavePage = pages.eq(index),
		   enterPage = pages.eq(newIndex);

	   var leavePage2 = pages2.eq(index),
		   enterPage2 = pages2.eq(newIndex);
		   

	   pages.removeClass("leave_active leave_to enter_active enter_to");
	   pages.not(leavePage).not(enterPage).hide();

	   pages2.removeClass("leave_active leave_to enter_active enter_to");
	   pages2.not(leavePage).not(enterPage).hide();

	   requestAnimationFrame(function () {
		   leavePage.show().addClass("leave");
		   enterPage.show().addClass("enter");
		   leavePage2.show().addClass("leave");
		   enterPage2.show().addClass("enter");
		   requestAnimationFrame(function () {
			   leavePage
				   .addClass("leave_active leave_to")
				   .removeClass("leave");
			   enterPage
				   .addClass("enter_active enter_to")
				   .removeClass("enter");
			   leavePage2
				   .addClass("leave_active leave_to")
				   .removeClass("leave");
			   enterPage2
				   .addClass("enter_active enter_to")
				   .removeClass("enter");
			   //page no
			   pageNo.removeClass("on").eq(newIndex).addClass("on");
			   index = newIndex;
			   if (bannerCheatId) {
				   $("#" + bannerCheatId).attr('src', enterPage.find("img").attr('src'));
			   }
		   });
	   });

	   slider.trigger("switch", newIndex);
   };

   var nextPage = function () {
	   var newIndex = index > len - 2 ? 0 : index + 1;
	   onSwtich(newIndex);
   };

   pageNo.click(function (evt) {
	   var li = $(evt.target).parent("li");
	   if (li.length < 1) return;

	   pageNo.removeClass("on");
	   li.addClass("on");
	   var i = pageNo.index(li);

	   if (index === i) return;

	   onSwtich(i);
	   runTimer();
   });

   var timer = 0;
   var runTimer = function () {
	   if (slider.attr("auto") === "false") return;

	   clearInterval(timer);
	   timer = setInterval(function () {
		   nextPage();
	   }, 5000);
   };

   runTimer();
   slider.on("doswitch", function (e, i) {
	   if (i == undefined)
		   nextPage();
	   else
		   onSwtich(i);
   });
}
var videoTimer = 0;

//For Video Slider in Slot Page
initSliderVideo = function(slider, secondSlider, isResume,  bannerCheatId, initIndex) {
	slider.each(function (i, item) {
		_initSliderVideo($(item), secondSlider, isResume,  bannerCheatId, initIndex);
	});

	return videoTimer;
};

//For Video Slider
function _initSliderVideo(slider, secondSlider, isResume, bannerCheatId, initIndex) {
   //var slider = id ? $("#" + id) : $(".slider");
   var pages = slider.find(".page_item"); // page_item
   var pages2 = slider.find(".page_item." + secondSlider); // page_item2
   setTimeout(() => {
	   
}, 1000);
   var pageNo = slider.find(".page_no li");
   var len = pages.length - pages2.length;
   pages.hide();
   pages.eq(0).show();
   pages2.eq(0).show();
   
   var index = 0;
   if (initIndex != undefined)
	   index = initIndex;

   pages2.filter(":gt(" + index + ")").hide();
   pages2.filter(":lt(" + index + ")").hide();

   var onSwtich = function (newIndex) {
	   if (newIndex === index) return;

	   var leavePage = pages.eq(index),
		   enterPage = pages.eq(newIndex);

	   var leavePage2 = pages2.eq(index),
		   enterPage2 = pages2.eq(newIndex);
		   

	   pages.removeClass("leave_active leave_to enter_active enter_to");
	   pages.not(leavePage).not(enterPage).hide();

	   pages2.removeClass("leave_active leave_to enter_active enter_to");
	   pages2.not(leavePage).not(enterPage).hide();

	   requestAnimationFrame(function () {
		   leavePage.show().addClass("leave");
		   enterPage.show().addClass("enter");
		   leavePage2.show().addClass("leave");
		   enterPage2.show().addClass("enter");
		   requestAnimationFrame(function () {
			   leavePage
				   .addClass("leave_active leave_to")
				   .removeClass("leave");
			   enterPage
				   .addClass("enter_active enter_to")
				   .removeClass("enter");
			   leavePage2
				   .addClass("leave_active leave_to")
				   .removeClass("leave");
			   enterPage2
				   .addClass("enter_active enter_to")
				   .removeClass("enter");
			   //page no
			   pageNo.removeClass("on").eq(newIndex).addClass("on");
			   index = newIndex;
			   if (bannerCheatId) {
				   $("#" + bannerCheatId).attr('src', enterPage.find("img").attr('src'));
			   }
		   });
	   });

	   slider.trigger("switch", newIndex);
   };

   var nextPage = function () {
	   var newIndex = index > len - 2 ? 0 : index + 1;
	   onSwtich(newIndex);
   };

   pageNo.click(function (evt) {
	   var li = $(evt.target).parent("li");
	   if (li.length < 1) return;

	   pageNo.removeClass("on");
	   li.addClass("on");
	   var i = pageNo.index(li);

	   if (index === i) return;

	   onSwtich(i);
	//    nextPage();
	//    runTimer();
   });

   var runTimer = function () {
	   if (slider.attr("auto") === "false") return;

	   clearInterval(videoTimer);
	   videoTimer = setInterval(function () {
		   nextPage();
	   }, 5000);
   };

   runTimer();
   slider.on("doswitch", function (e, i) {
	   if (i == undefined)
		   nextPage();
	   else
		   onSwtich(i);
   });

}




var gdResult;
var gdResultCallback = null;
getGdResult = function(callback) {
	
	var url = "https://www.bolaking.net/public/html/gd_table_info.js?callback=gdCallback";
	var domId = "domGdLiveResultScript";
	if(document.getElementById(domId) != null)
		document.head.removeChild(document.getElementById(domId));
	var domGdLiveResultScript = document.createElement("script");
	domGdLiveResultScript.type = "text/javascript";
	domGdLiveResultScript.src = url;
	domGdLiveResultScript.src += url.includes("?") ? "&nocache=" + new Date().getTime() : "?nocache=" + new Date().getTime();
	domGdLiveResultScript.id = domId;
	document.head.appendChild(domGdLiveResultScript);
	
	if(gdResult)
		callback(gdResult);
	else
		gdResultCallback = callback;
}

function gdCallback(data) {
	gdResult = data;
	if (gdResultCallback !== null) {
		gdResultCallback(gdResult);
		gdResultCallback = null;
	}
}

initHotGames = function() {
	 $("#sliderVideo").on("switch", function(){
        TweenMax.fromTo(".videoContent", 1, {
            opacity: 0,
            x: -20
        },{
            opacity: 1,
            x: 0
        });
    });


    $(".hotGamesContent .imgItem").on("mouseenter", function(){
        $(".imgContainer").each(function(){
            this.style.animationPlayState = "paused";
        });

        _hotGameOver(this);
    }).on("mouseleave", function(){
        $(".imgContainer").each(function(){
            this.style.animationPlayState = "running";
        });

        _hotGameOut(this);
    });
}

function _hotGameOver(el){
    var slider = $(el).find(".slider");
    clearInterval(slider.timer || 0);

    var timer = setInterval(function(){
        slider.trigger("doswitch");
    }, 1000);

    slider.attr("timer", timer);
}

function _hotGameOut(el){
    var slider = $(el).find(".slider");
    var timer = slider.attr("timer") || 0;
    clearInterval(timer);
    slider.trigger("doswitch", 0);

    slider.attr("timer", 0);
}

initChangeTab = function() {
	$("ul.tabPage").each(function (i, item) {
        var tab = $(item);
        tab.find("li").click(function () {
            if ($(this).hasClass("on")) return;

            tab.find("li.on").removeClass("on");
            $(this).addClass("on");
            tab.trigger("change", $(this).attr("data-type"));
        });
    });
	
	$(".tabPage").on("change", function(e, type){
        type = type || "android";
        var tag = ".downloadList[data-type=" + type + "]";

        var el = $(tag).show();
        $(".downloadList").not(tag).hide();

        TweenMax.killAll();
        TweenMax.staggerFromTo(tag + " .downItem", 1, {
            opacity: 0
        }, {
            opacity: 1,
            delay: 0.5
        }, 0.2);
    }).trigger("change");
}

initChangeCasinoTab = function() {
	$(".tabPage").on("change", function(){
        TweenMax.killAll();
        TweenMax.staggerFromTo("#list .imgItem", 1, {
            opacity: 0
        }, {
            opacity: 1,
            delay: 0.5
        }, 0.2);
    }).trigger("change");
}

initChangeSlotTab = function(){

	$(".tabPage").on("click", function(){
        TweenMax.killTweensOf(".imgItem");
        TweenMax.staggerFromTo(".imgItem", .6, {
            opacity: 0,
            rotationY: -90
        }, {
            opacity: 1,
            rotationY: 0,          
            delay: 0.5,
            onComplete: function(){
                this.target.style.transform = "";
            }
        }, 0.1);
	});
}

initLiveTv = function(){

	setInterval(function() {
		TweenMax.staggerTo(".liveTvWatchItem", 0.5, {
			opacity: 1
		}, 0.1);
	}, 0);
	
}



var upComingEventSlides;
var upComingEventSlidesCallback = null;
getUpComingEventSlides = function(callback, url) {
	
	var domId = "domUpComingEventScript";
	if(document.getElementById(domId) != null)
		document.head.removeChild(document.getElementById(domId));
	var domUpComingEventScript = document.createElement("script");
	domUpComingEventScript.type = "text/javascript";
	domUpComingEventScript.src = url;
	domUpComingEventScript.src += url.includes("?") ? "&nocache=" + new Date().getTime() : "?nocache=" + new Date().getTime();
	domUpComingEventScript.id = domId;
	document.head.appendChild(domUpComingEventScript);
	
	if(upComingEventSlides)
		callback(upComingEventSlides);
	else
		upComingEventSlidesCallback = callback;
}

function eventsCallback(data) {
	upComingEventSlides = data;
	if (upComingEventSlidesCallback !== null) {
		upComingEventSlidesCallback(upComingEventSlides);
		upComingEventSlidesCallback = null;
	}
}

homepageSportNewsPopupCallbackFromIframe = function (newsContent) {
	if (homepageSportNewsOpenPopupFromController !== null) {
		homepageSportNewsOpenPopupFromController(newsContent);
	}
};

homepageSportNewsOpenPopupFromController = function (callback) {
	
}

function templateMToggleSidebar() {
	if ($('#leftmenu').hasClass('ui-panel-closed')) {
		$('#leftmenu')[0].classList.remove('ui-panel-closed');
		$('#leftmenu')[0].classList.add('ui-panel-open');
		
		$('.ui-panel-dismiss').toggleClass('active');
		$('.m-main-content').bind('touchmove', function(e) { e.preventDefault(); } );
	} else {
		$('#leftmenu')[0].classList.remove('ui-panel-open');
		$('#leftmenu')[0].classList.add('ui-panel-closed');
		
		$('.ui-panel-dismiss').toggleClass('active');
		$('.m-main-content').unbind('touchmove');
	}
}

function templateMHideSidebar() {
	templateMToggleSidebar();
}

initMSlider = function(slider, bannerCheatId, initIndex) {
	slider.each(function (i, item) {
		_initMSlider($(item), bannerCheatId, initIndex);
	});
};

function addTransition(el, type) {
    type = type === "hide" ? "leave" : "enter";

    el.show();
    el.removeClass("leave leave-active leave-to");
    el.removeClass("enter enter-active enter-to");
    requestAnimationFrame(function () {
        el.addClass(type);
        requestAnimationFrame(function () {
            el.addClass(type + "-active").addClass(type + "-to").removeClass(type);
        });
    });

}

function _initMSlider(slider, bannerCheatId, initIndex) {
	var pageItemList = slider.find(".page_item");
    var pageNoList = slider.find(".slide_point li");
    var size = pageNoList.length;
    var pageIndex = 0;
	
	pageItemList.hide();
	pageItemList.eq(pageIndex).show();
	var switchPage = function(diretion){
        diretion = diretion || "left";
        
		var sliderElem = document.getElementById(slider[0].id);
		if (sliderElem && sliderElem.classList) {
			sliderElem.classList = 'slider ' + diretion;
        
			var newIndex = pageIndex + (diretion === "left" ? 1 : -1);
			newIndex = newIndex === size ? 0 : newIndex; 
			newIndex = newIndex < 0 ? size - 1 : newIndex;
			
			pageItemList.hide();
			var leaveItem = pageItemList.eq(pageIndex).show();
			var enterItem = pageItemList.eq(newIndex).show();
	
			addTransition(leaveItem, "hide");
			addTransition(enterItem);

			pageNoList.eq(pageIndex).removeClass("on");
			pageNoList.eq(newIndex).addClass("on");
	
			pageIndex = newIndex;
		} else {
			clearInterval(timer);
		}
    };
    
    slider[0].addEventListener('touchstart', handleTouchStart, false);
    slider[0].addEventListener('touchmove', handleTouchMove, false);
    var xDown = null;
    var yDown = null;
    
    function handleTouchStart(evt) {
        xDown = evt.touches[0].clientX;
        yDown = evt.touches[0].clientY;
    };
    
    function handleTouchMove(evt) {
        if ( ! xDown || ! yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;
        var yUp = evt.touches[0].clientY;

        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;
        if(Math.abs( xDiff )+Math.abs( yDiff )>150){ //to deal with to short swipes

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {/* left swipe */ 
            	//normal
                clearInterval(timer);
                timer = setInterval(switchPage, 3000);

                switchPage();
            } else {/* right swipe */
            	clearInterval(timer);
                timer = setInterval(switchPage, 3000);

                switchPage("right");
            }
        }
        /* reset values */
        xDown = null;
        yDown = null;
        }
	};

    timer = setInterval(switchPage, 3000);
}

mapCategoryClass = function(name) { 
	if (name === 'Sports') {
		return 'icons icon_home_menu_sports_off';
	} else if (name === 'Casino') {
		return 'icons icon_home_menu_livecasino_off';
	} else if (name === 'Slots') {
		return 'icons icon_home_menu_slots_off';
	} else if (name === 'Fishing') {
		return 'icons icon_home_menu_fishing_off';
	} else if (name === 'Poker') {
		return 'icons icon_home_menu_poker_off';
	} else if (name === 'LiveTV') {
		return 'icons icon_home_menu_livetv_off';
	} else if (name === 'Lottery') {
		return 'icons icon_home_menu_lottery_off';
	} else if (name === '918Kiss' || name === '918Kiss2') {
		return '';
	}
	return '';
}


mapBankIconClass = function (name, currency) {
    if (name === 'MBB') {
        return 'icons icon_myaccount_banking_mbb';
    } else if (name === 'CIMB' || name === 'CIMBN') {
        return 'icons icon_myaccount_banking_cimb';
    } else if (name === 'DBS') {
        return 'icons icon_myaccount_banking_dbs';
    } else if (name === 'EEZIEPAY') {
        return 'icons icon_myaccount_banking_eeziepay';
    } else if (name === 'GJKFYH') {
        return 'icons icon_myaccount_banking_gjkfyh';
    } else if (name === 'GSB') {
        return 'icons icon_myaccount_banking_gsb';
    } else if (name === 'H2P') {
        return 'icons icon_myaccount_banking_h2p';
    } else if (name === 'HLB') {
        return 'icons icon_myaccount_banking_hlb';
    } else if (name === 'HSBC') {
        return 'icons icon_myaccount_banking_hsbc';
    } else if (name === 'KKR') {
        return 'icons icon_myaccount_banking_kkr';
    } else if (name === 'KTB') {
        return 'icons icon_myaccount_banking_ktb';
    } else if (name === 'MBB') {
        return 'icons icon_myaccount_banking_mbb';
    } else if (name === 'MDR') {
        return 'icons icon_myaccount_banking_mdr';
    } else if (name === 'OCBC') {
        return 'icons icon_myaccount_banking_ocbc';
    } else if (name === 'PBB') {
        return 'icons icon_myaccount_banking_pbb';
    } else if (name === 'PMB') {
        return 'icons icon_myaccount_banking_pmb';
    } else if (name === 'POSB') {
        return 'icons icon_myaccount_banking_posb';
    } else if (name === 'RHB') {
        return 'icons icon_myaccount_banking_rhb';
    } else if (name === 'SCB' && currency=='SGD') {
        return 'icons icon_myaccount_banking_scb';
    } else if (name === 'SCB' && currency=='THB') {
        return 'icons icon_myaccount_banking_scbt';
    } else if (name === 'TMB') {
        return 'icons icon_myaccount_banking_tmb';
    } else if (name === 'UOB') {
        return 'icons icon_myaccount_banking_uob';
    } else if (name === 'ZGJCKYH') {
        return 'icons icon_myaccount_banking_zgjckyh';
    } else if (name === 'ZGNYFZYH') {
        return 'icons icon_myaccount_banking_zgnyfzyh';
    } else if (name === 'ZGRMYH') {
        return 'icons icon_myaccount_banking_zgrmyh';
    } else if (name === 'AMB') {
        return 'icons icon_myaccount_banking_amb';
    } else if (name === 'BAY') {
        return 'icons icon_myaccount_banking_bay';
    } else if (name === 'BBL') {
        return 'icons icon_myaccount_banking_bbl';
    } else if (name === 'BCA') {
        return 'icons icon_myaccount_banking_bca';
    } else if (name === 'BDMN') {
        return 'icons icon_myaccount_banking_bdmn';
    } else if (name === 'BIMB') {
        return 'icons icon_myaccount_banking_bimb';
    } else if (name === 'BNI') {
        return 'icons icon_myaccount_banking_bni';
    } else if (name === 'BOA') {
        return 'icons icon_myaccount_banking_boa';
    } else if (name === 'BR') {
        return 'icons icon_myaccount_banking_br';
    } else if (name === 'BRI') {
        return 'icons icon_myaccount_banking_bri';
    }


    return '';
}


var mapSubcategoryClass = function(obj) {
	if (obj.category === 'C') {
		if (obj.name === 'Allbet') {
			return 'icons icon_home_logo_ab';
		} else if (obj.name === 'GoldDeluxe') {
			return 'icons icon_home_logo_gd';
		} else if (obj.name === 'BMK' || obj.name === 'Bamako2') {
			return 'icons icon_home_logo_evo';
		} else if (obj.name === 'Gameplay') {
			return 'icons icon_home_logo_gp';
		} else if (obj.name === 'DreamGaming') {
			return 'icons icon_home_logo_dream';
		} else if (obj.name === 'AsiaGaming') {
			return 'icons icon_home_logo_ag';
		} else if (obj.name === 'PlayTech') {
			return 'icons icon_home_logo_pt';
		} else if (obj.name === 'SexyBaccarat') {
			return 'icons icon_home_logo_sexy';
		} else if (obj.name === 'MicroGamingPlus') {
			return 'icons icon_home_logo_mgp';
		}
	} else if (obj.category === 'SL') {
		if (obj.name === 'SpadeGaming') {
			return 'icons icon_home_logo_sg';
		} else if (obj.name === 'PlayTech') {
			return 'icons icon_home_logo_pt_slot';
		} else if (obj.name === 'MicroGaming') {
			return 'icons icon_home_logo_mg';
		} else if (obj.name === 'TTG') {
			return 'icons icon_home_logo_tt';
		} else if (obj.name === '918Kiss' || obj.name === '918Kiss2') {
			return 'icons icon_home_logo_918';
		} else if (obj.name === 'Ultimate') {
			return 'icons icon_home_logo_ultimate';
		} else if (obj.name === 'AsiaGaming') {
			return 'icons icon_home_logo_ag_slot';
		} else if (obj.name === 'PlayNGo') {
			return 'icons icon_home_logo_pg';
		} else if (obj.name === 'Gameplay') {
			return 'icons icon_home_logo_gp_slot';
		} else if (obj.name === 'MicroGamingPlus') {
			return 'icons icon_home_logo_mgp_slot';
		}
	} else if (obj.category === 'S') {
		if (obj.name === 'CMD368') {
			return 'icons icon_home_logo_cmd';
		} else if (obj.name === 'IBC') {
			return 'icons icon_home_mb';
		} else if (obj.name === 'SBO') {
			return 'icons icon_home_logo_sb';
		} else if (obj.name === 'IBC_WAP') {
			return 'icons icon_home_mb';
		} else if (obj.name === 'SBO_WAP') {
			return 'icons icon_home_logo_sb_wap';
		} else if (obj.name === 'SBO_WORLDCUP') {
			return 'icons icon_home_logo_sb_lite';
		}
	} else if (obj.category === 'F') {
		if (obj.name === 'GG Fishing') {
			return 'icons icon_home_logo_gg';
		} else if (obj.name === 'SG Fishing') {
			return 'icons icon_home_logo_fg';
		}
	} else if (obj.category === 'L') {
		if (obj.name === 'QQThai Lottery') {
			return 'icons icon_home_logo_qq';
		}
	}
}

showProceedLoginDialog= function(modal){
	var modalInstance = modal.open({
		animation: true,
		backdropClass :"proceed-login-backdrop",
		ariaLabelledBy: 'Please Login',
		ariaDescribedBy: 'Please Login',
		templateUrl: getMobileTemplateSrc('app/mobile/account/login/proceed-login.html', 'login', 'login', 'proceedlogin'),
		windowTemplateUrl: getMobileTemplateSrc('app/mobile/account/login/customizedModal.html', 'login', 'custom-modal', 'proceedlogin'),
		controller: 'ProceedLoginController',
		controllerAs: 'vm',
		size: 'sm',
		resolve: {
			
		}
	});
}

mapSpecialIconClass = function(obj) {
	// if (obj.category === 'S') {
	// 	if (obj.name === 'IBC_WAP') {
	// 		return 'absolute icons icon_home_logo_wap';
	// 	} else if (obj.name === 'SBO_WAP') {
	// 		return 'absolute icons icon_home_logo_wap';
	// 	} else if (obj.name === 'SBO_WORLDCUP') {
	// 		return 'absolute icons icon_home_logo_feather';
	// 	}
	// }
	return '';
}

mSubmenuDisplayMapping = function(submenuList) {
	var returnList = [];
	var rowItemContainer = [];
	// separate into 2d array, list of 4 item
	var counter = 0;
	for (var i = 0; i < submenuList.length; i++) {
		if (i > 0 && (i % 4 == 0)) {
			returnList.push(rowItemContainer);
			rowItemContainer = [];
		}
		rowItemContainer.push(submenuList[i]);
	}
	
	returnList.push(rowItemContainer);
	
	return returnList;
}

mCategorySelectedEvent = function(obj) {
	obj.isChecked = true;
	var aElement = document.getElementById('category-' + obj.submenu);
	aElement.classList.add('on');
}

mHideSubmenu = function(obj) {
	var aElement = document.getElementById('category-' + obj.submenu);
	aElement.classList.remove('on');
}

mGetSidebarMenuMapping = function(isLogin) {
	var items = [
    	{ 
    		id: "sm-1",
    		index: 1, heading: "global.sidebar.home.heading", 
    		image: "m-sidemenu-home", 
    		imagehover: "m-sidemenu-home-hover",
    		state: "mhome",
    		name: 'home',
    		subItems: []
    	},
    	{
    		id: "sm-2",
    		index: 2, heading: "global.sidebar.funds.heading", 
    		image: "m-sidemenu-funds", 
    		imagehover: "m-sidemenu-funds-hover",
    		state: "mfunds",
    		name: 'funds',
    		subItems: []
    		/*
    		subItems: [
    			{index: 1, sref: "mdeposit", heading: "global.sidebar.funds.deposit"},
    			{index: 2, sref: "mtransfer", heading: "global.sidebar.funds.transfer"},
    			{index: 3, sref: "mwithdraw", heading: "global.sidebar.funds.withdraw"},
    			{index: 4, sref: "mhistory", heading: "global.sidebar.funds.history"}
    		]
    		*/
    	},
    	{
    		id: "sm-3",
    		index: 3, heading: "global.sidebar.account.heading", 
    		image: "m-sidemenu-account", 
    		imagehover: "m-sidemenu-account-hover",
    		state: "mmyaccount",
    		name: 'account',
    		subItems: []
    	},
    	{
    		id: "sm-4",
    		index: 4, heading: "global.sidebar.promotion.heading", 
    		image: "m-sidemenu-promo", 
    		imagehover: "m-sidemenu-promo-hover",
    		state: "mpromotion",
    		name: 'promotion',
    		subItems: []
    	},
    	{
    		id: "sm-5",
    		index: 5, heading: "global.sidebar.contact-us.heading", 
    		image: "m-sidemenu-contact", 
    		imagehover: "m-sidemenu-contact-hover",
    		state: "mcontact",
    		name: 'contact',
    		subItems: []
    	},
    	{
    		id: "sm-6",
    		index: 6, heading: "global.sidebar.language.heading", 
    		image: "m-sidemenu-language", 
    		imagehover: "m-sidemenu-language-hover",
    		state: "mlanguage",
    		name: 'language',
    		subItems: []
    	},
    	{
    		id: "sm-7",
    		index: 7, heading: "global.sidebar.desktop.heading", 
    		image: "m-sidemenu-desktop", 
    		imagehover: "m-sidemenu-desktop-hover",
    		name: 'desktop',
    		subItems: []
    	},
    	{
			id: "sm-8",
			index: 8, heading: "global.sidebar.download.heading", 
			image: "m-sidemenu-download", 
			imagehover: "m-sidemenu-download-hover",
			state: "mdownload",
			name: 'download',
			subItems: []
		},
    	{
    		id: "sm-9",
    		index: 9, heading: "global.sidebar.logout.heading", 
    		image: "m-sidemenu-logout", 
    		imagehover: "m-sidemenu-logout-hover",
    		name: 'logout',
    		subItems: []
    	}
    ];
    
    if (isLogin == false) {
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 2; })[0]), 1 );
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 3; })[0]), 1 );
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 9; })[0]), 1 );
    }
    return items;
}

mapSidemenuClass = function(obj) {
	if (obj.name === 'home') {
		return 'icon-home-big';
	} else if (obj.name === 'funds') {
		return 'icon-funds-big';
	} else if (obj.name === 'account') {
		return 'icon-account-big';
	} else if (obj.name === 'promotion') {
		return 'icon-promotion-big';
	} else if (obj.name === 'contact') {
		return 'icon-contact';
	} else if (obj.name === 'language') {
		return 'icon-language';
	} else if (obj.name === 'desktop') {
		return 'icon-desktop';
	} else if (obj.name === 'download') {
		return 'icon-download';
	} else if (obj.name === 'logout') {
		return 'icon-logout';
	}
	return '';
}

mBottomNavbarHideShowEvent = function(isLogin) {
	if (isLogin == false) {
		var footerMainElem = document.getElementById('footerMain');
		footerMainElem.style.display = 'none';
	}
}

mBottomNavbarHighlightEvent = function(state) { 
	// default unhighlight all icon first
	var icons = document.getElementsByClassName("botnavbar");
	
	for (var i = 0; i < icons.length; i++) {
		if (icons[i].classList.contains('on')) {
			icons[i].classList.remove('on');
		}
	}
	
	if (state == "mhome") {
		icons[0].classList.add('on');
	}
	else if (state == "mfunds") {
		icons[1].classList.add('on');
	}
	else if (state === 'mmyaccount') {
		icons[2].classList.add('on');
	}
	else if (state == "mpromotion") {
		icons[3].classList.add('on');
	}
	
	icons = null;
}

mBankTransferRoundDateTime = function(datetime) {
	var year = datetime.getFullYear().toString();
	var month = datetime.getMonth() + 1;
	if (month < 10) {
		month = '0' + month;
	}
	var day = datetime.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	var hour = datetime.getHours();
	if (hour < 10) {
		hour = '0' + hour;
	}
	var minute = datetime.getMinutes();
	if (minute < 10) {
		minute = '0' + minute;
	}

	return year + '-' + month + '-' + day + 'T' + hour + ':' + minute;
}

mBankTransferSubmitDepositGetDateTimeValue = function() {
	return $('#depositDateTime')[0].value;
}

mHistoryFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var transType = filterList[i].transType;
		var divIDName = "#" + transType;
		var div = $(divIDName)[0];
		
		if (transType === selectedFilter) {
			div.className = "on ui-link";
		} else {
			div.className = "ui-link";
		}
	}
}

mOpenHistoryFilterDialog = function() {
	var overlay = document.getElementById('historyPop-screen');
	overlay.className = 'ui-popup-screen ui-overlay-a in';
	var popup = document.getElementById('historyPop-popup');
	popup.className = 'ui-popup-container pop in ui-popup-active';
}

mCloseHistoryFilterDialog = function() {
	var overlay = document.getElementById('historyPop-screen');
	overlay.className = 'ui-popup-screen ui-overlay-a ui-screen-hidden';
	var popup = document.getElementById('historyPop-popup');
	popup.className = 'ui-popup-container pop out ui-popup-hidden ui-popup-truncate';
}

initIENotSupport = function () {
	
	if (!String.prototype.startsWith) {
		Object.defineProperty(String.prototype, 'startsWith', {
			value: function(search, pos) {
				pos = !pos || pos < 0 ? 0 : +pos;
				return this.substring(pos, pos + search.length) === search;
			}
		});
	}

	if (!String.prototype.includes) {
		Object.defineProperty(String.prototype, 'includes', {
		  value: function(search, start) {
			if (typeof start !== 'number') {
			  start = 0
			}
			
			if (start + search.length > this.length) {
			  return false
			} else {
			  return this.indexOf(search, start) !== -1
			}
		  }
		})
	  }
}

changeToProxyUrl = function (url, oldUrl, replace, proxyUrl) {
	return url;
    // if (proxyUrl.includes('www.001.com')) {  // if local no need change
    //     return url;
    // } else {
	// 	// return 'http://bolaking.staging.zt828.net/banner/ui/eventsCallback';
    //     return url.split(oldUrl).join(replace)
    // }
}

// special requirement, click on casino and fishing submenu direct open game
navbarSpecialSubmenuChecking = function(submenuObj, isLogin, language) {
	var returnJson = {
		"specialCheck": false,
		"providerParam": null,
		"specialUrl": null
	};
	if (!affPortal) {
		// submenu handling
		if (isLogin && submenuObj.category) {
			if (submenuObj.category === 'C') {
				if (submenuObj.provider === 'AB') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameHall: 100, //default select hall 100
						provider: 'AB',
						isMobile: false,
						isFun: false,
						languageKey: language
					};
				} else if (submenuObj.provider === 'GD') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameHall: 'N',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'GD'
					};
				} else if (submenuObj.provider === 'BMK') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: '5919',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'BMK'
					};
				} else if (submenuObj.provider === 'BMK2') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'baccarat',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'BMK2'
					};
				} else if (submenuObj.provider === 'GP') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'roulette',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'GP'
					};
				} else if (submenuObj.provider === 'DG') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: '1',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'DG'
					};
				} else if (submenuObj.provider === 'AG') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: '0',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'AG'
					};
				} else if (submenuObj.provider === 'PT') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'bal',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'PT'
					};
				} else if (submenuObj.provider === 'UG') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						isMobile: false,
						isFun: false,
						languageKey: language,
						provider: 'UG',
						gameCode: 'BACCARAT',
					};
				} else if (submenuObj.provider === 'MGP') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						isMobile: false,
						isFun: false,
						languageKey: language,
						provider: 'MGP',
						gameCode: 'SMG_titaniumLiveGames_Baccarat',
					};
				}
			} else if (submenuObj.category === 'F') {
				if (submenuObj.provider === 'GG') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: '101',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'GG'
					};
				} else if (submenuObj.provider === 'PT') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'cashfi',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'PT'
					};
				} else if (submenuObj.provider === 'SG') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'F-SF01',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'SG'
					};
				}
			} else if (submenuObj.category === 'S') {
				if (submenuObj.provider === 'SBO') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'Football',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'SBO'
					};
				} else if (submenuObj.provider === 'BR') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'VFL',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'BR',
						redirectFromNavbar: true
					};
				}
			} else if (submenuObj.category === 'SL') {
				if (submenuObj.provider === 'UL') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: 'S-UL01',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'UL'
					};
				}
			} else if (submenuObj.category === 'L') {
				if (submenuObj.provider === 'QQT') {
					returnJson.specialCheck = true;
					returnJson.providerParam = {
						domain: location.hostname,
						gameCode: '1',
						isMobile: false,
						isFun: false,
						isMenuMode: true,
						isMiniGame: false,
						languageKey: language,
						provider: 'QQT'
					};
				}
			}
		}
		// menu handling
		else if (isLogin && submenuObj.provider) {
			if (submenuObj.provider === 'IDN') {
				returnJson.specialCheck = true;
				returnJson.providerParam = {
					domain: location.hostname,
					gameCode: 'LPK',
					isMobile: false,
					isFun: false,
					isMenuMode: true,
					isMiniGame: false,
					languageKey: language,
					provider: 'IDN'
				};
			}
		}
		// john terry menu handling
		else if (submenuObj.submenu === 'B') {
			returnJson.specialCheck = true;
			returnJson.specialUrl = 'https://www.johnterrybk8.com/';
		}
	} else {
		// handle affiliate mobile menu auto close when click on sidemenu
		if ( $('body').hasClass('overflow offcanvas') ) {
            $('body').removeClass('overflow offcanvas');
        }
		if ($('#sidemenu-toggle-btn').hasClass('active')) {
			$('#sidemenu-toggle-btn').toggleClass('active');
		}
		
		// navigate to portal home menu handling
		if (submenuObj.submenu === 'PortalHome') {
			returnJson.specialCheck = true;
			returnJson.specialUrl = 'https://' + location.hostname.replace('aff.', '');
		}
		// open brochure popup handling
		else if (submenuObj.submenu === 'AffBrochure') {
			returnJson.specialCheck = true;
			returnJson.specialUrl = 'https://' + location.hostname + '/public/html/new_bk8/affiliate/BK8AffiliateProgramme_' + countryLanguageKey + '.pdf';
		}
	}
	return returnJson;
}

mPromotionFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var transType = filterList[i].id;
		var divIDName = "#" + transType;
		var div = $(divIDName)[0];
		
		if (transType === selectedFilter) {
			div.className = "on ui-link";
		} else {
			div.className = "ui-link";
		}
	}
}

mCommonCasinoGetBackground = function(provider) {
	var headerImg = document.getElementById('casino-img');
	var bodyImg = document.getElementById('casino-body-img');
	if (provider === 'GD') {
		headerImg.setAttribute('http-src', 'm-casino-gd-logo');
		bodyImg.setAttribute('http-src', 'm-casino-gd');
	} else if (provider === 'BMK' || provider === 'BMK2') {
		headerImg.setAttribute('http-src', 'm-casino-eg-logo');
		bodyImg.setAttribute('http-src', 'm-casino-eg');
	} else if (provider === 'DG') {
		headerImg.setAttribute('http-src', 'm-casino-dg-logo');
		bodyImg.setAttribute('http-src', 'm-casino-dg');
	} else if (provider === 'AG') {
		headerImg.setAttribute('http-src', 'm-casino-ag-logo');
		bodyImg.setAttribute('http-src', 'm-casino-ag');
	}
	repopulateImage();
}

mGetIdnContentBackground = function(os) {
}

mSlotGameCategorySelectedEvent = function(id, categoryList) {
	if (categoryList) {
		for (var i = 0; i < categoryList.length; i++) {
			var div = $('#' + categoryList[i])[0];
			if (id === categoryList[i]) {
				div.className = 'on ui-link';
			} else {
				div.className = 'ui-link';
			}
		}
	}
	$("#" + id)[0].className = "on ui-link";
}

mSlotGameKeywordFilterTextboxClickEvent = function(showSearch) { }

mSlotGetPaginationItemsPerPage = function() {
	return 18;
}

mSlotGameItemClickedEvent = function(gameObj, gameList) {
	var overlayElem = document.getElementById('gamedetail-popup-overlay');
	overlayElem.style.display = 'block';

	var popupElem = document.getElementById('gamedetail-popup');
	popupElem.style.display = 'block';
}

mSlotGameItemHideGamePopup = function() {
	var overlayElem = document.getElementById('gamedetail-popup-overlay');
	overlayElem.style.display = 'none';

	var popupElem = document.getElementById('gamedetail-popup');
	popupElem.style.display = 'none';
}

mDownloadFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var id = filterList[i].name;
		var divIDName = '#' + id;
		var div = $(divIDName)[0];

		if (id === selectedFilter) {
			div.className = 'on ui-link';
		} else {
			div.className = 'ui-link';
		}
	}
}

mDownloadGetDownloadList = function(jsonObj) {
	return jsonObj['mobile_newbk8'];
}

mPopupTransferProviderIcon = function(provider) {
	if (provider === 'CMD') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_cmd.png';
	} else if (provider === 'AB') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_ab.png';
	} else if (provider === 'GD') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_gd.png';
	} else if (provider === 'BMK' || provider === 'BMK2') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_evo.png';
	} else if (provider === 'GP') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_gp.png';
	} else if (provider === 'DG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_dream.png';
	} else if (provider === 'AG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_ag.png';
	} else if (provider === 'PT') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_pt.png';
	} else if (provider === 'GG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_gg.png';
	} else if (provider === 'SG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_sg.png';
	} else if (provider === 'MG' || provider === 'MGP') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_mg.png';
	} else if (provider === 'TTG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_tt.png';
	} else if (provider === 'UL') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_ultimate.png';
	} else if (provider === 'SCR' || provider === 'SCR2') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_918.png';
	} else if (provider === 'IDN') {
		return 'public/new_bk8/content/images/mobile/home/icon_home_menu_poker_on.png';
	} else if (provider === 'SBO' || provider === 'SBO2') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_sb.png';
	} else if (provider === 'UG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_sexy.png';
	} else if (provider === 'QQT') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_qq.png';
	} else if (provider === 'PG') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_logo_pg.png';
	} else if (provider === 'IBC') {
		return 'public/new_bk8/content/images/mobile/home/firms/icon_home_mb.png';
	}
}

templateGetMenuClassEvent = function (menu, state) {
	for (var i = 1; i <= 3; i++) {
		var menuElem = document.getElementById('menu-' + menu.name + '-' + i);
		if (menuElem) {
			if (menu.sref === state) {
				menuElem.classList = 'active';
			} else {
				menuElem.classList = '';
			}
		}
	}
}

templateMenuPopulatedCompleted = function () {
	offcanvasMenu();
	burgerMenu();
	dropdown();
	parallax();
	mobileMenuOutsideClick();
}

affBannerOnloadEvent = function (bannerList, bannerObj) {
	setTimeout(function () {
		var slideElem = document.getElementById('aff-banner-' + bannerObj.id);
		slideElem.style.backgroundImage = 'url(' + bannerObj.src + ')';
	
		if (bannerList && bannerList.length > 0 && bannerList[bannerList.length - 1].id === bannerObj.id) {
			sliderMain();
		}
	}, 500);
}

templateBodyOnloadEvent = function() {
	contentWayPoint();
	counter();
}

bk8NewSettingPageInit = function(updateDateTimeCallback, updateDatePickerCallback) {

	// Vars
	var reloadButton  = document.querySelector( '.reload' );
	var reloadSvg     = document.querySelector( 'svg' );
	var reloadEnabled = true;
	var rotation      = 0;

	var currentPalette = 0;

	// Events
	reloadButton.addEventListener('click', function() { reloadClick() });

	// Functions
	function reloadClick() {

	  reloadEnabled = false;
	  rotation -= 180;
	  
	  // Eh, this works.
	  if(reloadSvg) {
		  reloadSvg.style.webkitTransform = 'translateZ(0px) rotateZ( ' + rotation + '360deg )';
		  reloadSvg.style.MozTransform  = 'translateZ(0px) rotateZ( ' + rotation + '360deg )';
		  reloadSvg.style.transform  = 'translateZ(0px) rotateZ( ' + rotation + '360deg )';
	  }
	}

	// Show button.
	setTimeout(function() {
		if(reloadButton)
			reloadButton.classList.add('active');
	}, 1);

	$('.button img').click(function() {
		var src = $(this).attr('src');
		$(this).attr('src', src.replace('-off', '-on'));   //change off to on
		$('img').not(this).attr('src', function(_, src) {  //change on to off
			return src.replace('-on', '-off');
		});
	});

    $(document).ready( function () {
    	initDateTimePicker(jQuery, updateDateTimeCallback);
        $('#picker').dateTimePicker();
    })
    
	// Date Picker.
	initBk8DatePickerClass = function() {
	  // IE11 forEach, padStart
	  (function () {
	    if ( typeof NodeList.prototype.forEach === "function" ) return false;
	    NodeList.prototype.forEach = Array.prototype.forEach;
	  })();

	  (function () {
	    if ( typeof String.prototype.padStart === "function" ) return false;
	    String.prototype.padStart = function padStart(length, value) {
	      var res = String(this);
	      if(length >= (value.length + this.length)) {
	        for (var i = 0; i <= (length - (value.length + this.length)); i++) {
	          res = value + res;
	        }
	      }
	      return res;
	    };
	  })();

	  var datePickerTpl = '<div class="yearMonth"><a class="previous">&lsaquo;</a><span class="year">{y}</span>-<span class="month">{m}</span><a class="next">&rsaquo;</a></div><div class="days"><a>1</a><a>2</a><a>3</a><a>4</a><a>5</a><a>6</a><a>7</a><a>8</a><a>9</a><a>10</a><a>11</a><a>12</a><a>13</a><a>14</a><a>15</a><a>16</a><a>17</a><a>18</a><a>19</a><a>20</a><a>21</a><a>22</a><a>23</a><a>24</a><a>25</a><a>26</a><a>27</a><a>28</a><a>29</a><a>30</a><a>31</a>';

	  function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	  }

	  function hideInvalidDays(dp, month, year){
	    dp.querySelectorAll(".days a").forEach(function(a){
	      a.style.display = "inline-block";
	    });
	    var days = daysInMonth(month, year);
	    var invalidCount = 31 - days;
	    if(invalidCount > 0) {
	      for (var j = 1; j <= invalidCount; j++) {
	        dp.querySelector(".days a:nth-last-child(" + j + ")").style.display = "none";
	      }
	    }
	  }

	  function clearSelected(dp) {
	    dp.querySelectorAll(".days a.selected").forEach(function(e){
	      e.classList.remove("selected");
	    });
	  }

	  function setMonthYear(dp, month, year, input) {
	    dp.querySelector(".month").textContent = String(month).padStart(2, "0");
	    dp.querySelector(".year").textContent = year;
	    clearSelected(dp);
	    hideInvalidDays(dp, month, year);
	    if(input && input.value) {
	      var date = input.value.split("-");
	      var curYear = parseInt(dp.querySelector(".year").textContent), curMonth = parseInt(dp.querySelector(".month").textContent);
	      if(date[0] == curYear && date[1] == curMonth) {
	        dp.querySelector(".days a:nth-child(" + parseInt(date[2]) + ")").className = "selected";
	      }
	    }
	  }
	  
	  document.querySelectorAll(".datepicker").forEach(function(input) {
	    input.setAttribute("readonly", "true");
	    var dp = document.createElement("div");
	    dp.className = "contextmenu";
	    dp.style.left = input.offsetLeft + "px";
	    dp.style.top = input.offsetTop + input.offsetHeight + "px";
	    var now = new Date();
	    dp.insertAdjacentHTML('beforeEnd', datePickerTpl.replace("{m}", String(now.getMonth() + 1).padStart(2, "0")).replace("{y}", now.getFullYear()));
	    hideInvalidDays(dp, now.getMonth() + 1, now.getFullYear());

	    dp.querySelector("a.previous").addEventListener("click", function(e){
	      var curYear = parseInt(dp.querySelector(".year").textContent), curMonth = parseInt(dp.querySelector(".month").textContent);
	      var firstMonth = curMonth - 1 == 0;
	      setMonthYear(dp, firstMonth ? 12 : curMonth - 1, firstMonth ? curYear - 1 : curYear, input);
	    });

	    dp.querySelector("a.next").addEventListener("click", function(e){
	      var curYear = parseInt(dp.querySelector(".year").textContent), curMonth = parseInt(dp.querySelector(".month").textContent);
	      var lastMonth = curMonth + 1 == 13;
	      setMonthYear(dp, lastMonth ? 1 : curMonth + 1, lastMonth ? curYear + 1 : curYear, input);
	    });

	    dp.querySelectorAll(".days a").forEach(function(a){
	      a.addEventListener("click", function(e) {
	        clearSelected(dp);
	        e.target.className = "selected";
	        input.value = dp.querySelector(".year").textContent + "-" + dp.querySelector(".month").textContent + "-" + this.text.padStart(2, "0");
	        if(updateDatePickerCallback)
	        	updateDatePickerCallback(input.value, input.getAttribute('id'));
	      });
	    });

	    input.parentNode.insertBefore(dp, input.nextSibling);

	    input.addEventListener("focus", function(){
	      if (input.value){
	        var date = input.value.split("-");
	        setMonthYear(dp, date[1], date[0]);
	        dp.querySelector(".days a:nth-child(" + parseInt(date[2]) + ")").className = "selected";
	      }
	    });
	    
	    input.value = formatDateStr(new Date()); // initialize
	  });
	};
	initBk8DatePickerClass();
	
	// update datepicker value from radios named "transactionDate"
	
	var radios = document.getElementsByName('transactionDate');
	for(var x=0; x<radios.length; x++) {
			
		var radio = radios[x]
		var d = new Date();
		switch(radio.getAttribute('day')) {
		case 'today'	: break;
		case 'threedays': d.setDate(d.getDate() - 3);
						  break;
		case 'oneweek'	: d.setDate(d.getDate() - 7);
						  break;
		case 'onemonth'	: d.setMonth(d.getMonth() - 1);
						  break;
		}
		this.value = formatDateStr(d);
		
		radio.addEventListener('click', function() {
			var now = new Date();
			now.setHours(0);
			now.setMinutes(0);
			now.setSeconds(0);

			var historyStartDate = document.getElementById('historyStartDate');
			var historyEndDate = document.getElementById('historyEndDate');

			historyEndDate.value = formatDateStr(now);
			if(this.getAttribute('day') === "today") {
				historyStartDate.value = formatDateStr(now);
			}
			if(this.getAttribute('day') === 'threedays') {
				now.setDate(now.getDate() - 3);
				historyStartDate.value = formatDateStr(now);
			}
			if(this.getAttribute('day') === 'oneweek') {
				now.setDate(now.getDate() - 7);
				historyStartDate.value = formatDateStr(now);
			}
			if(this.getAttribute('day') === 'onemonth') {
				now.setMonth(now.getMonth() - 1);
				historyStartDate.value = formatDateStr(now);
			}
		});
	}
	
	function formatDateStr(dateObj) {
		var year = dateObj.getFullYear() + '';
		var month = (parseInt(dateObj.getMonth())+1) + '';
		var date = dateObj.getDate() + '';
		if(month.length == 1)
			month = '0' + month;
		if(date.length == 1)
			date = '0' + date;
		return year + '-' + month + '-' + date;
	}

	(function($) {

	    $.organicTabs = function(el, options) {
	    
	        var base = this;
	        base.$el = $(el);
	        base.$nav = base.$el.find(".nav");
	                
	        base.init = function() {
	        
	            base.options = $.extend({},$.organicTabs.defaultOptions, options);
	            
	            // Accessible hiding fix
	            $(".hide").css({
	                "position": "relative",
	                "top": 0,
	                "left": 0,
	                "display": "none"
	            }); 
	            
	            base.$nav.delegate("li > a", "click", function() {
	            	
	                // Figure out current list via CSS class
	                var curList = base.$el.find("a.current").attr("href").substring(1),
	                
	                // List moving to
	                    $newList = $(this),
	                    
	                // Figure out ID of new list
	                    listID = $newList.attr("href").substring(1),
	                
	                // Set outer wrapper height to (static) height of current inner list
	                    $allListWrap = base.$el.find(".list-wrap"),
	                    curListHeight = $allListWrap.height();
	                $allListWrap.height(curListHeight);
	                                        
	                if ((listID != curList) && ( base.$el.find(":animated").length == 0)) {
	                                            
	                    // Fade out current list
	                    base.$el.find("#"+curList).fadeOut(base.options.speed, function() {
	                        
	                        // Fade in new list on callback
	                        base.$el.find("#"+listID).fadeIn(base.options.speed);
	                        
	                        // Adjust outer wrapper to fit new list snuggly
	                        var newHeight = base.$el.find("#"+listID).height();
	                        $allListWrap.animate({
	                            height: newHeight
	                        });
	                        
	                        // Remove highlighting - Add to just-clicked tab
	                        base.$el.find(".nav li a").removeClass("current");
	                        $newList.addClass("current");
	                            
	                    });
	                    
	                }   
	                
	                // Don't behave like a regular link
	                // Stop propegation and bubbling
	                return false;
	            });
	            
	        };
	        base.init();
	    };
	    
	    $.organicTabs.defaultOptions = {
	        "speed": 300
	    };
	    
	    $.fn.organicTabs = function(options) {
	        return this.each(function() {
	            (new $.organicTabs(this, options));
	        });
	    };
	    
	})(jQuery);
	
	function initDateTimePicker ($, updateDateTimeCallback) {
	    'use strict';
	    $.fn.dateTimePicker = function (options) {

	        var settings = $.extend({
	            selectData: "now",
	            dateFormat: "YYYY-MM-DDTHH:mm:ss",
	            showTime: true,
	            locale: 'en',
	            positionShift: { top: 20, left: 0},

	            buttonTitle: "Select"
	        }, options);
	        moment.locale(settings.locale);
	        var elem = this;
	        var limitation = {"hour": 23, "minute": 59};
	        var mousedown = false;
	        var timeout = 800;
	        var selectDate = settings.selectData == "now" ? moment() : moment(settings.selectData, settings.dateFormat);
	        if (selectDate < moment()) {
	            selectDate = moment();
	        }
	        var startDate = copyDate(moment());
	        var lastSelected = copyDate(selectDate);
	        if(updateDateTimeCallback != null)
	        	updateDateTimeCallback(selectDate.format(settings.dateFormat));
	        return this.each(function () {
	            if (lastSelected != selectDate) {
	                selectDate = copyDate(lastSelected);
	            }
	            elem.addClass("dtp_main");
	            updateMainElemGlobal();
	            //  elem.text(selectDate.format(settings.dateFormat));
	            function updateMainElemGlobal() {
	                var arrF = settings.dateFormat.split(' ');
	                if (settings.showTime && arrF.length != 2) {
	                    arrF.length = 2;
	                    arrF[0] = 'DD/MM/YY';
	                    arrF[1] = 'HH:mm:ss';
	                }
	                var $s = $('<span>');
	                $s.text(lastSelected.format(arrF[0]));
	                elem.empty();
	                elem.append($s);
	                $s = $('<i>');
	                $s.addClass('');
	                elem.append($s);
	                if (settings.showTime) {
	                    $s = $('<span>');
	                    $s.text(lastSelected.format(arrF[1]));
	                    elem.append($s);
	                    $s = $('<i>');
	                    $s.addClass('');
	                    elem.append($s);
	                }
	            }
	            elem.on('click', function () {
	                var $win = $('<div>');
	                $win.addClass("dtp_modal-win");
	                var $body = $('body');
	                $body.append($win);
	                var $content = createContent();
	                $body.append($content);
	                var offset = elem.offset();
	                $content.css({top: (offset.top + settings.positionShift.top) + "px", left: (offset.left + settings.positionShift.left) + "px"});
	                feelDates(selectDate);
	                $win.on('click', function () {
	                    $content.remove();
	                    $win.remove();
	                })
	                if (settings.showTime) {
	                    attachChangeTime();
	                    var $fieldTime = $('#field-time');
	                    var $hour = $fieldTime.find('#d-hh');
	                    var $minute = $fieldTime.find('#d-mm');
	                }

	                function feelDates(selectM) {
	                    var $fDate = $content.find('#field-data');
	                    $fDate.empty();
	                    $fDate.append(createMonthPanel(selectM));
	                    $fDate.append(createCalendar(selectM));
	                }

	                function createCalendar(selectedMonth) {
	                    var $c = $('<div>');
	                    $c.addClass('dtp_modal-calendar');
	                    for (var i = 0; i < 7; i++) {
	                        var $e = $('<div>');
	                        $e.addClass('dtp_modal-calendar-cell dtp_modal-colored');
	                        $e.text(moment().weekday(i).format('ddd'));
	                        $c.append($e);
	                    }
	                    var m = copyDate(selectedMonth);
	                    m.date(1);
	                    // console.log(m.format('DD--MM--YYYY'));
	                    // console.log(selectData.format('DD--MM--YYYY'));
	                    // console.log(m.weekday());
	                    var flagStart = totalMonths(selectedMonth) === totalMonths(startDate);
	                    var flagSelect = totalMonths(lastSelected) === totalMonths(selectedMonth);
	                    var cerDay = parseInt(selectedMonth.format('D'));
	                    var dayNow = parseInt(startDate.format('D'));
	                    for (var i = 0; i < 6; i++) {
	                        for (var j = 0; j < 7; j++) {
	                            var $b = $('<div>');
	                            $b.html('&nbsp;');
	                            $b.addClass('dtp_modal-calendar-cell');
	                            if (m.month() == selectedMonth.month() && m.weekday() == j) {
	                                var day = parseInt(m.format('D'));
	                                $b.text(day);
//	                                if (flagStart && day < dayNow) {
//	                                    $b.addClass('dtp_modal-grey');
//	                                }
	                                if (flagSelect && day == cerDay) {
	                                    $b.addClass('dtp_modal-cell-selected');
	                                }
	                                else {
	                                    $b.addClass('cursorily');
	                                    $b.bind('click', changeDate);
	                                }
	                                m.add(1, 'days');
	                            }
	                            $c.append($b);
	                        }
	                    }
	                    return $c;
	                }

	                function changeDate() {

	                    var $div = $(this);
	                    selectDate.date($div.text());
	                    lastSelected = copyDate(selectDate);
	                    updateDate();
	                    var $fDate = $content.find('#field-data');
	                    var old = $fDate.find('.dtp_modal-cell-selected');
	                    old.removeClass('dtp_modal-cell-selected');
	                    old.addClass('cursorily');
	                    $div.addClass('dtp_modal-cell-selected');
	                    $div.removeClass('cursorily');
	                    old.bind('click', changeDate);
	                    $div.unbind('click');
	                    // console.log(selectDate.format('DD-MM-YYYY'));
	                }

	                function createMonthPanel(selectMonth) {
	                    var $d = $('<div>');
	                    $d.addClass('dtp_modal-months');
	                    var $s = $('<a></a>');
	                    $s.addClass('chevron-left cursorily ico-size-month hov');
	                    //$s.attr('data-fa-mask', 'fas fa-circle');
	                    $s.bind('click', prevMonth);
	                    $d.append($s);
	                    $s = $('<span>');
	                    $s.text(selectMonth.format("MMMM YYYY"));
	                    $d.append($s);
	                    $s = $('<a></a>');
	                    $s.addClass('chevron-right cursorily ico-size-month hov');
	                    $s.bind('click', nextMonth);
	                    $d.append($s);
	                    return $d;
	                }

	                function close() {
	                    if (settings.showTime) {
	                        lastSelected.hour(parseInt($hour.text()));
	                        lastSelected.minute(parseInt($minute.text()));
	                        selectDate.hour(parseInt($hour.text()));
	                        selectDate.minute(parseInt($minute.text()));
	                    }
	                    updateDate();
	                    $content.remove();
	                    $win.remove();
	                }

	                function nextMonth() {
	                    selectDate.add(1, 'month');
	                    feelDates(selectDate);
	                }

	                function prevMonth() {
//	                    if (totalMonths(selectDate) > totalMonths(startDate)) {
	                        selectDate.add(-1, 'month');
	                        feelDates(selectDate);
//	                    }
	                }

	                function attachChangeTime() {
	                    var $angles = $($content).find('i[id^="angle-"]');
	                    // $angles.bind('click', changeTime);
	                    $angles.bind('mouseup', function () {
	                        mousedown = false;
	                        timeout = 800;
	                    });
	                    $angles.bind('mousedown', function () {
	                        mousedown = true;
	                        changeTime(this);
	                    });
	                    $angles.mouseleave('mouseleave', function(ev) {
	                    	mousedown = false;
	                    	timeout = 800;
	                    });
	                }

	                function changeTime(el) {
	                    var $el = this || el;
	                    $el = $($el);
	                    ///angle-up-hour angle-up-minute angle-down-hour angle-down-minute
	                    var arr = $el.attr('id').split('-');
	                    var increment = 1;
	                    if (arr[1] == 'down') {
	                        increment = -1;
	                    }
	                    appendIncrement(arr[2], increment);
	                    setTimeout(function () {
	                        autoIncrement($el);
	                    }, timeout);
	                }

	                function autoIncrement(el) {
	                    if (mousedown) {
	                        if (timeout > 200) {
	                            timeout -= 200;
	                        }
	                        changeTime(el);
	                    }
	                }

	                function appendIncrement(typeDigits, increment) {

	                    var $i = typeDigits == "hour" ? $hour : $minute;
	                    var val = parseInt($i.text()) + increment;
	                    if (val < 0) {
	                        val = limitation[typeDigits];
	                    }
	                    else if (val > limitation[typeDigits]) {
	                        val = 0;
	                    }
	                    $i.text(formatDigits(val));
	                    lastSelected.minute(parseInt($minute.text()));
	                    lastSelected.hour(parseInt($hour.text()));
	                    selectDate.minute(parseInt($minute.text()));
	                    selectDate.hour(parseInt($hour.text()));
	                    updateDate();
	                }

	                function formatDigits(val) {

	                    if (val < 10) {
	                        return '0' + val;
	                    }
	                    return val;
	                }

	                function createTimer() {
	                    var $div = $('<div>');
	                    $div.addClass('dtp_modal-time-mechanic');
	                    var $panel = $('<div>');
	                    $panel.addClass('dtp_modal-append');
	                    var $i = $('<i>');
	                    $i.attr('id', 'angle-up-hour');
	                    $i.addClass('chevron-top ico-size-large cursorily hov');
	                    $panel.append($i);
	                    var $m = $('<span>');
	                    $m.addClass('dtp_modal-midle');
	                    $panel.append($m);
	                    $i = $('<i>');
	                    $i.attr('id', 'angle-up-minute');
	                    $i.addClass('chevron-top ico-size-large cursorily hov');
	                    $panel.append($i);
	                    $div.append($panel);

	                    $panel = $('<div>');
	                    $panel.addClass('dtp_modal-digits');
	                    var $d = $('<span>');
	                    $d.addClass('dtp_modal-digit');
	                    $d.attr('id', 'd-hh');
	                    $d.text(lastSelected.format('HH'));
	                    $panel.append($d);
	                    $m = $('<span>');
	                    $m.addClass('dtp_modal-midle-dig');
	                    $m.html(':');
	                    $panel.append($m);
	                    $d = $('<span>');
	                    $d.addClass('dtp_modal-digit');
	                    $d.attr('id', 'd-mm');
	                    $d.text(lastSelected.format('mm'));
	                    $panel.append($d);
	                    $div.append($panel);

	                    $panel = $('<div>');
	                    $panel.addClass('dtp_modal-append');
	                    $i = $('<i>');
	                    $i.attr('id', 'angle-down-hour');
	                    $i.addClass('chevron-down  ico-size-large cursorily hov');
	                    $panel.append($i);
	                    $m = $('<span>');
	                    $m.addClass('dtp_modal-midle');
	                    $panel.append($m);
	                    $i = $('<i>');
	                    $i.attr('id', 'angle-down-minute');
	                    $i.addClass('chevron-down  ico-size-large cursorily hov');
	                    $panel.append($i);
	                    $div.append($panel);
	                    return $div;
	                }

	                function createContent() {
	                    var $c = $('<div>');
	                    if (settings.showTime) {
	                        $c.addClass("dtp_modal-content");
	                    }
	                    else {
	                        $c.addClass("dtp_modal-content-no-time");
	                    }
	                    var $el = $('<div>');
	               
	                    $c.append($el);
	                    $el = $('<div>');
	                    $el.addClass('dtp_modal-cell-date');
	                    $el.attr('id', 'field-data');
	                    $c.append($el);
	                    if (settings.showTime) {
	                        $el = $('<div>');
	                        $el.addClass('dtp_modal-cell-time');
	                        var $a = $('<div>');
	                        $a.addClass('dtp_modal-time-block');
	                        $a.attr('id', 'field-time');
	                        $el.append($a);
	                        var $line = $('<div>');
	                        $line.attr('id', 'time-line');
	                        $line.addClass('dtp_modal-time-line');
	                        $line.text(lastSelected.format(settings.dateFormat).replace('T', ' '));

	                        $a.append($line);
	                        $a.append(createTimer());
	                        var $but = $('<div>');
	                        $but.addClass('dpt_modal-button');
	                        $but.text(settings.buttonTitle);
	                        $but.bind('click', close);
	                        $el.append($but);
	                        $c.append($el);
	                    }
	                    return $c;
	                }
	                function updateDate() {
	                    if (settings.showTime) {
	                        $('#time-line').text(lastSelected.format(settings.dateFormat).replace('T', ' '));
	                    }
	                    updateMainElem();
	                    elem.next().val(selectDate.format(settings.dateFormat));
	                    if (!settings.showTime) {
	                        $content.remove();
	                        $win.remove();
	                    }
	                }

	                function updateMainElem() {
	                    var arrF = settings.dateFormat.split(' ');
	                    if (settings.showTime && arrF.length != 2) {
	                        arrF.length = 2;
	                        arrF[0] = 'DD/MM/YY';
	                        arrF[1] = 'HH:mm:ss';
	                    }
	                    var $s = $('<span>');
	                    $s.text(lastSelected.format(arrF[0]));
	                    elem.empty();
	                    elem.append($s);
	                    $s = $('<i>');
	                    $s.addClass('fa fa-calendar ico-size');
	                    elem.append($s);
	                    if (settings.showTime) {
	                        $s = $('<span>');
	                        $s.text(lastSelected.format(arrF[1]));
	                        elem.append($s);
	                        $s = $('<i>');
	                        $s.addClass('fa fa-clock-o ico-size');
	                        elem.append($s);
	                    }
	                    
	                    if(updateDateTimeCallback != null)
	        	        	updateDateTimeCallback(lastSelected.format(settings.dateFormat));
	                }
	            });

	        });

	    };

	    function copyDate(d) {
	        return moment(d.toDate());
	    }

	    function totalMonths(m) {
	        var r = m.format('YYYY') * 12 + parseInt(m.format('MM'));
	        return r;
	    }

	};
	// fa-caret-down
}

initNewBk8Sidebar = function() {
	if(jQuery('#userActivity-nav>ul>li') != null) {
		var items = jQuery('#userActivity-nav>ul>li').each(function() {

			// initialize first current tab by url
//			if(window.location.pathname.endsWith(jQuery(this).attr('tab'))) {
//				jQuery(this).addClass('current');
//				jQuery(this).find('img').attr('src', jQuery(this).find('img').attr('src').replace('.png', '_White.png'));
//			}
			
			jQuery(this).click(function() {
	            //remove previous class and add it to clicked tab
	            items.removeClass('current');
	            jQuery(this).addClass('current');

	            jQuery('li.userActivityMenuItem').each(function(i, obj) {
	              var imageSrc = jQuery(obj).children('img').attr('src').replace('_White', '');

	              jQuery(obj).children('img').attr('src', imageSrc);
	            });

	            var whiteImg = jQuery(this).children('img').attr('src').replace('.png', '_White.png');

	            jQuery(this).children('img').attr('src', whiteImg);

	            //hide all content divs and show current one
	            //var jQuerytarget = jQuery('#userActivity-nav>div.tab-content').hide().eq(items.index($(this)));
	            //$target.show('fast');
	            //alert($target.attr('id'));
//	            $('#loader').show();
//	            myFunction(jQuerytarget.attr('id'));
//	            window.location.hash = $(this).attr('tab');
	          });
	      });
	}
	
	function showTab(tab) {
		jQuery("#userActivity-nav ul li:[tab*=" + tab + "]").click();
    }
	function myFunction(tabContentId) {
		setTimeout(function() {
			showPage(tabContentId);
		}, 0);
	}
	function showPage(tabContentId) {
	  document.getElementById("loader").style.display = "none";
	  //document.getElementById("myDiv").style.display = "block";
	  $('#' + tabContentId).show();
	}
}

var affContactUsPopulateContactEvent = function(contactList) {
	setTimeout(function() {
		for (var i = 0; i < contactList.length; i++) {
			var contactObj = contactList[i];
			var contactDiv = document.getElementById('contactus-' + contactObj.name);
			var imgElem = document.createElement('img');
			if (contactObj.name === 'Email') {
				imgElem.src = 'https://www.bolaking.net/public/new_bk8/content/images/affiliate/icon_email.png';
				var anchorElem = document.createElement('a');
				anchorElem.href = 'mailto:' + contactObj.description;
				anchorElem.innerHTML = contactObj.description;
		
				contactDiv.appendChild(imgElem);
				contactDiv.appendChild(anchorElem);
			} else if (contactObj.name === 'Whatsapp') {
				imgElem.src = 'https://www.bolaking.net/public/new_bk8/content/images/affiliate/icon_whatsapp.png';
				var value = contactObj.description.replace(/ /g, '');
				value = value.replace('+', '');
				value = value.replace('-', '');
				var anchorElem = document.createElement('a');
				anchorElem.href = 'https://api.whatsapp.com/send?phone=' + value;
				anchorElem.innerHTML = contactObj.description;
		
				contactDiv.appendChild(imgElem);
				contactDiv.appendChild(anchorElem);
			} else if (contactObj.name === 'Skype') {
				imgElem.src = 'https://www.bolaking.net/public/new_bk8/content/images/affiliate/icon_skype.png';
				contactDiv.appendChild(imgElem);
				contactDiv.innerHTML += contactObj.description;
			} else if (contactObj.name === 'WeChat') {
				imgElem.src = 'https://www.bolaking.net/public/new_bk8/content/images/affiliate/icon_wechat.png';
				contactDiv.appendChild(imgElem);
				contactDiv.innerHTML += contactObj.description;
			}
		}
	}, 0);
}

affGetCommissionPlanFileUrl = function() {
	return 'https://www.bolaking.net/public/html/new_bk8/affiliate/commission_plan_' + countryLanguageKey.toLowerCase() + '.txt?' + new Date().getTime();
}

affGetFaqFileUrl = function() {
	return 'https://www.bolaking.net/public/new_bk8/affiliate/faq_' + countryLanguageKey.toLowerCase() + '.txt?' + new Date().getTime();
}

mapBankIconsForNewBk8Setting = function(bankCode, id, postfix, currency) {
	var basedPath = 'https://www.bolaking.net/public/new_bk8/content/images/newSetting/';
	var result = '';
	
	if(bankCode != 'SCB') {
		result = basedPath + bankCode + postfix;
	} else {
		if(bankCode == 'SCB') {
			if(currency === 'MYR') {
				result = basedPath + 'SCB' + postfix;
			}
			if(currency === 'THB') {
				result = basedPath + 'SCBT' + postfix;
			}
		}
	}
	
	return result;
}
