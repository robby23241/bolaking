function repopulateImage() {
	// populate image again if return image file path service too slow
	var missingImgList = document.querySelectorAll("img[http-src]");
	for (var i = 0; i < missingImgList.length; i++) {
		if (missingImgList[i].src === "" || missingImgList[i].src.indexOf("/public/images/transparentIcon.png") !== -1) {
			var attributes = missingImgList[i].attributes;
			for (var j = 0; j < attributes.length; j++) {
				if (attributes[j].nodeName === "http-src") {
					missingImgList[i].src = getDelayImgSrc(attributes[j].nodeValue);
					break;
				}
			}
			attributes = null;
		}
	}
	missingImgList = null;
}

function getDelayImgSrc(name) {
	return imageJsonCache[name];
}

function navigateHelp(page) {
	var language = countryLanguageKey.toLowerCase();
	
	window.location.href = "/help/" + language + "/" + page;
}

function navigateFaq(page) {
	var language = countryLanguageKey.toLowerCase();
	
	var path = "/info-centre/" + language + "/" + page;
	window.open(path, "_blank", "top=500, width=995, height=700");

}

function navigateInfoCentre(page) {
	var language = countryLanguageKey.toLowerCase();
	
	var path = "/info-centre/" + language + "/" + page;
	window.open(path, "_self", "top=500, width=995, height=700");

}

function getJson(path, callback) {
    function success(respond) {
		callback(respond);
	}

	function failed(jqXHR, textStatus, errorThrown) {
		console.log(errorThrown);
		callback(undefined);
	}

	var request = {
		type: 'GET',
    	url: path,
    	dataType: 'json',
		contentType: "application/json",
		success: success,
		failed: failed
    };
	
	$.ajax(request);
}

function tryGetImage(elem, elemType, url, defUrl) {
	$.ajax({
		url: url,
		type: 'HEAD',
		success: function () {
			if (elemType === 'img') {
				elem.src = url;
			}
			else if (elemType === 'divbg') {
				elem.style.backgroundImage = "url(" + url + ")";
			}
		},
		error: function () {
			if (elemType === 'img') {
				elem.src = defUrl;
			}
			else if (elemType === 'divbg') {
				elem.style.backgroundImage = "url(" + defUrl + ")";
			}
		}
	});
}

var translationDict = {};

function getTranslateFile(key, callback) {
	if (!translationDict.hasOwnProperty(key)) {
		var src = 'i18n/' + getPortalLanguageKey() + '/' + key;
		getJson(src, function (data) {
			translationDict[key] = data;
			callback();
		});
	}
	else {
		callback();
	}
}

function tryTranslate(key, elemType, tag) {
	var query = elemType + '[' + tag + ']';
	var elements = document.querySelectorAll(query);
	for (var i = 0; i < elements.length; i++) {
		var attributes = elements[i].attributes;
		for (var j = 0; j < attributes.length; j++) {
			if (attributes[j].nodeName === tag) {
				elements[i].innerHTML = findTranslateValue(translationDict[key], attributes[j].nodeValue, null);
				break;
			}
		}
		attributes = null;
	}
	elements = null;
}

function findTranslateValue(dict, fullKey, key) {
	if (key === null) {
		var splitStr = fullKey.split('.');
		key = splitStr[splitStr.length - 1];
	}
	
	var count =  fullKey.match(/\b\.\b/g); 
	
	if (count.length === 0) {
		return "";
	}
	else {
		var splitStr = fullKey.split('.');
		
		if (!dict.hasOwnProperty(splitStr[0])) {
			return "";
		}
		else {
			if (count.length === 1) {
				return dict[splitStr[0]][key];
			}
			else {
				var nextDict = dict[splitStr[0]];
				
				var nextFullKey = '';
				for (var i = 1; i < splitStr.length; i++) {
					nextFullKey += splitStr[i]
					if (i !== splitStr.length - 1) {
						nextFullKey += '.';
					}
				}
				
				return findTranslateValue(nextDict, nextFullKey, key);
			}
		}
	}
}

function getPortalLanguageKey() {
	var langObj = $.grep(currencyLanguageList, function(e){ return e.countryLanguageKey == countryLanguageKey; });
	if (langObj === undefined || langObj === null || langObj.length === 0) {
		return 'en';
	}
	else {
		return langObj[0].key;
	}
}

function getDocumentCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			var returnVal = c.substring(name.length, c.length);
			
			if (returnVal !== undefined && returnVal !== null && (returnVal === 'undefined' || returnVal === 'null')) {
				returnVal = null;
			}
			
			return returnVal;
		}
	}
	return null;
}

function getStoredQueryParam(paramName) {
	var storedQueryParams = getDocumentCookie('queryStr');
	var splitParams = storedQueryParams.split('&');
	
	for (var i = 0; i < splitParams.length; i++) {
		var paramKeyVal = splitParams[i].split('=');
		if (paramKeyVal[0] === paramName) {
			return paramKeyVal[1];
		}
	}
	
	return '';
}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function convertDateToString(datetime, showTime) {
	var year = datetime.getFullYear().toString();
	var month = datetime.getMonth() + 1;
	if (month < 10) {
		month = '0' + month;
	}
	var day = datetime.getDate();
	if (day < 10) {
		day = '0' + day;
	}

	var hour = datetime.getHours();
	if (hour < 10) {
		hour = '0' + hour;
	}
	var minute = datetime.getMinutes();
	if (minute < 10) {
		minute = '0' + minute;
	}

	var returnString = year + '-' + month + '-' + day;
	if (showTime) {
		returnString += 'T' + hour + ':' + minute;
	}
	return returnString;
}

// function to detect resize on window for particular element in html
function ResizeSensor(element, callback) {
    let zIndex = parseInt(getComputedStyle(element));
    if(isNaN(zIndex)) { zIndex = 0; };
    zIndex--;

    let expand = document.createElement('div');
    expand.style.position = "absolute";
    expand.style.left = "0px";
    expand.style.top = "0px";
    expand.style.right = "0px";
    expand.style.bottom = "0px";
    expand.style.overflow = "hidden";
    expand.style.zIndex = zIndex;
    expand.style.visibility = "hidden";

    let expandChild = document.createElement('div');
    expandChild.style.position = "absolute";
    expandChild.style.left = "0px";
    expandChild.style.top = "0px";
    expandChild.style.width = "10000000px";
    expandChild.style.height = "10000000px";
    expand.appendChild(expandChild);

    let shrink = document.createElement('div');
    shrink.style.position = "absolute";
    shrink.style.left = "0px";
    shrink.style.top = "0px";
    shrink.style.right = "0px";
    shrink.style.bottom = "0px";
    shrink.style.overflow = "hidden";
    shrink.style.zIndex = zIndex;
    shrink.style.visibility = "hidden";

    let shrinkChild = document.createElement('div');
    shrinkChild.style.position = "absolute";
    shrinkChild.style.left = "0px";
    shrinkChild.style.top = "0px";
    shrinkChild.style.width = "200%";
    shrinkChild.style.height = "200%";
    shrink.appendChild(shrinkChild);

    element.appendChild(expand);
    element.appendChild(shrink);

    function setScroll()
    {
        expand.scrollLeft = 10000000;
        expand.scrollTop = 10000000;

        shrink.scrollLeft = 10000000;
        shrink.scrollTop = 10000000;
    };
    setScroll();

    let size = element.getBoundingClientRect();

    let currentWidth = size.width;
    let currentHeight = size.height;

    let onScroll = function()
    {
        let size = element.getBoundingClientRect();

        let newWidth = size.width;
        let newHeight = size.height;

        if(newWidth != currentWidth || newHeight != currentHeight)
        {
            currentWidth = newWidth;
            currentHeight = newHeight;

            callback();
        }

        setScroll();
    };

    expand.addEventListener('scroll', onScroll);
    shrink.addEventListener('scroll', onScroll);
};

// !!! this function does not support reposition height yet
// button element must position absolute, parent container must position relative
// must provide oriBannerWidth and oriBannerHeight
// if scale >= 1 do nothing
// will return top and left property
function imgBgBtnRepositionEvent(btnProp) {
	var returnPos = {
		'left': btnProp.left,
		'top': btnProp.top // temporary no care y position first
	};

	// calculate scale first
	var scaleX = document.body.clientWidth / btnProp.oriBannerWidth;
	if (scaleX < 1) {
		// due to background image is always center aligned, hence we need to divide by 2, to reposition x-axis
		var adjX = (btnProp.oriBannerWidth - document.body.clientWidth) / 2.0;
		returnPos.left = (btnProp.left - adjX);
	}

	return returnPos;
}

// btnProp must have height and width
function generateTransparentBtn(btnProp, btnPosition) {
	var btn = document.createElement('a');
	btn.id = btnProp.id;
	btn.style.width = btnProp.width + 'px';
	btn.style.height = btnProp.height + 'px';
	btn.style.left = btnPosition.left + 'px';
	btn.style.top = btnPosition.top + 'px';
	btn.style.position = 'absolute';
	return btn;
}

// magical function to bind javascript dynamic element into angularjs scope
// must pass $scope and $compile from controller
function jsToAngularJsBinding(elem, scope, compile) {
	compile(elem)(scope);
}

function copyToClipboard(text) {
	//var elOriginalText = el.attr('data-original-title');
	var textArea = document.createElement("textarea");
        	
	// Place in top-left corner of screen regardless of scroll position.
	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;

	// Ensure it has a small width and height. Setting to 1px / 1em
	// doesn't work as this gives a negative w/h on some browsers.
	textArea.style.width = '2em';
	textArea.style.height = '2em';

	// We don't need padding, reducing the size if it does flash render.
	textArea.style.padding = 0;

	// Clean up any borders.
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';

	// Avoid flash of white box if rendered for any reason.
	textArea.style.background = 'transparent';
	textArea.contentEditable = true;
	textArea.readOnly = true;

	textArea.value = text;

	document.body.appendChild(textArea);

	var userAgent = navigator.userAgent;
	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		// create a selectable range
		var range = document.createRange();
		range.selectNodeContents(textArea);

		// select the range
		var selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);
		textArea.setSelectionRange(0, 999999);
	}
	else {
		textArea.select();
	}
	
	try {
		var successful = document.execCommand('copy');
		var msg = successful ? 'Copied!' : 'Oops, unable to copy!';
		alert(msg);
			 //el.attr('data-original-title', msg).tooltip('show');
	} catch (err) {
		console.log('Oops, unable to copy');
	}

	document.body.removeChild(textArea);
}

var cacheableValue = {};
var cachedCallbackJson = {};
function cacheable(api, http, type, deferred, onSuccess, onFailed) {
	var key = api;
	var cachedData = cacheableValue[key];
	var currentTime = new Date().getTime();
	if (!cachedData
		|| (cachedData && currentTime - cachedData.lastUpdated >= 1000)) {
		cachedData = null;

		// to handle simutaneous call when cached value is still empty
		if (!cachedData) {
			if (!cachedCallbackJson[key]) {
				cachedCallbackJson[key] = [];
				cachedCallbackJson[key].push({
					deferred: (deferred) ? deferred : null,
					onSuccess: (onSuccess) ? onSuccess : null,
					onFailed: (onFailed) ? onFailed : null
				});
			} else {
				cachedCallbackJson[key].push({
					deferred: (deferred) ? deferred : null,
					onSuccess: (onSuccess) ? onSuccess : null,
					onFailed: (onFailed) ? onFailed : null
				});
				return;
			}
		}

		// if (cachedData) {
		// 	cachedData.lastUpdated = new Date().getTime();

		// 	if (deferred) {
		// 		deferred.resolve(cachedData.value);
		// 	} else if (onSuccess) {
		// 		onSuccess(cachedData.value);
		// 	}
		// }
		
		if (type === 'get') {
			http.get(api).then(function(data) {
				if (cachedData) {
					cachedData.value = data.data;
				} else {
					var cachedData = {
						lastUpdated: new Date().getTime(),
						value: data.data
					};
					cacheableValue[key] = cachedData;
	
					var callbackList = cachedCallbackJson[key];
					if (callbackList) {
						for (var i = 0; i < callbackList.length; i++) {
							if (callbackList[i].deferred) {
								callbackList[i].deferred.resolve(cachedData.value);
							} else if (callbackList[i].onSuccess) {
								callbackList[i].onSuccess(cachedData.value);
							}
						}
						delete cachedCallbackJson[key];
					}
				}
			},
			function(failed) {
				
			});
		}
	} else {
		if (deferred) {
			deferred.resolve(cachedData.value);
		} else if (onSuccess) {
			onSuccess(cachedData.value);
		}
	}
}

function isDisplayBanner(currentTime, startDate, endDate, ignoreHours) {
	var nowDate = new Date();
	if (currentTime) {
		nowDate = currentTime;
	}

	if (ignoreHours === true) {
		nowDate = nowDate.setHours(0,0,0,0);
		if (
			(startDate === '' || new Date(startDate).setHours(0,0,0,0) <= nowDate)
			&& (endDate === '' || nowDate <= new Date(endDate).setHours(0,0,0,0))
		) {
			return true;
		}
	} else {
		if (
			(startDate === '' || new Date(startDate).getTime() <= nowDate.getTime())
			&& (endDate === '' || nowDate <= new Date(endDate).getTime())
		) {
			return true;
		}
	}
	return false;
}