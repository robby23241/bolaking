const _MS_PER_DAY = 1000 * 60 * 60 * 24;

//a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function isMobile() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
}

function getMobileOS() {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	if (/windows phone/i.test(userAgent)) {
	    return "Windows Phone";
	}
	
	if (/android/i.test(userAgent)) {
		globalAllBetMOS = "ANDROID";
	    return "Android";
	}
	
	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		globalAllBetMOS = "IPHONE";
		if(/iPad/.test(userAgent)){
			globalAllBetMOS = "IPAD";
		}
	    return "iOS";
	}
	
	return "unknown";
}

function downloadURI(uri, name) {
  var link = document.createElement("a");
  link.download = name;
  link.href = uri;
  link.target = "_blank";
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}

var getTemplateSrc = function (src, type, part, state) {
	if (newSettingTemplate === true) {
		if (state !== undefined && state !== null) {
			if (state === 'settingv2.history') {
				if (type === 'content' && part === 'settingv2.profile.tab_layer') {
					return 'app/entities/common/empty.html';
				} else if (type === 'content' && part === 'settingv2.profile.history') {
					return 'template/new_setting/history/history-tab.html';
				}
			} else if (state === 'settingv2.inbox') {
				if (part === 'settingv2.profile.tab_layer') {
					return 'template/new_setting/profile/profile_tab.html';
				}
			}
		} else {
			if (type === 'content' && part === 'settings') {
				return 'template/new_setting/setting.html';
			} else if (type === 'content' && part === 'settingv2.profile.tab_layer') {
				return 'template/new_setting/profile/profile_tab.html';
			} else if (type === 'content' && part === 'settingv2.profile') {
				return 'template/new_setting/profile/member-profile.html';
			} else if (type === 'content' && part === 'settingv2.profile.chgpwd') {
				return 'template/new_setting/profile/change-member-password.html';
			} else if (type === 'content' && part === 'settingv2.profile.chgproviderpwd') {
				return 'template/new_setting/profile/change-provider-password.html';
			} else if (type === 'content' && part === 'settingv2.profile.bank') {
				return 'template/new_setting/profile/bank.html';
			} else if (type === 'content' && part === 'settingv2.inbox') {
				return 'template/new_setting/inbox/inbox.html';
			} else if (type === 'content' && part === 'settingv2.withdraw') {
				return 'template/new_setting/withdraw/withdraw.html';
			} else if (type === 'content' && part === 'settingv2.withdraw.tab_layer') {
				return 'template/new_setting/withdraw/withdraw-tab.html';
			} else if (type === 'content' && part === 'settingv2.transfer') {
				return 'template/new_setting/transfer/transfer.html';
			} else if (type === 'content' && part === 'settingv2.profile.history') {
				return 'template/new_setting/history/history-tab.html';
			} else if (type === 'content' && part === 'settingv2.profile.history.withdraw_deposit') {
				return 'template/new_setting/history/withdraw-deposit-history.html';
			} else if (type === 'content' && part === 'settingv2.profile.history.transfer') {
				return 'template/new_setting/history/transfer-history.html';
			} else if (type === 'content' && part === 'settingv2.profile.history.promotion') {
				return 'template/new_setting/history/promotion-applied-history.html';
			} else if (type === 'content' && part === 'settingv2.profile.history.rebate') {
				return 'template/new_setting/history/rebate-history.html';
			} else if (type === 'content' && part === 'settingv2.profile.history.reward') {
				return 'template/new_setting/history/reward-history.html';
		    } else if (type === 'content' && part === 'settingv2.deposit.tab_layer') {
			    return 'template/new_setting/deposit/deposit-tab.html';
		    }  else if (type === 'content' && part === 'settingv2.deposit') {
			    return 'template/new_setting/deposit/bank-transfer.html';
		    }  else if (type === 'content' && part === 'settingv2.deposit.online_transfer') {
			   return 'template/new_setting/deposit/online-transfer.html';
		    }
		}
	}
	return src;
};
var templateSettings = null;
var templateText = null;

var getMobileTemplateSrc = function (src, type, part, state) {
	return src;
};

var getAffiliateTemplateSrc = function (src, type, part, state) {
	return src;
}

var getCustomController = function (oriCtrl, state, part) {
	return oriCtrl;
}

var getTemplateSetting = function (settingName, getValue) {
	if (template === 'default'
		|| (templateSettings !== null && templateSettings[settingName] !== undefined && templateSettings[settingName] !== null)) {
		if (getValue !== undefined) {
			if (templateSettings !== null && templateSettings[settingName] !== undefined) {
				return templateSettings[settingName];
			}
		} else {
			return true;
		}
	}
	return false;
};
var getTemplateText = function () {};
var registerFormChanged = function () {};
getNotificationTemplate = function(msg, extraClass) {
	if (merchantCode == '96c') {
		var classes = 'notification-inner';
		if(extraClass != null)
			classes += ' ' + extraClass;
		var param = {
			isCustomHtml: true,
			customHtmlCode: '<div class=\"notification-container\">'
				+ '<button type=\"button\" class=\"notification-close\" onclick=\"this.parentNode.hidden = true;\">x</button>'
				+ '<div class=\"'
				+ classes
				+ '\">' + msg + '</div>'
				+ '</div>',
			width: 350
		};
		return param;
	}
};
var populateMenuNgClass = function() {};
var templatePromotionTypeSelectEvent = function(filterType) {
	var promoAll = $("#promo-all")[0];
	var promoSlots = $("#promo-slots")[0];
	var promoCasino = $("#promo-casino")[0];
	var promoSportbook = $("#promo-sportbook")[0];
	var promoSpecial = $("#promo-special")[0];
	var promoMember = $("#promo-member")[0];
	var promoWinner = $("#promo-winner")[0];
	var promoVip = $('#promo-vip')[0];
	
	if (
		(filterType == "all" && promoAll.className == "filter_select")
		|| (filterType == "slot" && promoSlots.className == "filter_select")
		|| (filterType == "casino" && promoCasino.className == "filter_select")
		|| (filterType == "sportbook" && promoSportbook.className == "filter_select")
		|| (filterType == "special" && promoSpecial.className == "filter_select")
		|| (filterType == "member" && promoMember.className == "filter_select")
		|| (filterType == "winner" && promoMember.className == "filter_select")
		|| (promoVip !== undefined && promoVip !== null && filterType === 'vip' && promoVip.className === 'filter_select'))
	{
		return;
	} else {
		promoAll.className = "filter_btn";
		promoSlots.className = "filter_btn";
		promoCasino.className = "filter_btn";
		promoSportbook.className = "filter_btn";
		promoSpecial.className = "filter_btn";
		promoMember.className = "filter_btn";
		promoWinner.className = "filter_btn";
		if (promoVip !== undefined && promoVip !== null) {
			promoVip.className = 'filter_btn';
		}
		
		if (filterType == "all") {
			promoAll.className = "filter_select";
		}
		else if (filterType == "slot") {
			promoSlots.className = "filter_select";
		}
		else if (filterType == "casino") {
			promoCasino.className = "filter_select";
		}
		else if (filterType == "sportbook") {
			promoSportbook.className = "filter_select";
		}
		else if (filterType == "special") {
			promoSpecial.className = "filter_select";
		}
		else if (filterType == "member") {
			promoMember.className = "filter_select";
		}
		else if (filterType == "winner") {
			promoWinner.className = "filter_select";
		} else if (promoVip !== undefined && promoVip !== null && filterType === 'vip') {
			promoVip.className = 'filter_select';
		}
	}
	
	promoAll = null;
	promoSlots = null;
	promoCasino = null;
	promoSportbook = null;
	promoSpecial = null;
	promoMember = null;
	promoWinner = null;
	promoVip = null;
};

var loadBodySlotJackpot = false;
var displayTemplatePromotionDetail = function () {};
var hideTemplatePromotionDetail = function() {};
var templatePromotionDetailResize = function() {};
var changePasswordFormChanged = function() {};
var addBankFormFieldChanged = function() {};
var templateHistorySubtabChangeEvent = function() {};

var getMemberAllWalletMapping = function(listData, copyProviderWalletFunc) {
	var mainWalletBalance = 0;
	var providerWallets = {};
	var totalturnoveramount = 0;
	
	// map data
	for (var i = 0; i < listData.length; i++) {
		if (listData[i].provider == null) {
			mainWalletBalance = listData[i].balance;
		} else {
			var provider = listData[i].provider;
			providerWallets[provider] = copyProviderWalletFunc(listData[i]);
			totalturnoveramount = providerWallets[provider].totalturnoveramount;
		}
	}
	
	var mappedData = {
		'mainWalletBalance': mainWalletBalance,
		'providerWallets': providerWallets,
		'totalturnoveramount': totalturnoveramount
	};
	
	return mappedData;
};

var providerTransferListMapping = function(providerWallets) {
	if (newSettingTemplate === true) {
		var returnList = [];
		var rowlist = [];
		var i = 0;
		var rowIdentifier = '';
		
		for (var key in providerWallets) {
			if (i % 2 === 0) {
				if (returnList.length > 0) {
					rowlist = [];
				}
			}
			providerWallets[key].rowIdentifier = '' + returnList.length + (i % 2);
			rowlist.push(providerWallets[key]);
			
			if (i % 2 === 1) {
				returnList.push(rowlist);
			}
			
			i++;
		}
		
		// append extra dummy column if rowlist is not even
		if (rowlist.length % 2 === 1) {
			var dummyColumn = {
				isNull: true
			};
			rowlist.push(dummyColumn);
			returnList.push(rowlist);
		}
		
		return returnList;;
	} else {
		return providerWallets;
	}
}

var maskProviderTransferList = function(displayList) {
	if (newSettingTemplate === true) {
		for (var row = 0; row < displayList.length; row++) {
			for (var column = 0; column < displayList[row].length; column++) {
				if (displayList[row][column].isNull === false) {
					displayList[row][column].isdisplay = false;
				}
			}
		}
	}
	
	return displayList;
}

var mapProviderWallet = function(displayList, serviceData) {
	if (newSettingTemplate === true) {
		for (var row = 0; row < displayList.length; row++) {
			for (var column = 0; column < displayList[row].length; column++) {
				if (displayList[row][column].isNull === false) {
					var data = $.grep(serviceData, function(e) { return e.provider == displayList[row][column].provider; })[0];
					if (data !== undefined) {
						displayList[row][column].balance = data.balance;
						displayList[row][column].serverIsUnderMaintenance = data.serverIsUnderMaintenance;
						displayList[row][column].isdisplay = true;
					}
				}
			}
		}
		return displayList;
	} else {
		var providers = Object.keys(displayList);
		for (var i = 0; i < providers.length; i++) {
			var data = $.grep(serviceData, function(e) { return e.provider == providers[i]; })[0];
			if(data != null) {
				displayList[providers[i]].balance = data.balance;
				displayList[providers[i]].serverIsUnderMaintenance = data.serverIsUnderMaintenance;
				displayList[providers[i]].isdisplay = true;
			} else {
				// if no this provider wallet, give default values
				displayList[providers[i]].balance = 0;
				displayList[providers[i]].serverIsUnderMaintenance = false;
				displayList[providers[i]].isdisplay = true;
			}
		}
		return displayList;
	}
}

var getProviderWalletDictionaryFromDisplayList = function(displayList, provider) {
	if (newSettingTemplate === true) {
		var returnProviderWallet = null;
		var isFound = false;
		for (var row = 0; row < displayList.length && !isFound; row++) {
			for (var column = 0; column < displayList[row].length; column++) {
				if (displayList[row][column].provider === provider) {
					returnProviderWallet = displayList[row][column];
					isFound = true;
				}
			}
		}
		return returnProviderWallet;
	} else {
		return displayList[provider];
	}
}

var populateCommonSlotBackground = function(data, provider, providerClassName, providerDefaultBannerName) {
	var backgroundDiv = $("#slot-header-container");
	backgroundDiv[0].className = providerClassName;
	
	var providerInfoDict = data[provider];
	if (providerInfoDict !== undefined && providerInfoDict !== null) {
		var bgUrl = providerInfoDict.bannerSrc.replace('{0}', countryLanguageKey);
		var defaultBgUrl = '/public/' + merchantCode + '/content/images/background/' + providerDefaultBannerName;
		tryGetImage(backgroundDiv[0], 'divbg', bgUrl, defaultBgUrl);
	}
	
	backgroundDiv = null;
}

var templateSlotTypeSelectEvent = function(filterType, lastSelectedFilter, productList) {
	var returnDict = {
		selectedFilter: filterType
	};
	
	if (lastSelectedFilter === filterType) {
		return returnDict;
	}
	
	returnDict.selectedFilter = filterType;
	
	for (var i = 0; i < productList.length; i++) {
		var id = productList[i];
		var divIDName = "#" + id;
		var div = $(divIDName)[0];
		
		if (id === filterType) {
			div.className = "filter_select";
		}
		else {
			div.className = "filter_btn";
		}
	}
	
	return returnDict;
}

var templateSlotDefaultSelectTypeAll = function() {
	var all = $("#ALL")[0];
	all.className = "filter_select";

	var all = $("#HOT")[0];
	all.className = "filter_btn";
}

var templateMobileDownloadTypeSelectEvent = function (filterType, lastSelectedFilter, productList) {
	var returnDict = {
		selectedFilter: filterType
	};
	
	if (lastSelectedFilter === filterType) {
		return returnDict;
	}
	
	returnDict.selectedFilter = filterType;
	
	for (var i = 0; i < productList.length; i++) {
		var id = productList[i].name;
		var divIDName = "#" + id;
		var div = $(divIDName)[0];
		
		if (id == filterType) {
			div.className = "filter_select";
		}
		else {
			div.className = "filter_btn";
		}
	}
	
	return returnDict;
}

var customIconsForContact = function(){
	
};
var customIconsForMContact = function(){};
var navbarControllerInit = function() { };
var navbarControllerCustomGetLanguage = function() { };
var getTemplateCustomCountryIconClass = function() { };
var getMTemplateCustomCountryIconClass = function() { };
var getTemplateCustomLanguageIsSelectedClass = function() { };
var templateCustomAfterLoginEvent = function (currency) {
	var url = document.location.href;
    if ((url.length > 2 && url.slice(-2) !== '/m') || url.indexOf("/m/") === -1) {
    	if(affPortal) {
    		window.location.href = "aff/settings/deposit/cash";
    	} else {
    		$.getJSON( "public/html/settings/deposit.json", function( data ) {
    			var url = "/member-settings/deposit";
    			if(data["defaultDepositMethod"] != null && data["defaultDepositMethod"][currency] != null) {
    				if(data["defaultDepositMethod"][currency] !== 'onlinetransfer' ) {
    					url = "/member-settings/deposit/online-transfer";
    				}
    			}
    			
    			window.location.href = url;
			});
    	}
    }
};
var initSlider = function() { };
var initCommonSliderDiv = function() { };
var initCommonPromoSliderDiv = function() { };
var initCommonDivBg = function() { };
var initCommonDivSmallBg = function() { };
var templateSettingTabIndicator = function(tabname, statename) { 
	if (tabname === 'profileTab') {
		if (statename === 'settingv2.profile' || statename === 'settingv2.profile.chgpwd'
			|| statename === 'settingv2.profile.chgproviderpwd' || statename === 'settingv2.profile.bank'
			|| statename === 'settingv2.inbox') {
			return true;
		}
	} else if (tabname === 'depositTab') {
		if (statename === 'settingv2.deposit' || statename === 'settingv2.deposit.online-transfer') {
			return true;
		}
	} else if (tabname === 'withdrawTab') {
		if (statename === 'settingv2.withdraw') {
			return true;
		}
	} else if (tabname === 'transferTab') {
		if (statename === 'settingv2.transfer') {
			return true;
		}
	} else if (tabname === 'inboxTab') {
		if (statename === 'settingv2.inbox') {
			return true;
		}
	} else if (tabname === 'historyTab') {
		if (statename === 'settingv2.history') {
			return true;
		}
	}
	// subtab indicator
	else if (tabname === 'profileMainSubtab') {
		if (statename === 'settingv2.profile') {
			return true;
		}
	} else if (tabname === 'profileChgpwdSubtab') {
		if (statename === 'settingv2.profile.chgpwd') {
			return true;
		}
	} else if (tabname === 'profileBankSubtab') {
		if (statename === 'settingv2.profile.bank') {
			return true;
		}
	} else if (tabname === 'profileHistorySubtab') {
		if (statename === 'settingv2.history') {
			return true;
		}
	} else if (tabname === 'bankOnlineTransferSubtab') {
		if (statename === 'settingv2.deposit.online-transfer') {
			return true;
		}
	} else if (tabname === 'bankDepositSubtab') {
		if (statename === 'settingv2.deposit') {
			return true;
		}
	} else if (tabname === 'withdrawSubtab') {
		if (statename === 'settingv2.withdraw') {
			return true;
		}
	}
	
	return false;
};

var templateCasinoInit = function() {};

var templateGetSlotSlidingBanner = function() { return null; };
var templateSlotInit = function() { };
var templateSlotSlideProvider = function() { };
var templateSlotWinnerInit = function(winList) {
	$('.carousel').carousel();
	
    $("#winner").on('slide.bs.carousel', function (e) {
 	var carouselData = $(this).data('bs.carousel');
 	var currentIndex = carouselData.getItemIndex($(e.relatedTarget));
 	var currentItem = $(e.relatedTarget);
 	var currentPage = currentIndex + 1;
	
 	$('#winner-pager-current').html(currentPage);
	
 	$(function() {
     	$.fn.extend({
     	    animateCss: function (animationName, callback) {
     	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
     	        this.addClass('animated ' + animationName).one(animationEnd, function() {
     	            $(this).removeClass('animated ' + animationName);
     	            if (callback) {
     	              callback();
     	            }
     	        });
     	        return this;
     	    }
     	});
     	carouselData.$element.find('.item_container').animateCss('flipInY');
         });
     });
};
var templateSlotGameSmallBannerInit = function() { return null; };
var templateGetCasinoButton = function() { return null; };
var templateNavbarGetMenu = function() {
	if (merchantCode == '96c') {
		var menuList = [
    		{ "id": 1, "name": "Home", "content": "global.menu.home", "submenu": null, "sref": "home", "isImageMenu": true, "imageMenuSrc": "menu-home", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 2, "name": "Mobile", "content": "global.menu.mobile", "submenu": null, "sref": "mobile", "isImageMenu": true, "imageMenuSrc": "menu-mobile", "imageMenuHoverSrc": "menu-mobile-hover","isLive": false, "isNew": false, "provider": null }
    		,{ "id": 3, "name": "Sportsbook", "content": "global.menu.sportsbook", "submenu": "S", "sref": "all-sport", "isImageMenu": true, "imageMenuSrc": "nav-sport", "imageMenuHoverSrc": "nav-sport-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 4, "name": "Casino", "content": "global.menu.casino", "submenu": "C", "sref": "all-casino", "isImageMenu": true, "imageMenuSrc": "nav-casino", "imageMenuHoverSrc": "nav-casino-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 5, "name": "Slot", "content": "global.menu.slot", "submenu": "SL", "sref": "", "isImageMenu": true, "imageMenuSrc": "nav-slots", "imageMenuHoverSrc": "nav-slots-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 6, "name": "Poker", "content": "global.menu.poker", "submenu": 'P', "sref": "", "isImageMenu": true, "imageMenuSrc": "nav-poker", "imageMenuHoverSrc": "nav-poker-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 7, "name": "Fishing", "content": "global.menu.fishing", "submenu": "F", "sref": "", "isImageMenu": true, "imageMenuSrc": "nav-fish", "imageMenuHoverSrc": "nav-fish-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 8, "name": "918Kiss", "content": "global.menu.scr888", "submenu": null, "sref": "918kiss", "isImageMenu": false, "imageMenuSrc": "menu-scr", "imageMenuHoverSrc": "", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 9, "name": "Promotions", "content": "global.menu.promotions", "submenu": null, "sref": "promotion", "isImageMenu": true, "imageMenuSrc": "nav-promo", "imageMenuHoverSrc": "nav-promo-ov", "isLive": false, "isNew": false, "provider": null }
    		,{ "id": 10, "name": "LiveTV", "content": "global.menu.tv", "submenu": null, "sref": "livetv", "isImageMenu": true, "imageMenuSrc": "nav-tv", "imageMenuHoverSrc": "nav-tv-ov", "isLive": true, "isNew": false, "provider": null }
    		,{ "id": 11, "name": "LiveScore", "content": "global.menu.score", "submenu": null, "sref": "livescore", "isImageMenu": false, "imageMenuSrc": "", "imageMenuHoverSrc": "", "isLive": true, "isNew": false, "provider": null }
    		,{ "id": 13, "name": "VIP", "content": "global.menu.vip", "submenu": null, "sref": "viphome", "isImageMenu": true, "imageMenuSrc": "nav-vip", "imageMenuHoverSrc": "nav-vip-ov", "isLive": false, "isNew": false, "provider": null }
    	];
	} else {
		var menuList = [
			{ "id": 1, "name": "Home", "content": "global.menu.home", "submenu": null, "sref": "home", "isImageMenu": true, "imageMenuSrc": "menu-home", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 2, "name": "Mobile", "content": "global.menu.mobile", "submenu": null, "sref": "mobile", "isImageMenu": true, "imageMenuSrc": "menu-mobile", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 3, "name": "Sportsbook", "content": "global.menu.sportsbook", "submenu": "S", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 4, "name": "Casino", "content": "global.menu.casino", "submenu": "C", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 5, "name": "Slot", "content": "global.menu.slot", "submenu": "SL", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 6, "name": "Poker", "content": "global.menu.poker", "submenu": null, "sref": "poker", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": "IDN" }
			,{ "id": 7, "name": "Fishing", "content": "global.menu.fishing", "submenu": "F", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 8, "name": "918Kiss", "content": "global.menu.scr888", "submenu": null, "sref": "918kiss", "isImageMenu": true, "imageMenuSrc": "menu-scr", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 9, "name": "Promotions", "content": "global.menu.promotions", "submenu": null, "sref": "promotion", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 10, "name": "LiveTV", "content": "global.menu.tv", "submenu": null, "sref": "livetv", "isImageMenu": false, "imageMenuSrc": "", "isLive": true, "isNew": false, "provider": null }
			,{ "id": 11, "name": "LiveScore", "content": "global.menu.score", "submenu": null, "sref": "livescore", "isImageMenu": false, "imageMenuSrc": "", "isLive": true, "isNew": false, "provider": null }
			// SUP-122, hide affiliate from menu and only display in affiliate footer
			//,{ "id": 12, "name": "Affiliate", "content": "global.menu.affiliate", "submenu": null, "sref": "ahome", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 13, "name": "VIP", "content": "global.menu.vip", "submenu": null, "sref": "viphome", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			// special menu for testing please use negative id
			//,{ "id": -99, "name": "Affiliate", "content": "global.menu.affiliate", "submenu": null, "sref": "ahome", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null }
			,{ "id": 15, "name": "Lottery", "content": "global.menu.lottery", "submenu": "L", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null }
			,{ "id": 16, "name": "AffiliateNav", "content": "footer.affiliate", "submenu": null, "sref": "ahome", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null}
			,{ "id": 17, "name": "howToRegister", "content": "global.menu.howToRegister", "submenu": null, "sref": "howToRegister", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "isNew": false, "provider": null}
		];
	}

	return menuList;
};
var templateNavbarGetSubmenu = function(category) {
	if (category === 'SL') {
		var slotSubmenu = [
			{ "id": 1, "name": "SpadeGaming", "content": "global.submenu.slots.spadeGaming", "category": "SL", "provider": "SG", "sref": "slot({provider: 'spadeGaming'})", "httpSrc": "submenu-sg" }
			,{ "id": 2, "name": "PlayTech", "content": "global.submenu.slots.playtech", "category": "SL", "provider": "PT", "sref": "slot({provider: 'playTech'})", "httpSrc": "submenu-slot-pt" }
			,{ "id": 3, "name": "MicroGaming", "content": "global.submenu.slots.microgaming", "category": "SL", "provider": "MG", "sref": "slot({provider: 'microGaming'})", "httpSrc": "submenu-mg" }
			,{ "id": 4, "name": "TTG", "content": "global.submenu.slots.ttg", "category": "SL", "provider": "TTG", "sref": "slot({provider: 'toptrendgaming'})", "httpSrc": "submenu-ttg" }
			,{ "id": 5, "name": "Ultimate", "content": "global.submenu.slots.ultimate", "category": "SL", "provider": "UL", "sref": "ultimate", "httpSrc": "submenu-ultimate" }
			,{ "id": 6, "name": "GamePlay", "content": "global.submenu.slots.gp", "category": "SL", "provider": "GP", "sref": "slot({provider: 'gameplay'})", "httpSrc": "submenu-gp" }
			,{ "id": 7, "name": "AsiaGaming", "content": "global.submenu.slots.ag", "category": "SL", "provider": "AG", "sref": "slot({provider: 'asia-gaming'})", "httpSrc": "submenu-ag" }
			,{ "id": 8, "name": "918Kiss", "content": "global.submenu.slots.918kiss", "category": "SL", "provider": "SCR", "sref": "918kiss", "httpSrc": "submenu-scr" }
			,{ "id": 9, "name": "PlayNGo", "content": "global.submenu.slots.playNGo", "category": "SL", "provider": "PG", "sref": "slot({provider: 'playNGo'})", "httpSrc": "submenu-pg" }
			,{ "id": 10, "name": "MicroGamingPlus", "content": "global.submenu.slots.microgaming", "category": "SL", "provider": "MGP", "sref": "slot({provider: 'microGamingPlus'})", "httpSrc": "submenu-mg" }
			// special menu for testing please use negative id
			//,{ "id": -99, "name": "918Kiss", "content": "global.submenu.slots.scr888", "category": "SL", "provider": "SCR", "sref": "918kiss", "httpSrc": "submenu-scr" }
    	];
		
		return slotSubmenu;
	}
	else if (category === 'C') {
		var casinoSubmenu = [
			{ "id": 1, "name": "Allbet", "content": "global.submenu.casino.allbet", "category": "C", "provider": "AB", "sref": "allbet", "httpSrc": "submenu-ab" }
			,{ "id": 2, "name": "GoldDeluxe", "content": "global.submenu.casino.goldDeluxe", "category": "C", "provider": "GD", "sref": "gd", "httpSrc": "submenu-gd" }
			,{ "id": 3, "name": "Bamako", "content": "global.submenu.casino.evo", "category": "C", "provider": "BMK", "sref": "eg", "httpSrc": "submenu-eg" }
			,{ "id": 4, "name": "GamePlay", "content": "global.submenu.casino.gamePlay", "category": "C", "provider": "GP", "sref": "gp", "httpSrc": "submenu-casino-gp" }
			,{ "id": 5, "name": "DreamGaming", "content": "global.submenu.casino.dreamGame", "category": "C", "provider": "DG", "sref": "dg", "httpSrc": "submenu-dg" }
			,{ "id": 6, "name": "AsiaGaming", "content": "global.submenu.casino.asiaGaming", "category": "C", "provider": "AG", "sref": "ag", "httpSrc": "submenu-casino-ag" }
			,{ "id": 7, "name": "PlayTech", "content": "global.submenu.slots.playtech", "category": "C", "provider": "PT", "sref": "pt", "httpSrc": "submenu-casino-pt" }
			,{ "id": 8, "name": "SexyBaccarat", "content": "global.submenu.casino.sexyBaccarat", "category": "C", "provider": "UG", "sref": "ug", "httpSrc": "submenu-ug" }
			,{ "id": 9, "name": "Bamako2", "content": "global.submenu.casino.evo", "category": "C", "provider": "BMK2", "sref": "eg2", "httpSrc": "submenu-eg" }
			,{ "id": 10, "name": "MicroGamingPlus", "content": "global.submenu.slots.microgaming", "category": "C", "provider": "MGP", "sref": "mgp", "httpSrc": "submenu-casino-mgp" }
		];
		
		return casinoSubmenu;
	}
	else if (category === 'S') {
		var sportsSubmenu = [
			{ "id": 1, "name": "CMD368", "content": "global.submenu.vsports.cmd368", "category": "S", "provider": "CMD", "sref": "sport", "httpSrc": "submenu-cmd" }
			,{ "id": 2, "name": "BetRadar", "content": "global.submenu.vsports.vsports", "category": "S", "provider": "BR", "sref": "vsports", "httpSrc": "submenu-br" }
			,{ "id": 3, "name": "IBC", "content": "global.submenu.sport.ibc", "category": "S", "provider": "IBC", "sref": "ibc-sport", "httpSrc": "submenu-ibc" }
			,{ "id": 4, "name": "SBO", "content": "global.submenu.sport.sbo", "category": "S", "provider": "SBO", "sref": "sbo-sport", "httpSrc": "submenu-sbo" }
			,{ "id": 5, "name": "SBOBET", "content": "global.submenu.sport.sbo", "category": "S", "provider": "SBO", "sref": "sbo-sport-bet", "httpSrc": "submenu-sbo" }
    	];
		
		return sportsSubmenu;
	}
	else if (category === 'F' && merchantCode != '96c') {
		var fishingSubmenu = [
			{ "id": 1, "name": "GG Fishing", "content": "global.submenu.fishing.gg", "category": "F", "provider": "GG", "sref": "fishing", "httpSrc": "submenu-gg" }
			,{ "id": 2, "name": "PT Fishing", "content": "global.submenu.fishing.pt", "category": "F", "provider": "PT", "sref": "cashfish", "httpSrc": "submenu-pt" }
			,{ "id": 3, "name": "SG Fishing", "content": "global.submenu.fishing.sg", "category": "F", "provider": "SG", "sref": "sg-fishing", "httpSrc": "submenu-sg" }
    	];
		
		return fishingSubmenu;
	}
	else if (category === 'F' && merchantCode == '96c') {
		var fishingSubmenu = [
			{ "id": 1, "name": "GG Fishing", "content": "global.submenu.fishing.gg", "category": "F", "provider": "GG", "sref": "fishing", "httpSrc": "submenu-gg" }
			,{ "id": 2, "name": "PT Fishing", "content": "global.submenu.fishing.pt", "category": "F", "provider": "PT", "sref": "cashfish", "httpSrc": "submenu-pt" }
			,{ "id": 3, "name": "SG Fishing", "content": "global.submenu.fishing.sg", "category": "F", "provider": "SG", "sref": "sg-fishing", "httpSrc": "submenu-sg-fish" }
		];
		
		return fishingSubmenu;
	}
	else if (category === 'L') {
		var lotterySubmenu = [
			{ "id": 1, "name": "QQThai Lottery", "content": "global.submenu.lottery.qqthai", "category": "L", "provider": "QQT", "sref": "lottery-qqt", "httpSrc": "submenu-qqt" }
		];
		
		return lotterySubmenu;
	}
	else if (category === 'P') {
		var pokerSubmenu = [
			{ "id": 1, "name": "Poker", "content": "global.submenu.poker.idn", "category": "P", "provider": "IDN", "sref": "poker", "httpSrc": "submenu-poker" }
		];

		return pokerSubmenu;
	}
};
var templateNavbarGetAffiliateMenu = function() {
	var affiliateMenu = [
		{ "id": 1, "name": "Home", "content": "global.menu.home", "submenu": null, "sref": "ahome", "isImageMenu": true, "imageMenuSrc": "menu-home", "isLive": false, "provider": null },
		{ "id": 2, "name": "PortalHome", "content": "global.menu.portal-home", "submenu": "PortalHome", "sref": "", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 3, "name": "Report", "content": "global.menu.report", "submenu": null, "sref": "affSettings({view: 'reports'})", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 4, "name": "Products", "content": "global.menu.products", "submenu": null, "sref": "a-products", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 5, "name": "Commission Plans", "content": "global.menu.commission-plans", "submenu": null, "sref": "a-commission-plans", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 6, "name": "FAQ", "content": "global.menu.faq", "submenu": null, "sref": "a-faq", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 7, "name": "Contact", "content": "global.menu.contact-us", "submenu": null, "sref": "a-contact-us", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 8, "name": "Overview", "content": "global.menu.overview", "submenu": null, "sref": "a-overview", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null },
		{ "id": 9, "name": "Gallery", "content": "global.menu.gallery", "submenu": null, "sref": "a-gallery", "isImageMenu": false, "imageMenuSrc": "", "isLive": false, "provider": null }
	];
	return affiliateMenu;
};
var templateGetCasinoSlidingBanner = function() { return null; };
var templateGetCasinoSmallSlidingBanner = function() { return null; };
var templateGetFishingSlidingBanner = function() { return null; };
var templateFishingInitProviderBanner = function() { return null; }
var templateGetPokerSlidingBanner = function() { return null; };

var getGdResult = function() {};
var initHotGames = function() {};
var initChangeTab = function() {};
var initChangeCasinoTab = function() {};
var initChangeSlotTab = function() {};
var initLiveTv = function() {};
var getUpComingEventSlides = function() {};
var templateMToggleSidebar = function() {
	$('.m-overlay').toggleClass('active');
	$('.sidebar-nav').toggleClass('active');
	
	if ($('.m-overlay').hasClass('active')) {
		$(".m-main-content").toggleClass('active');
		$('.m-main-content').bind('touchmove', function(e) { e.preventDefault() } );
		$('.m-mobile')[0].classList.add("m-noscroll-x");
	}
	else {
		$(".m-main-content").toggleClass('active');
		$('.m-main-content').unbind('touchmove');
		$('.m-mobile')[0].classList.remove("m-noscroll-x");
	}
};

var templateMHideSidebar = function() {
	$('.m-overlay').toggleClass('active');
	$('.sidebar-nav').toggleClass('active');
	
	if ($('.m-main-content').hasClass('sub-active')) {
		$(".m-main-content").toggleClass('sub-active');
	}
	
	$(".m-main-content").toggleClass('active');
	
	if ($('.m-overlay').hasClass('active')) {
		$('.m-main-content').bind('touchmove', function(e) { e.preventDefault() } );
	}
	else {
		$('.m-main-content').unbind('touchmove');
	}
	
	if ($('.subsidebar-menu').hasClass('active')) {
		$('.subsidebar-menu').toggleClass('active');
	}
	
	if ($('.sidebar-menu-container').hasClass('subactive')) {
		var sidemenuDivs = document.getElementsByClassName("sidebar-menu-container");
		
		for (var i = 0; i < sidemenuDivs.length; i++) {
			if (sidemenuDivs[i].classList.contains("subactive")) {
				sidemenuDivs[i].classList.remove("subactive");
				var menuSrc = sidemenuDivs[i].children[0].src.substring(0, sidemenuDivs[i].children[0].src.length - 7); ;
				sidemenuDivs[i].children[0].src = menuSrc + ".png";
				break;
			}
		}
	}
	
	$('.m-mobile')[0].classList.remove("m-noscroll-x");
};

var initMSlider = function() { };
var mapCategoryClass = function() { };
var mapBankIconClass = function() { };
var mapSubcategoryClass = function() { };
var mapSpecialIconClass = function() { };
var showProceedLoginDialog = function(modal) {
	var modalInstance = modal.open({
		animation: true,
		backdropClass :"proceed-login-backdrop",
		ariaLabelledBy: 'Please Login',
		ariaDescribedBy: 'Please Login',
		templateUrl: getMobileTemplateSrc('app/mobile/account/login/proceed-login.html', 'login', 'login', 'proceedlogin'),
		windowTemplateUrl: getMobileTemplateSrc('app/mobile/account/login/customizedModal.html', 'login', 'custom-modal', 'proceedlogin'),
		controller: 'ProceedLoginController',
		controllerAs: 'vm',
		size: 'sm',
		resolve: {
			
		}
	});
};
var mSubmenuDisplayMapping = function(submenuList) {
	return submenuList;
};
var mHideSubmenu = function(obj) {
	var divElement = document.getElementById(obj.submenu);
	divElement.classList.remove("m-provider-category-selected");
	
	var imgElement = divElement.children[0].children[0];
	imgElement.src = getDelayImgSrc(obj.imageMenuSrc);
	
	var spanElement = divElement.children[1];
	spanElement.classList.remove("m-category-title-selected");
	
	imgElement = null;
	spanElement = null;
	divElement = null;
};
var mCategorySelectedEvent = function(obj) {
	obj.isChecked = true;
		
	var divElement = document.getElementById(obj.submenu);
	divElement.classList.add("m-provider-category-selected");
	
	var imgElement = divElement.children[0].children[0];
	var src = obj.imageMenuSrc + "-hover";
	imgElement.src = getDelayImgSrc(src);
	
	var spanElement = divElement.children[1];
	spanElement.classList.add("m-category-title-selected");
	
	imgElement = null;
	spanElement = null;
	divElement = null;
};

var mGetSidebarMenuMapping = function(isLogin) {
	var items = [
    	{ 
    		id: "sm-1",
    		index: 1, heading: "global.sidebar.home.heading", 
    		image: "m-sidemenu-home", 
    		imagehover: "m-sidemenu-home-hover",
    		state: "mhome",
    		subItems: []
    	},
    	{
    		id: "sm-2",
    		index: 2, heading: "global.sidebar.funds.heading", 
    		image: "m-sidemenu-funds", 
    		imagehover: "m-sidemenu-funds-hover",
    		state: "mfunds",
    		subItems: []
    		/*
    		subItems: [
    			{index: 1, sref: "mdeposit", heading: "global.sidebar.funds.deposit"},
    			{index: 2, sref: "mtransfer", heading: "global.sidebar.funds.transfer"},
    			{index: 3, sref: "mwithdraw", heading: "global.sidebar.funds.withdraw"},
    			{index: 4, sref: "mhistory", heading: "global.sidebar.funds.history"}
    		]
    		*/
    	},
    	{
    		id: "sm-3",
    		index: 3, heading: "global.sidebar.account.heading", 
    		image: "m-sidemenu-account", 
    		imagehover: "m-sidemenu-account-hover",
    		state: "mmyaccount",
    		subItems: []
    	},
    	{
    		id: "sm-4",
    		index: 4, heading: "global.sidebar.promotion.heading", 
    		image: "m-sidemenu-promo", 
    		imagehover: "m-sidemenu-promo-hover",
    		state: "mpromotion",
    		subItems: []
    	},
    	{
    		id: "sm-5",
    		index: 5, heading: "global.sidebar.contact-us.heading", 
    		image: "m-sidemenu-contact", 
    		imagehover: "m-sidemenu-contact-hover",
    		state: "mcontact",
    		subItems: []
    	},
    	{
    		id: "sm-6",
    		index: 6, heading: "global.sidebar.language.heading", 
    		image: "m-sidemenu-language", 
    		imagehover: "m-sidemenu-language-hover",
    		state: "mlanguage",
    		subItems: []
    	},
    	{
    		id: "sm-7",
    		index: 7, heading: "global.sidebar.desktop.heading", 
    		image: "m-sidemenu-desktop", 
    		imagehover: "m-sidemenu-desktop-hover",
    		subItems: []
    	},
    	{
    		id: "sm-8",
    		index: 8, heading: "global.sidebar.download.heading", 
    		image: "m-sidemenu-download", 
    		imagehover: "m-sidemenu-download-hover",
    		state: "mdownload",
    		subItems: []
    	},
    	{
    		id: "sm-9",
    		index: 9, heading: "global.sidebar.logout.heading", 
    		image: "m-sidemenu-logout", 
    		imagehover: "m-sidemenu-logout-hover",
    		subItems: []
    	}
    ];
    
    if (isLogin == false) {
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 2; })[0]), 1 );
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 3; })[0]), 1 );
    	items.splice( items.indexOf($.grep(items, function(e){ return e.index == 9; })[0]), 1 );
    }
    return items;
};
var mapSidemenuClass = function() { };
var mBottomNavbarHideShowEvent = function(isLogin) { };
var mBottomNavbarHighlightEvent = function(state) {
	// default unhighlight all icon first
	var icons = document.getElementsByClassName("nav-btn");
	
	for (var i = 0; i < icons.length; i++) {
		if (icons[i].classList.contains('active')) {
			icons[i].classList.remove('active');
		}
	}
	
	if (state == "mhome") {
		icons[0].classList.add('active');
	}
	else if (state == "mfunds") {
		icons[1].classList.add('active');
	}
	else if (state == "mpromotion") {
		icons[2].classList.add('active');
	}
	
	icons = null;
};

var mBankTransferRoundDateTime = function(datetime) {
	return datetime;
};

var mBankTransferSubmitDepositGetDateTimeValue = function() {
	var dateTimeValue = $('#depositDateTime')[0].value;
	var splitStr = dateTimeValue.split(' ');
	return splitStr[0] + "T" + splitStr[1] + ":00";
};

var mHistoryFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var transType = filterList[i].transType;
		var divIDName = "#" + transType;
		var div = $(divIDName)[0];
		
		if (transType == selectedFilter) {
			div.className = "m-filter_select";
		} else {
			div.className = "m-filter_btn";
		}
	}
};

var mOpenHistoryFilterDialog = function() { };
var mCloseHistoryFilterDialog = function() { };

var initIENotSupport = function () {
	
	if (!String.prototype.startsWith) {
		Object.defineProperty(String.prototype, 'startsWith', {
			value: function(search, pos) {
				pos = !pos || pos < 0 ? 0 : +pos;
				return this.substring(pos, pos + search.length) === search;
			}
		});
	}

	if (!String.prototype.includes) {
		Object.defineProperty(String.prototype, 'includes', {
		  value: function(search, start) {
			if (typeof start !== 'number') {
			  start = 0
			}
			
			if (start + search.length > this.length) {
			  return false
			} else {
			  return this.indexOf(search, start) !== -1
			}
		  }
		})
	  }
}

var changeToProxyUrl = function(url, replaceUrl, proxyUrl) { };

// mandatory flag
// specialCheck : flag to indicate whether requires special submenu click function
var navbarSpecialSubmenuChecking = function(submenuObj) {
	return {  
		"specialCheck": false
	};
};

var mCommonCasinoGetBackground = function(provider) {
	var headerDiv = $("#m-casino-header");
	headerDiv[0].className = mGetCommonCasinoGetHeaderBackground(provider);
	
	var contentDiv = $("#m-casino-content");
	contentDiv[0].className = mGetCommonCasinoContentBackground(provider);
	
	headerDiv = null;
	contentDiv = null;
};

var mGetCommonCasinoGetHeaderBackground = function(provider) {
	if (provider == "BMK" || provider == "BMK2") {
		return "m-casino-bmk";
	} else if (provider == "GD") {
		return "m-casino-gd";
	} else if (provider == "DG") {
		return "m-casino-dg";
	} else if (provider == "AG") {
		return "m-casino-ag";
	}
};

var mGetCommonCasinoContentBackground = function(provider) {
	if (provider == "BMK" || provider == "BMK2") {
		return "m-casino-bmk-play";
	} else if (provider == "GD") {
		return "m-casino-gd-play";
	} else if (provider == "DG") {
		return "m-casino-dg-play";
	} else if (provider == "AG") {
		return "m-casino-ag-play";
	}
}

var mGetIdnContentBackground = function(os) {
	var contentDiv = $("#m-casino-play");
	contentDiv[0].className = getContentBackground(os);
	contentDiv = null;
};

function getContentBackground(os) {
	if (os == "Android") {
		return "m-casino-idn-play-android";
	} else if (os == "iOS") {
		return "m-casino-idn-play-ios";
	}
}

var mSlotGameCategorySelectedEvent = function(id, categoryList) {
	if (categoryList) {
		for (var i = 0; i < categoryList.length; i++) {
			var div = $('#' + categoryList[i])[0];
			if (id === categoryList[i]) {
				div.className = 'm-filter_select';
			} else {
				div.className = 'm-filter_btn';
			}
		}
	} else {
		$("#" + id)[0].className = "m-filter_select";
	}
};

var mSlotGameKeywordFilterTextboxClickEvent = function(showSearch) {
	if (showSearch) {
		document.getElementById("m-slot-gallery-container").style.marginTop = "110px";
	} else {
		document.getElementById("m-slot-gallery-container").style.marginTop = "40px";
	}
}

var mSlotGetPaginationItemsPerPage = function() {
	return 12;
}

var mSlotGameItemClickedEvent = function(gameObj, gameList) {
	for (var i = 0; i < gameList.length; i++) {
		gameList[i].isSelected = false;
	}
	var foundGameObj = $.grep(gameList, function(e){ return e.id === gameObj.id; })[0];
	foundGameObj.isSelected = true;
}

var mPromotionFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var transType = filterList[i].id;
		var divIDName = "#" + transType;
		var div = $(divIDName)[0];
		
		if (transType === selectedFilter) {
			div.className = "m-filter_select";
		} else {
			div.className = "m-filter_btn";
		}
	}
}

var mSlotGameItemHideGamePopup = function() { }

var mDownloadFiltering = function(filterList, selectedFilter) {
	for (var i = 0; i < filterList.length; i++) {
		var id = filterList[i].name;
		var divIDName = '#' + id;
		var div = $(divIDName)[0];

		if (id === selectedFilter) {
			div.className = 'm-filter_select';
		} else {
			div.className = 'm-filter_btn';
		}
	}
}

var mDownloadGetDownloadList = function(jsonObj) {
	return jsonObj['mobile'];
}

var mPopupTransferProviderIcon = function(provider) {
	var providerImg = null;
	switch (provider) {
		case "CMD":
			providerImg = "m-submenu-soccer";
			break;
		case "AB":
			providerImg = "m-submenu-allbet"
			break;
		case "GD":
			providerImg = "m-submenu-gd";
			break;
		case "BMK":
		case "BMK2":
			providerImg = "m-submenu-evo";
			break;
		case "GP":
			providerImg = "m-submenu-gp";
			break;
		case "DG":
			providerImg = "m-submenu-dg";
			break;
		case "AG":
			providerImg = "m-submenu-ag";
			break;
		case "PT":
			providerImg = "m-submenu-pt";
			break;
		case "GG":
			providerImg = "m-submenu-gg";
			break;
		case "SG":
			providerImg = "m-submenu-spade";
			break;
		case "MG":
		case "MGP":
			providerImg = "m-submenu-mg";
			break;
		case "TTG":
			providerImg = "m-submenu-ttg";
			break;
		case "SCR":
		case "SCR2":
			providerImg = "m-submenu-scr";
			break;
		case "UL":
			providerImg = "m-submenu-ultimate";
			break;
		case "IBC":
			providerImg = "m-submenu-ibc";
			break;
		case "IDN":
			providerImg = "m-menu-poker";
			break;
		case "SBO":
		case 'SBO2':
			providerImg = "m-submenu-sbo";
			break;
		case "UG":
			providerImg = "m-submenu-ug";
			break;
		case "QQT":
			providerImg = "m-submenu-qqthai";
			break;
		case "PG":
			providerImg = "m-submenu-pg";
			break;
	}
	if (providerImg) {
		return getDelayImgSrc(providerImg);
	}
}

var templateGetMenuClassEvent = function (menu, state) { };
var templateMenuPopulatedCompleted = function() { };
var affBannerOnloadEvent = function (bannerList, bannerObj) { };
var templateBodyOnloadEvent = function() { };
var bk8NewSettingPageInit = function() {};
var initNewBk8Sidebar = function() {};
var initDateTimePicker = function() {};
var homepageSportNewsOpenPopupFromController = null;
var providerTransferListMappingForNewBk8Setting = function() {};
var affContactUsMappingEvent = function (contactusList) {
	for (var i = 0; i < contactusList.length; i++) {
		contactusList[i].nameTrans = affGetNameTrans(contactusList[i].name);
		contactusList[i].typeInfo = affGetTypeInfo(contactusList[i].name);
	}
	return contactusList;
}
var affContactUsPopulateContactEvent = function(contactObj) { }

function affGetNameTrans(name) {
	if (name === 'Email') {
		return "contactus.affiliate.email";
	} else if (name === 'Skype') {
		return "contactus.affiliate.skype";
	} else if (name === 'WeChat') {
		return "contactus.affiliate.wechat";
	} else if (name === 'Phone') {
		return "contactus.affiliate.phone";
	} else if (name === 'Whatsapp') {
		return "contactus.affiliate.whatsapp";
	} else if (name === 'Line') {
		return "contactus.affiliate.line";
	} else {
		return "";
	}
}

function affGetTypeInfo(name) {
	if (name === 'Email') {
		return "contactus.affiliate.general";
	} else if (name === 'Skype' || name === "WeChat") {
		return "contactus.affiliate.clickhere";
	} else {
		return "contactus.affiliate.moreinfo";
	}
}

function affGetCommissionPlanFileUrl() {
	return '/public/html/affiliate/commission_plan_' + countryLanguageKey.toLowerCase() + '.txt?' + new Date().getTime();
}

function affGetFaqFileUrl() {
	return '/public/html/affiliate/faq_' + countryLanguageKey.toLowerCase() + '.txt?' + new Date().getTime();
}

var mapBankIconsForNewBk8Setting = function() {};