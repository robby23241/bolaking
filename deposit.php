<?php include("navbar-intern.php") ?>

    <div class="tab-content" id="tabDeposit" aria-hidden="false">
        <div ui-view="setting_content">
            <div class="tabContentHeader">
                <div class="font-14" style="height: 54px">
                    <div class="userActivityTitle" style="padding-left: 45px" data-translate="settings.tabs.heading.depositCap">DEPOSIT
                    </div>
                </div>
            </div>
            <div class="line-divider section">
                <hr>
            </div>
            <div class="alert alert-danger ng-hide" aria-hidden="true">
                Unable to load bank information, please contact Customer Service. Errorcode: rc1b004s
            </div>
            <div class="alert alert-danger ng-hide" aria-hidden="true">
                Deposit failed, please contact Customer Service. Errorcode: rc1b006s
            </div>
            <form name="form" role="form" class="ng-pristine ng-valid-min ng-valid-max ng-invalid ng-invalid-required">
                <div class="radio-tile-group row" style="margin-top: 22px;display: flex">
                    <div class="left-title font-14 text-blue">
                        <span>Metode Pembayaran</span>*
                    </div>
                    <label class="layersMenu">
                        <input type="radio" class="form-check-input ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" name="depositDepositOption" id="depositDepositOption" required="" checked="checked" aria-invalid="false">
                        <img src="public/new_bk8/content/images/newSetting/bank_transfer_ori.png">
                        <div class="tContent_textContent text-center">Bank Transfer</div>
                    </label>

                    <label class="layersMenu">
                        <input type="radio" class="form-check-input ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" name="depositDepositOption" id="depositDepositOption" required="" checked="checked" aria-invalid="false">
                        <img src="public/new_bk8/content/images/newSetting/bank_transfer_ori.png">
                        <div class="tContent_textContent text-center">Kupon</div>
                    </label>
                </div>

                <div class="radio-tile-group row" style="margin-top: 10px">
                    <div class="left-title font-14 text-blue"></div>
                    <!---->
                    <!---->
                    <label class="layersMenu LayerDepositOption">
                        <input type="radio" class="form-check-input ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" name="depositDepositAmount" id="depositDepositAmount" required="" value="[object Object]" checked="checked" aria-invalid="false">
                        <img src="public/new_bk8/content/images/newSetting/BCA_deposit.png">

                        <div class="tContent_textContent text-center">BCA</div>
                    </label>
                    <!---->
                    <!---->
                    <!---->
                    <label class="layersMenu LayerDepositOption">
                        <input type="radio" class="form-check-input ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" name="depositDepositAmount" id="depositDepositAmount" required="" value="[object Object]" aria-invalid="false">
                        <img src="public/new_bk8/content/images/newSetting/BRI_depositGray.png">
                        <div class="tContent_textContent text-center">BRI</div>
                    </label>
                    <!---->
                    <!---->
                    <!---->
                    <label class="layersMenu LayerDepositOption">
                        <input type="radio" class="form-check-input ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" name="depositDepositAmount" id="depositDepositAmount" required="" value="[object Object]" aria-invalid="false">
                        <img src="public/new_bk8/content/images/newSetting/BNI_depositGray.png">
                        <div class="tContent_textContent text-center">BNI</div>
                    </label>
                    <!---->
                    <!---->
                </div>
                <div class="radio-tile-group row" style="margin-top: 22px;display: flex">
                    <div class="left-title font-14 text-blue">
                        <span>Negara</span>*
                    </div>
                    <div class="w-290 h-30 font-14 mt-2" style="width: fit-content; width: -moz-fit-content">
                        <div class="user-bank-detail" style="margin-left: 15px">
                            <span>Indonesia</span>
                        </div>
                    </div>
                </div>
                <div class="radio-tile-group row" aria-hidden="false">
                    <div class="left-title font-14 text-blue"></div>
                    <div class="user-bank-account w-150 h-30 font-14" style="width: fit-content; width: -moz-fit-content">
                        <div class="user-bank-detail" style="margin-left: 15px">
                            <span id="bankNum-425">5310735983</span>
                        </div>
                        <img class="bankAccount-icon cpybtn" data-clipboard-target="#bankNum-425" src="public/new_bk8/content/images/newSetting/tContent_depositContent_bankAccount.png">
                    </div>
                </div>

                <div class="radio-tile-group row" style="margin-top: 20px">
                    <div class="left-title font-14 text-blue">
                        <span>Deposit Amount</span>*
                    </div>
                    <div class="input-container" style="display: flex">
                        <div class="inputDeposit w-300 h-40 align-items-center">
                            <input class="ml-15 font-14 ng-pristine ng-untouched ng-empty ng-valid-min ng-valid-max ng-invalid ng-invalid-required" id="amount" name="amount" type="number" style="width: 100%; padding-right: 10px" placeholder="Min/Max Limit: 25.00/ 100,000.00" required="">
                        </div>
                        <label style="display: flex; align-items: center; margin-left: 10px" aria-hidden="false" class="">1 = 1000 Rp
                        </label>
                    </div>
                    <div class="formMessage ng-hide" aria-hidden="true">
                        <img alt="tContent_depositContent_errorIcon.png" class="ml-10" src="public/new_bk8/content/images/newSetting/tContent_depositContent_errorIcon.png">
                        <span class="message-warning ml-10 font-14" style="color: red">Min/Max Limit: 25 / 100000
												</span>
                    </div>
                </div>
                <div class="radio-tile-group row" style="margin-top: 13px">
                    <div class="left-title font-14 text-blue">

                    </div>
                    <!---->
                    <div class="input-container">
                        <input class="radio-button" id="tranferRow4" name="row4" type="radio">
                        <div class="radio-tile4">
                            <div class="icon">100</div>
                        </div>
                    </div>
                    <div class="input-container">
                        <input class="radio-button" id="tranferRow4" name="row4" type="radio">
                        <div class="radio-tile4">
                            <div class="icon">200</div>
                        </div>
                    </div>
                    <div class="input-container">
                        <input class="radio-button" id="tranferRow4" name="row4" type="radio">
                        <div class="radio-tile4">
                            <div class="icon">500</div>
                        </div>
                    </div>
                    <div class="input-container">
                        <input class="radio-button" id="tranferRow4" name="row4" type="radio">
                        <div class="radio-tile4">
                            <div class="icon">1000</div>
                        </div>
                    </div>
                    <div class="input-container">
                        <input class="radio-button" id="tranferRow4" name="row4" type="radio">
                        <div class="radio-tile4">
                            <div class="icon">2000</div>
                        </div>
                    </div>

                    <div class="radio-tile-group row" style="margin-top: 26px">
                        <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                            <span>Select Prmotion</span>&nbsp;*
                        </div>
                        <div class="custom-select w-300 font-14" style="margin-right: 25px">
                            <select required="" class="ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" aria-invalid="false">
                                <option value="S" data-translate="settings.dropdown.pleaseselect">--- Please Select ---</option>
                                <option value="O" data-translate="settings.dropdown.onlineBanking">12222455</option>
                                <option value="D" data-translate="settings.dropdown.cashDepositMachine">7744848</option>
                                <option value="A" data-translate="settings.dropdown.atmTransfer">4454877</option>
                            </select>
                            <div class="select-selected" role="button" tabindex="0">
                                <div aria-hidden="false" class="">--- Please Select ---</div>
                                <div aria-hidden="true" class="ng-hide">--- Please Select ---</div>
                            </div>
                            <div class="select-items select-hide">
                                <div role="button" tabindex="0">--- Please Select ---</div>
                                <div role="button" tabindex="0">Online Banking</div>
                                <div role="button" tabindex="0">Cash Deposit Machine</div>
                                <div role="button" tabindex="0">ATM</div>
                            </div>
                        </div>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 26px">
                        <div class="left-title font-14 text-blue">Reference ID</div>
                        <div class="input-container">
                            <div class="inputDeposit w-300 h-40 align-items-center">
                                <input class="ml-15 font-14 ng-pristine ng-untouched ng-valid ng-empty" id="referenceID" name="referenceID" placeholder="Reference ID" type="Reference" aria-invalid="false">
                            </div>
                        </div>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 26px">
                        <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                            <span>Name Of Bank</span>&nbsp;*
                        </div>
                        <div class="custom-select w-300 font-14" style="margin-right: 25px">
                            <select required="" class="ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-required" aria-invalid="false">
                                <option value="S" data-translate="settings.dropdown.pleaseselect">--- Please Select ---</option>
                                <option value="O" data-translate="settings.dropdown.onlineBanking">BCA</option>
                                <option value="D" data-translate="settings.dropdown.cashDepositMachine">BNI</option>
                                <option value="A" data-translate="settings.dropdown.atmTransfer">MANDIRI</option>
                            </select>
                            <div class="select-selected" role="button" tabindex="0">
                                <div aria-hidden="false" class="">--- Please Select ---</div>
                                <div aria-hidden="true" class="ng-hide">--- Please Select ---</div>
                            </div>
                            <div class="select-items select-hide">
                                <div role="button" tabindex="0">--- Please Select ---</div>
                                <div role="button" tabindex="0">Online Banking</div>
                                <div role="button" tabindex="0">Cash Deposit Machine</div>
                                <div role="button" tabindex="0">ATM</div>
                            </div>
                        </div>
                    </div>

                    <div class="radio-tile-group row" style="margin-top: 18px" aria-hidden="false">
                        <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                            <span>Deposit Date/Time</span>&nbsp;*
                        </div>
                        <div class="dateTimePickerBox w-300 h-40 font-14">
                            <div id="picker" class="dtp_main">
                                <span></span>
                            </div><img class="datetimePicker" role="button" tabindex="0">
                            <input type="hidden" id="result" value="" required="">
                        </div>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 18px" aria-hidden="false">
                        <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                            <span>Account Number</span>&nbsp;*
                        </div>
                        <div class="dateTimePickerBox w-300 h-40 font-14">
                            <div id="picker" class="dtp_main">
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 18px" aria-hidden="false">
                        <div class="left-title font-14 text-blue" style="width: 125px; margin-right: 20px">
                            <span>Account Name</span>&nbsp;*
                        </div>
                        <div class="dateTimePickerBox w-300 h-40 font-14">
                            <div id="picker" class="dtp_main">
                                <span>3</span>

                            </div>
                        </div>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 31px">
                        <div class="left-title font-14 text-blue">
                            <span>Upload</span>&nbsp;*
                        </div>
                        <label class="inputDeposit w-300 h-40 align-items-center" style="display: flex; align-items: center; justify-content: center;">
                            <span>UPLOAD RECEIPT</span>
                            <input type="file" accept="image/*" name="file" class="ng-pristine ng-untouched ng-valid ng-empty" aria-invalid="false">
                        </label>
                    </div>
                    <div class="radio-tile-group row" style="margin-top: 26px">
                        <div class="left-title font-14 text-blue">Remarks</div>
                        <div class="input-container">
                            <div class="inputDeposit w-300 h-40 align-items-center">
                                <input class="ml-15 font-14 ng-pristine ng-untouched ng-valid ng-empty" id="referenceID" name="referenceID" placeholder="remarks" type="Reference" aria-invalid="false">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="radio-tile-group row" style="margin-top: 31px">
                    <div class="left-title font-14 text-blue">

                    </div>
                    <button class="gradientBtn" type="submit" style="font-size: inherit" disabled="disabled">SUBMIT</button>
                    <!---->

                    <!---->
                    <!---->
                </div>
            </form>

        </div>
    </div>

    </div>
    </section>
    </div>
    </div>
    </div>
    <?php include('footer_sign.php') ?>